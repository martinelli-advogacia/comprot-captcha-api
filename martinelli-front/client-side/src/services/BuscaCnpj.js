import api from "./Api";

const chamadas = {
  async pesquisarProcessosPorCnpj(cnpj) {
    return await api().get(`/processo/cnpj/${cnpj}`);
  },
  async demandarAtualizacaoCnpjs(demanda) {
    return await api().post(`/demanda`, demanda);
  }
  
};

export default chamadas;
