import apiComprot from "./ApiComprot";

const comprotApi = {
  async buscarListaProcessos(request){
    return await apiComprot().post(`/captcha/listaProcessos`,request)
  },
  async buscarListaProcessosCaptcha(requestCaptcha){
    return await apiComprot().post(`/captcha/listaProcessos/captcha`,requestCaptcha)
  },
  async buscarProcesso(request){
    return await apiComprot().post(`/captcha/processo`,request)
  },
  async buscarProcessoCaptcha(requestCaptcha){
    return await apiComprot().post(`/captcha/processo/captcha`,requestCaptcha)
  }
};

export default comprotApi;
