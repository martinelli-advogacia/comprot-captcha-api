import axios from "axios";

export default function() {
  
    const config = {
      baseURL: process.env.VUE_APP_URL,
      withCredentials: true,
    };
  
    const api = axios.create(config);
  
    api.interceptors.response.use(
      response => response,
      error => {
        if (401 === error.response.status) {
          window.location("/");
        } else {
          return Promise.reject(error);
        }
      }
    );
  
    return api;
  }
  