import Vue from 'vue';
import VueRouter from 'vue-router';

import Principal from '../views/PaginaPrincipal.vue';
import ListaProcessos from '../views/ListaProcessosPrincipal.vue';
import ConsultaCNPJ from '../views/ConsultaCNPJ.vue';
import ConsultaProcesso from '../views/ConsultaProcesso.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'principal',
    component: Principal,
  },
  {
    path: '/lista/processos/:cnpj',
    name: 'lista-processos',
    component: ListaProcessos,
    props: true,
  },
  {
    path: '/processo/info/:processo',
    name: 'processo',
    component: ConsultaProcesso,
    props: true,
  },
  {
    path: "/demanda",
    name: "lista-cnpj",
    component: ConsultaCNPJ,
    props: true,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
