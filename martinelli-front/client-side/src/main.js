import Vue from 'vue';
import App from './App.vue';
import router from './router';

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';

import 'bootstrap/dist/css/bootstrap.min.css';
import jquery from 'jquery';

import './assets/css/normalize.css';
import './assets/css/style.css';

window.$ = jquery;
window.jQuery = jquery;

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
