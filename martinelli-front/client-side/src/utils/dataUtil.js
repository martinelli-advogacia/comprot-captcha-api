export const utils = {
  getTimeComOffset: function(data) {
    if (!data) return;
    // O método getTimezoneOffset retorna um valor em minutos. As multiplicação
    // transformam em milisegundos
    let offset = data.getTimezoneOffset() * 60 * 1000;

    return data.getTime() + offset;
  },
  aplicarOffset: function(data) {
    if (!data) return;

    return new Date(utils.getTimeComOffset(data));
  },
  maskHora: function(data, seconds = true) {
    let dataOffset = utils.getTimeComOffset(new Date(data));
    let formatado = null;
    // Opções do formatador
    let options = {
      hour: "numeric",
      minute: "numeric",
      second: seconds ? "numeric" : undefined,
      hour12: false,
      timeZone: "America/Sao_Paulo",
    };

    if (data != null) {
      formatado = new Intl.DateTimeFormat("pt-BR", options).format(dataOffset);
    }
    return formatado;
  },
  maskDataHora: function(data, seconds = false) {
    let dataOffset = utils.getTimeComOffset(new Date(data));
    let formatado = null;
    // Opções do formatador
    let options = {
      year: "numeric",
      month: "numeric",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: seconds ? "numeric" : undefined,
      hour12: false,
      timeZone: "America/Sao_Paulo",
    };

    if (data != null) {
      formatado = new Intl.DateTimeFormat("pt-BR", options).format(dataOffset);
    }
    return formatado;
  },
  maskData: function(data) {
    let dataOffset = utils.getTimeComOffset(new Date(data));
    if (isNaN(dataOffset)) {
      return `${data.dayOfMonth}/${data.monthValue}/${data.year}`;
    }
    let formatado = null;
    // Opções do formatador
    let options = {
      year: "numeric",
      month: "numeric",
      day: "numeric",
      // hour: 'numeric', minute: 'numeric', second: seconds ? 'numeric' : undefined,
      // hour12: false,
      // timeZone: 'America/Sao_Paulo'
    };

    if (data != null && dataOffset != null) {
      formatado = new Intl.DateTimeFormat("pt-BR", options).format(dataOffset);

      if (data != null) {
        formatado = new Intl.DateTimeFormat("pt-BR", options).format(
          dataOffset
        );
      }
      return formatado;
    }
  },
  aplicarISODateTime: function(
    dataStr,
    separadorData = "/",
    adiocionarCentessimos = true
  ) {
    if (
      dataStr === undefined ||
      dataStr.length == 0 ||
      typeof dataStr !== "string"
    ) {
      return dataStr;
    }

    let dataArray = dataStr.split(" ");
    dataArray[0] = dataArray[0]
      .split(separadorData)
      .reverse()
      .join("-");
    let dataIso = dataArray.join("T") + (adiocionarCentessimos ? ":00Z" : "Z");
    return dataIso;
  },

  diaMesAnoParaAnoMesDia: function(data) {
    data = data.replace(/-/g, "/");

    const dia = data.split("/")[0];
    const mes = data.split("/")[1];
    const ano = data.split("/")[2];

    return ano + "-" + ("0" + mes).slice(-2) + "-" + ("0" + dia).slice(-2);
    // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
  },
  getMesAno(date) {
    if (date === undefined || date === null) return null;
    return date.toLocaleDateString("pt-BR", {
      month: "long",
      year: "numeric",
    });
  },
  getNomeDoMes(mes) {
    if (mes === undefined || mes === null) return null;

    const date = new Date(1, mes, 1);
    return date.toLocaleString("default", {
      month: "long",
    });
  },
  dateToJavaDateString(date) {
    return date
      .toLocaleDateString()
      .split("/")
      .reverse()
      .join("-");
  },
};
