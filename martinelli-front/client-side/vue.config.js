module.exports = {
  outputDir: "../WebContent/",
  publicPath: "/martinelli-front",
  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = "Martinelli Advogados";
      return args;
    });
  },
};
