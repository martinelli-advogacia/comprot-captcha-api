package br.com.martinelliadvogados.martinellifront.service;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import br.com.martinelliadvogados.martinellifront.dto.DemandaDTO;
import br.com.martinelliadvogados.martinellifront.dto.FilialDTO;

class DemandaServiceTest {
	DemandaService service = new DemandaService();

	@Test
	void testCriar() {
		try {
			FilialDTO filial = new FilialDTO();
			filial.setCnpjCompleto("01718074000120");
			Set<FilialDTO> setFiliais = new HashSet<>();
			setFiliais.add(filial);
			DemandaDTO demanda = new DemandaDTO();
			demanda.setDias(30);
			demanda.setEmailDemandante("davi.diniz@hepta.com.br");
			demanda.setFiliais(setFiliais);
			demanda.setNomeDemandante("davi");
			service.criar(demanda);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
