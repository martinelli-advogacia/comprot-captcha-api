package br.com.martinelliadvogados.martinellifront.service;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import br.com.martinelliadvogados.martinellifront.dto.DemandaDTO;
import br.com.martinelliadvogados.martinellifront.dto.FilialDTO;
import br.com.martinelliadvogados.martinellifront.persistence.FilialExtDAO;

public class ComprotServiceTest {

	ComprotService service = new ComprotService();
	FilialExtDAO filialDAO = new FilialExtDAO();

	@Test
	void erradoTest() {
		DemandaDTO demanda = new DemandaDTO();
		try {
			demanda.setDias(10);
			demanda.setFiliais(null);
			service.buscaPorDemandaComprot(demanda);
			fail();
		} catch (Exception e) {
			//
		}
		try {
			demanda.setDias(-1);
			demanda.setFiliais(new HashSet<>());
			service.buscaPorDemandaComprot(demanda);
			fail();
		} catch (Exception e) {
			//
		}
		try {
			demanda.setDias(30);
			demanda.setFiliais(new HashSet<>());
			service.buscaPorDemandaComprot(demanda);
			fail();
		} catch (Exception e) {
			//
		}
		assert (true);
	}

	@Test
	void corretoTest() {
		String[] cnpjs = { "81551145000182", "53627428000125", "84430149000109", "08520338000186" };
		Set<FilialDTO> filiais = new HashSet<>();
		for (String cnpj : cnpjs) {
			FilialDTO nova = new FilialDTO();
			nova.setCnpjCompleto(cnpj);
			filiais.add(nova);
		}
		DemandaDTO demanda = new DemandaDTO();
		demanda.setDias(null);
		demanda.setFiliais(filiais);
		try {
			System.out.println("Inicio!");
			String res = service.buscaPorDemandaComprot(demanda);
			System.out.println("Fim! " + res);
			assert (!res.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
