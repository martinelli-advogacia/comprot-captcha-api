package br.com.martinelliadvogados.martinellifront.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class HeptaException extends Exception {

	private static final long serialVersionUID = 1L;
	Status status = Status.BAD_REQUEST;

	public HeptaException(String msg, Status status) {
		super(msg);
		this.status = status;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
