package br.com.martinelliadvogados.martinellifront.utils.exceptions;

public class ObjetoEmailErradoException extends Exception {
	private static final long serialVersionUID = 1L;

	public ObjetoEmailErradoException(String errorMessage) {
		super(errorMessage);
	}
}
