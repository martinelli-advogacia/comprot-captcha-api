package br.com.martinelliadvogados.martinellifront.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.persistence.ProcessoDAO;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;

public class ProcessoExtDAO extends ProcessoDAO {

	private static final Logger LOG = LogManager.getLogger(ProcessoExtDAO.class);

	private static final long serialVersionUID = 1L;

	public Processo buscar(Long id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Processo processo = null;
		try {

			String sql = "SELECT p from Processo p"
					+ " JOIN FETCH p.filial f"
					+ " LEFT JOIN FETCH p.movimentos m"
					+ " where p.id = :id";

			Query query = em.createQuery(sql).setParameter("id", id);
			processo = (Processo) query.getSingleResult();

		} catch (NoResultException noResultException) {
			return null;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
		return processo;
	}

	public Processo buscaPorNumProcesso(String numProcesso) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Processo processo;
		try {

			String sql = "SELECT p from Processo p"
					+ " JOIN FETCH p.filial f"
					+ " LEFT JOIN FETCH p.movimentos m"
					+ " where p.numeroProcessoPrincipal = :numProcesso";

			Query query = em.createQuery(sql).setParameter("numProcesso", numProcesso);
			processo = (Processo) query.getSingleResult();

		} catch (NoResultException noResultException) {
			return null;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
		return processo;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Processo> porCnpj(Set<String> cnpj) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Map<String, Processo> map = new HashMap<>();
		try {

			String sql = "SELECT p from Processo p"
					+ " JOIN FETCH p.filial f"
					+ " LEFT JOIN FETCH p.movimentos m"
					+ " WHERE f.cnpj in (:cnpj)";

			Query query = em.createQuery(sql).setParameter("cnpj", cnpj);
			List<Processo> list = query.getResultList();
			for (Processo p : list) {
				map.put(p.getNumeroProcessoPrincipal(), p);
			}
		} catch (NoResultException noResultException) {
			return new HashMap<>();
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	public List<Processo> buscaProcessosPorCnpj(String cnpj) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<Processo> list = new ArrayList<>();
		try {

			String sql = "SELECT p from Processo p"
					+ " JOIN FETCH p.filial f"
					+ " LEFT JOIN FETCH p.movimentos m"
					+ " WHERE f.cnpj = :cnpj";

			Query query = em.createQuery(sql).setParameter("cnpj", cnpj);
			list = query.getResultList();
		} catch (NoResultException noResultException) {
			return new ArrayList<>();
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
		return list;
	}

}