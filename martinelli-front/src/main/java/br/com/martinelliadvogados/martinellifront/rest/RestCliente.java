package br.com.martinelliadvogados.martinellifront.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.martinelliadvogados.martinellifront.dto.FilialDTO;
import br.com.martinelliadvogados.martinellifront.persistence.FilialExtDAO;

@Path("/cliente")
public class RestCliente {

	static final String ERRO_CNPJ_VAZIO = "Erro, cnpj vazio";
	static final String FILIAL_CRIADA_COM_SUCESSO = "Filial criada com sucesso";
	static final String ERRO_AO_DELETAR_FILIAL = "Erro ao deletar filial";
	static final String FILIAL_DELETADA_COM_SUCESSO = "Filial deletada com sucesso";
	static final String ERRO_AO_EDITAR_FILIAL = "Erro ao editar filial";
	static final String FILIAL_EDITADA_COM_SUCESSO = "Filial editada com sucesso";
	static final String ERRO_AO_BUSCAR_FILIAL = "Erro ao buscar filial";
	static final Object ERRO_AO_CRIAR_FILIAL = "Erro ao criar filial";

	FilialExtDAO dao = new FilialExtDAO();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/hello")
	public Response buscar() {
		return Response.ok().entity("OI").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cnpj}")
	public Response buscar(@PathParam(value = "cnpj") String cnpj) {
		if (cnpj.isEmpty())
			return Response.serverError().entity(ERRO_CNPJ_VAZIO).build();
		try {
			FilialDTO res = new FilialDTO(dao.buscarPorCnpj(cnpj));
			return Response.ok().entity(res).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_BUSCAR_FILIAL).build();
		}
	}

	@POST
	@Path("/criar")
	public Response criar(FilialDTO filial) {
		if (filial == null)
			return Response.serverError().entity("Dados inválidos").build();

		try {
			dao.criarFilial(filial);
			return Response.ok().entity(FILIAL_CRIADA_COM_SUCESSO).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_CRIAR_FILIAL).build();
		}
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/deletar/{cnpj}")
	public Response deletar(@PathParam(value = "cnpj") String cnpj) {
		if (cnpj.isEmpty())
			return Response.serverError().entity(ERRO_CNPJ_VAZIO).build();
		try {

			dao.deletarPorCnpj(cnpj);

			return Response.ok().entity(FILIAL_DELETADA_COM_SUCESSO).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_DELETAR_FILIAL).build();
		}
	}

	@POST
	@Path("/editar")
	public Response editar(FilialDTO filial) {
		try {
			dao.editarFilial(filial);

			return Response.ok(FILIAL_EDITADA_COM_SUCESSO).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_EDITAR_FILIAL).build();
		}
	}
}
