package br.com.martinelliadvogados.martinellifront.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.entity.Demanda;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.EstadoDemanda;
import com.hepta.martinelliadvogados.shared.persistence.DemandaDAO;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

import br.com.martinelliadvogados.martinellifront.dto.DemandaDTO;
import br.com.martinelliadvogados.martinellifront.dto.FilialDTO;
import br.com.martinelliadvogados.martinellifront.persistence.FilialExtDAO;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.DemandaInexistenteException;

public class DemandaService {
	private static final Logger LOG = Logger.getLogger(DemandaService.class);
	DemandaDAO dao = new DemandaDAO();
	FilialExtDAO daoFilial = new FilialExtDAO();
	EmailService emailService = new EmailService();

	public void criar(DemandaDTO demanda) throws Exception {
		Demanda ent = new Demanda();
		ent.setDataIni(LocalDateTime.now());
		ent.setEstado(EstadoDemanda.EM_ANDAMENTO);
		ent.setNomeDemandante(demanda.getNomeDemandante());
		ent.setQuantidade(demanda.getFiliais().size());
		ent.setEmailDemandante(demanda.getEmailDemandante());

		Set<String> cnpjs = new HashSet<>();
		for (FilialDTO f : demanda.getFiliais()) {
			cnpjs.add(f.getCnpjCompleto());
		}
		List<Filial> filiais = daoFilial.buscarPorCnpj(cnpjs);
		if(filiais.isEmpty()) {
			daoFilial.achaOuCria(demanda.getFiliais());
			filiais = daoFilial.buscarPorCnpj(cnpjs);
		}
		ent.setFiliais(new HashSet<>(filiais));
		
		

		dao.save(ent);
		final Long id = ent.getId();
		LOG.error("terminei normal");
		new Thread() {

			@Override
			public void run() {
				ComprotService service = new ComprotService();
				try {
					LOG.error("inicio thread");
					service.buscaPorDemandaComprot(demanda);
					atualizar(id);
					Demanda atualizado = dao.buscarComFiliais(id);
					emailService.enviarEmail(atualizado);
					LOG.error("fim thread");
				} catch (Exception e) {
					e.printStackTrace();
					LOG.error(StacktraceToString.convert(e));
				}
			}
		}.start();
	}

	public Demanda atualizar(Long id) throws Exception {
		Demanda ent = dao.find(id);
		if (ent == null)
			throw new DemandaInexistenteException();
		ent.setDataFim(LocalDateTime.now());
		ent.setEstado(EstadoDemanda.FINALIZADA);
		dao.update(ent);
		return ent;
	}

	public List<DemandaDTO> listar() throws Exception {
		List<DemandaDTO> result = new ArrayList<>();
		List<Demanda> demandasEnt = dao.listar();
		for (Demanda ent : demandasEnt) {
			result.add(new DemandaDTO(ent));
		}
		return result;
	}
}
