package br.com.martinelliadvogados.martinellifront.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.hepta.martinelliadvogados.shared.dto.ProcessoDTO;

import br.com.martinelliadvogados.martinellifront.service.ProcessoService;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.HeptaException;

@Path("/processo")
public class RestProcesso {

	static final String ERRO_AO_BUSCAR_PROCESSO = "Erro ao buscar processo";
	static final String ERRO_CNPJ_VAZIO = "Erro, cnpj ou numero do processo não pode ser vazio";

	ProcessoService service = new ProcessoService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cnpj/{cnpj}")
	public Response buscarCnpj(@PathParam(value = "cnpj") String cnpj) {
		if (cnpj.isEmpty())
			return Response.serverError().entity(ERRO_CNPJ_VAZIO).build();
		try {
			List<ProcessoDTO> result = service.buscarCnpj(cnpj);
			return Response.ok().entity(result).build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return Response.status(e.getStatus()).entity(e.getMessage()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_BUSCAR_PROCESSO).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/numProcesso/{numProcesso}")
	public Response buscarNumProcesso(@PathParam(value = "numProcesso") String numProcesso) {
		if (numProcesso.isEmpty())
			return Response.serverError().entity(ERRO_CNPJ_VAZIO).build();
		try {
			ProcessoDTO result = service.buscarNumProcesso(numProcesso);
			return Response.ok().entity(result).build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return Response.status(e.getStatus()).entity(e.getMessage()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_BUSCAR_PROCESSO).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/id/{id}")
	public Response buscarNumProcesso(@PathParam(value = "id") Long id) {
		try {
			ProcessoDTO result = service.buscar(id);
			return Response.ok().entity(result).build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return Response.status(e.getStatus()).entity(e.getMessage()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_BUSCAR_PROCESSO).build();
		}
	}

}
