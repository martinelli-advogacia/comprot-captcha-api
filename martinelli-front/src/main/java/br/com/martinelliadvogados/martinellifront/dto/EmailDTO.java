package br.com.martinelliadvogados.martinellifront.dto;

import java.util.Map;

import com.hepta.martinelliadvogados.shared.entity.Processo;

import br.com.martinelliadvogados.martinellifront.email.EmailDemanda;
import br.com.martinelliadvogados.martinellifront.enums.Email;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.ObjetoEmailErradoException;

public class EmailDTO {
	private static final String ERRO_OBJETO_EMAIL_ERRADO = null;
	public String titulo;
	public String mensagem;
	public String endereco;
	public Boolean contemArquivos;
	public String nomeArquivo;
	public Map<String, Processo> processos;

	public EmailDTO(Object campos, Email tipoEmail) throws ObjetoEmailErradoException {
		switch (tipoEmail) {
			case RESULTADO_DEMANDA:
				if (campos instanceof CamposEmailDemandaFinalizadaDTO) {
					CamposEmailDemandaFinalizadaDTO aux = (CamposEmailDemandaFinalizadaDTO) campos;
					this.titulo = aux.assunto;
					this.mensagem = EmailDemanda.getTemplate(aux);
					this.endereco = aux.emailDestinatario;
					this.contemArquivos = aux.contemArquivos;
					this.nomeArquivo = aux.nomeArquivo;
					this.processos = aux.processos;
				} else
					throw new ObjetoEmailErradoException(ERRO_OBJETO_EMAIL_ERRADO);
				break;
			default:
				break;
		}
	}

	public String getNomeArquivo() {
		return this.nomeArquivo + ".xlsx";
	}
}
