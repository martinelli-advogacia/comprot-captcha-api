package br.com.martinelliadvogados.martinellifront.service;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.hepta.martinelliadvogados.shared.entity.Demanda;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.entity.Processo;

import br.com.martinelliadvogados.martinellifront.dto.CamposEmailDemandaFinalizadaDTO;
import br.com.martinelliadvogados.martinellifront.email.EmailUtil;
import br.com.martinelliadvogados.martinellifront.enums.Email;
import br.com.martinelliadvogados.martinellifront.persistence.ProcessoExtDAO;

public class EmailService {
	
	ProcessoExtDAO daoProcesso = new ProcessoExtDAO();
	public void enviarEmail(Demanda atualizado) throws Exception {
		Set<String> cnpjs = new HashSet<>();
		for (Filial f : atualizado.getFiliais()) {
			cnpjs.add(f.getCnpj());
		}
		Map<String,Processo> processos = daoProcesso.porCnpj(cnpjs);
		CamposEmailDemandaFinalizadaDTO campos = new CamposEmailDemandaFinalizadaDTO(atualizado.getEmailDemandante(),
				atualizado.getNomeDemandante(), processos);
		EmailUtil.enviarEmail(campos, Email.RESULTADO_DEMANDA);
	}

}