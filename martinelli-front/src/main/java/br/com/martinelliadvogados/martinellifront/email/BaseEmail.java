package br.com.martinelliadvogados.martinellifront.email;

public class BaseEmail {
	public static String emailHeader() {
		StringBuilder str = new StringBuilder();
		str.append("<!doctype html>");
		str.append("<html>");
		str.append("<head>");
		str.append(" 	<meta charset=\"utf-8\">");
		str.append(" 	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
		str.append(" 	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
		str.append(" 	<title>NIN</title>");
		str.append(" 	<style type=\"text/css\">");
		str.append(" 	html,body{font-family:sans-serif;font-size:15px;mso-height-rule:exactly;line-height:20px;color:#555;margin:0!important;padding:0!important;height:100%!important;width:100%!important}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}.ExternalClass{width:100%}div[style*=\"margin: 16px 0\"]{margin:0!important}table,td{mso-table-lspace:0pt!important;mso-table-rspace:0pt!important}table{border-spacing:0!important;border-collapse:collapse!important;table-layout:fixed!important;margin:0 auto!important}table table table{table-layout:auto}img{-ms-interpolation-mode:bicubic}.yshortcuts a{border-bottom:none!important}a[x-apple-data-detectors]{color:inherit!important}.button-td,.button-a{transition:all 100ms ease-in}.button-td:hover,.button-a:hover{background:#555555!important;border-color:#555555!important}@media screen and (max-width:600px){.email-container{width:100%!important}.fluid,.fluid-centered{max-width:100%!important;height:auto!important;margin-left:auto!important;margin-right:auto!important}.fluid-centered{margin-left:auto!important;margin-right:auto!important}.stack-column,.stack-column-center{display:block!important;width:100%!important;max-width:100%!important;direction:ltr!important}.stack-column-center{text-align:center!important}.center-on-narrow{text-align:center!important;display:block!important;margin-left:auto!important;margin-right:auto!important;float:none!important}table.center-on-narrow{display:inline-block!important}}");
		str.append(" 	</style>");
		str.append("</head>");
		str.append("<body bgcolor=\"#E8F0F2\" width=\"100%\" style=\"margin: 0;\">");
		return str.toString();
	}
	
	public static String emailFooter() {
		StringBuilder str = new StringBuilder();
		str.append("</body>");
		str.append("</html>");
		return str.toString();
	}
}
