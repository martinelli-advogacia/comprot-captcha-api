package br.com.martinelliadvogados.martinellifront.service.comprot;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Erro;
import com.hepta.martinelliadvogados.shared.service.MuitosProcessosService;

import br.com.martinelliadvogados.martinellifront.dto.FilialDTO;
import br.com.martinelliadvogados.martinellifront.persistence.FilialExtDAO;

public class MuitosProcessosServiceNIN extends MuitosProcessosService {
	private static final Logger LOG = Logger.getLogger(MuitosProcessosServiceNIN.class);

	private FilialExtDAO daoFilialExt = new FilialExtDAO();

	public MuitosProcessosServiceNIN() {
		super(LOG, false);
		this.mostrarLogs = false;
	}

	public int pegarMuitosProcessos(Set<FilialDTO> set) throws Exception {
		Set<String> cnpjs = new HashSet<>();
		for (FilialDTO f : set) {
			cnpjs.add(f.getCnpjCompleto());
		}
		List<Filial> filiais = daoFilialExt.porErroECNPJ(cnpjs, Erro.PROCESSOS_DEMAIS);
		if (filiais.isEmpty()) {
			return 0;
		}
		return buscarAndSave(filiais);
	}

}
