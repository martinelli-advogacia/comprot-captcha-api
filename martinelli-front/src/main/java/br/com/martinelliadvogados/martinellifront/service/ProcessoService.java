package br.com.martinelliadvogados.martinellifront.service;

import java.util.ArrayList;
import java.util.List;

import com.hepta.martinelliadvogados.shared.dto.ProcessoDTO;
import com.hepta.martinelliadvogados.shared.entity.Processo;

import br.com.martinelliadvogados.martinellifront.persistence.ProcessoExtDAO;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.CNPJMalformadoException;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.ProcessoInexistenteException;

public class ProcessoService {

	ProcessoExtDAO dao = new ProcessoExtDAO();

	public ProcessoDTO buscar(Long id) throws Exception {
		Processo proc = dao.buscar(id);
		if (proc == null)
			throw new ProcessoInexistenteException();

		return new ProcessoDTO(proc);
	}

	public ProcessoDTO buscarNumProcesso(String numProcesso) throws Exception {
		Processo proc = dao.buscaPorNumProcesso(numProcesso);
		if (proc == null)
			throw new ProcessoInexistenteException();

		return new ProcessoDTO(proc);
	}

	public List<ProcessoDTO> buscarCnpj(String cnpj) throws Exception {
		List<Processo> processos;
		if (cnpj.length() != 14) {
			throw new CNPJMalformadoException();
		}

		processos = dao.buscaProcessosPorCnpj(cnpj);
		if (processos.isEmpty())
			throw new ProcessoInexistenteException();

		List<ProcessoDTO> result = new ArrayList<>();
		for (Processo p : processos) {
			result.add(new ProcessoDTO(p));
		}
		return result;
	}

}
