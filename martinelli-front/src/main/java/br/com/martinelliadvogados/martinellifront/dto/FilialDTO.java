package br.com.martinelliadvogados.martinellifront.dto;

import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;

public class FilialDTO {
	String cnpjCompleto;
	String cnpjPrefixo;
	String cnpjSufixo;
	TipoCliente tipo;

	public FilialDTO() {
		super();
	}

	public FilialDTO(Filial filial) {
		this.cnpjCompleto = filial.getCnpj();
		this.cnpjPrefixo = filial.getCliente().getCnpjPrefixo();
		this.cnpjSufixo = filial.getCnpjSufixo();
		this.tipo = filial.getTipo();
	}

	public String getCnpjCompleto() {
		return cnpjCompleto;
	}

	public void setCnpjCompleto(String cnpjCompleto) {
		this.cnpjCompleto = cnpjCompleto;
	}

	public String getCnpjPrefixo() {
		return cnpjPrefixo;
	}

	public void setCnpjPrefixo(String cnpjPrefixo) {
		this.cnpjPrefixo = cnpjPrefixo;
	}

	public String getCnpjSufixo() {
		return cnpjSufixo;
	}

	public void setCnpjSufixo(String cnpjSufixo) {
		this.cnpjSufixo = cnpjSufixo;
	}

	public TipoCliente getTipo() {
		return tipo;
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}

}