package br.com.martinelliadvogados.martinellifront.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class CNPJMalformadoException extends HeptaException {

	private static final long serialVersionUID = 1L;

	public CNPJMalformadoException() {
		super("CNPJ inválido", Status.BAD_REQUEST);
	}

}