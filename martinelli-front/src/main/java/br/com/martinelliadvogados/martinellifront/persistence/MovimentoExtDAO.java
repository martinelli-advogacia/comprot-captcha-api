package br.com.martinelliadvogados.martinellifront.persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.persistence.MovimentoDAO;

public class MovimentoExtDAO extends MovimentoDAO {
	private static final Logger LOGGER = LogManager.getLogger(MovimentoExtDAO.class);

	private static final long serialVersionUID = 1L;
}