package br.com.martinelliadvogados.martinellifront.service.comprot;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.service.NovosProcessosService;

import br.com.martinelliadvogados.martinellifront.dto.FilialDTO;
import br.com.martinelliadvogados.martinellifront.persistence.FilialExtDAO;

public class NovosProcessosServiceNIN extends NovosProcessosService {
	private static final Logger LOG = Logger.getLogger(NovosProcessosServiceNIN.class);

	private FilialExtDAO daoFilialExt = new FilialExtDAO();

	public NovosProcessosServiceNIN() {
		super(LOG, false);
		this.mostrarLogs = true;
	}

	public int pesquisarNovosProcessos(long ini, long fim, Set<FilialDTO> set) throws Exception {
		Set<String> cnpjs = new HashSet<>();
		for (FilialDTO f : set) {
			cnpjs.add(f.getCnpjCompleto());
		}
		List<Filial> filiais = daoFilialExt.porCNPJ(cnpjs);
		if (filiais.isEmpty()) {
			return 0;
		}
		info("Achou " + filiais.size() + " filiais no banco");
		return buscarAndSave(filiais);
	}

}
