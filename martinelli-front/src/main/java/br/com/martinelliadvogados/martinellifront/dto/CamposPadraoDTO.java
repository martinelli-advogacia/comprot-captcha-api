package br.com.martinelliadvogados.martinellifront.dto;

public class CamposPadraoDTO {
	public String emailDestinatario;
	public String assunto;
	public Boolean contemArquivos;
	public String nomeArquivo;
}
