package br.com.martinelliadvogados.martinellifront.email;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.Application;

import br.com.martinelliadvogados.martinellifront.dto.EmailDTO;
import br.com.martinelliadvogados.martinellifront.enums.Email;
import br.com.martinelliadvogados.martinellifront.excel.PlanilhaDemanda;
import br.com.martinelliadvogados.martinellifront.utils.OSValidator;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.TipoNaoImplementadoException;

public class EmailUtil {
	private static final Logger LOG = Logger.getLogger(EmailUtil.class);
	private static final String EMAIL_REMETENTE = Application.getSmtpContato();
	private static final String NOME_REMETENTE = "Hepta";

	private static Properties setPropertiesHepta() {
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", Application.getSmtpHost());
		properties.setProperty("mail.smtp.port", Application.getSmtpPort());
		properties.setProperty("mail.user", Application.getSmtpUser());
		properties.setProperty("mail.password", Application.getSmtpPassword());
		return properties;
	}

	public static void enviarEmail(Object campos, Email tipoEmail) throws Exception {
		Properties properties = null;

		properties = setPropertiesHepta();

		Session session = Session.getDefaultInstance(properties);
		MimeMessage msg = new MimeMessage(session);

		EmailDTO email = new EmailDTO(campos, tipoEmail);

		msg.setSubject(email.titulo, "UTF-8");

		msg.setFrom(new InternetAddress(EMAIL_REMETENTE, NOME_REMETENTE));

		if (email.contemArquivos)
			msg.setContent(adicionaArquivoXLSX(email));
		else
			msg.setContent(email.mensagem, "text/html; charset=UTF-8");

		msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email.endereco));
		Transport.send(msg);

	}

	public static Multipart adicionaArquivoXLSX(EmailDTO email)
			throws IOException, MessagingException, TipoNaoImplementadoException {
		MimeBodyPart textBodyPart = new MimeBodyPart();
		textBodyPart.setContent(email.mensagem, "text/html");

		MimeBodyPart messageBodyPart = new MimeBodyPart();
		Multipart multipart = new MimeMultipart();
		messageBodyPart = new MimeBodyPart();

		PlanilhaDemanda planilha = new PlanilhaDemanda();
		XSSFWorkbook wb = planilha.gerarXLSX(email.processos);// demanda

		ByteArrayOutputStream fileOut = null;
		fileOut = new ByteArrayOutputStream();
		wb.write(fileOut);
		fileOut.close();
		String data = LocalDateTime.now().toString();
		data = data.substring(0, 19).replace(":", "_");
		String caminhoPlanoB = "";
		if (OSValidator.isWindows()) {
			caminhoPlanoB = "C:\\Demandas comprot\\demanda_" + data + ".xlsx";
		} else {
			caminhoPlanoB = new File("").getAbsolutePath() + "/demanda_" + data + ".xlsx";
		}
		LOG.error(caminhoPlanoB);
		File targetFile = new File(caminhoPlanoB);
		OutputStream outStream = new FileOutputStream(targetFile);
		wb.write(outStream);
		outStream.close();

		String fileName = email.getNomeArquivo();
		DataSource source = new ByteArrayDataSource(fileOut.toByteArray(),
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(fileName);

		multipart.addBodyPart(textBodyPart);
		multipart.addBodyPart(messageBodyPart);
		LOG.error("OK");
		LOG.error(caminhoPlanoB);
		return multipart;
	}
}
