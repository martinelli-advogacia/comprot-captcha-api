package br.com.martinelliadvogados.martinellifront.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.martinelliadvogados.martinellifront.dto.HealthCheckDTO;
import br.com.martinelliadvogados.martinellifront.service.HealthCheckService;

@Path("/health")
public class RestHealthCheck {

	HealthCheckService service = new HealthCheckService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response check() {
		try {
			HealthCheckDTO result = service.checkHealth();
			return Response.ok().entity(result).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}

	}
}
