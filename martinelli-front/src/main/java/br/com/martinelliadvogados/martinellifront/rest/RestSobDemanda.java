package br.com.martinelliadvogados.martinellifront.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.martinelliadvogados.martinellifront.dto.DemandaDTO;
import br.com.martinelliadvogados.martinellifront.service.DemandaService;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.HeptaException;

@Path("/demanda")
public class RestSobDemanda {
	private static final Object ERRO_DADOS_INVALIDOS = "Dados inválidos";
	private static final Object ERRO_AO_BUSCAR_SOB_DEMANDA = "Erro ao tentar buscar processos de clientes sob demanda";
	DemandaService serviceDemanda = new DemandaService();

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarCnpj(DemandaDTO demanda) {
		if (demanda != null && demanda.getFiliais() != null && demanda.getFiliais().isEmpty())
			return Response.serverError().entity(ERRO_DADOS_INVALIDOS).build();
		try {
			serviceDemanda.criar(demanda);
			return Response.ok().build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return Response.status(e.getStatus()).entity(e.getMessage()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_BUSCAR_SOB_DEMANDA).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listar() {
		try {
			List<DemandaDTO> demandas = serviceDemanda.listar();
			return Response.ok().entity(demandas).build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return Response.status(e.getStatus()).entity(e.getMessage()).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(ERRO_AO_BUSCAR_SOB_DEMANDA).build();
		}
	}
}
