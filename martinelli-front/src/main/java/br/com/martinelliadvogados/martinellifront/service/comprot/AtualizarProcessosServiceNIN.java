package br.com.martinelliadvogados.martinellifront.service.comprot;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.service.AtualizarProcessosService;

import br.com.martinelliadvogados.martinellifront.dto.FilialDTO;
import br.com.martinelliadvogados.martinellifront.persistence.ProcessoExtDAO;

public class AtualizarProcessosServiceNIN extends AtualizarProcessosService {
	private static final Logger LOG = Logger.getLogger(AtualizarProcessosServiceNIN.class);
	ProcessoExtDAO daoProcessoExt = new ProcessoExtDAO();

	public AtualizarProcessosServiceNIN(boolean novosProcessos, boolean imprimirLogs,
			int numeroProcessos) {
		super(novosProcessos, imprimirLogs, numeroProcessos, false);
	}

	public AtualizarProcessosServiceNIN() {
		super();
	}

	public int atualizarProcessos(Set<FilialDTO> set) throws Exception {
		Set<String> cnpjs = new HashSet<>();
		for (FilialDTO f : set) {
			cnpjs.add(f.getCnpjCompleto());
		}
		Map<String, Processo> processosEntity = daoProcessoExt.porCnpj(cnpjs);
		return requestsAndUpdate(processosEntity);
	}

}
