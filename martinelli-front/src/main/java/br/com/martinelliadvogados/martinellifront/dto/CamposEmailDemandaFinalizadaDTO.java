package br.com.martinelliadvogados.martinellifront.dto;

import java.util.Map;

import com.hepta.martinelliadvogados.shared.entity.Processo;

public class CamposEmailDemandaFinalizadaDTO extends CamposPadraoDTO {
	private static final String ASSUNTO_EMAIL_DEMANDA = "Resultados da busca por demanda";
	public String nomeDestino;
	public Map<String, Processo> processos;

	public CamposEmailDemandaFinalizadaDTO(String emailDestino, String nomeDestino, Map<String, Processo> processos) {
		this.assunto = ASSUNTO_EMAIL_DEMANDA;
		this.emailDestinatario = emailDestino;
		this.nomeDestino = nomeDestino;
		this.processos = processos;
		this.contemArquivos = true;
		this.nomeArquivo="resultados_demanda";
	}
}
