package br.com.martinelliadvogados.martinellifront.dto;

import java.util.HashSet;
import java.util.Set;

import com.hepta.martinelliadvogados.shared.entity.Demanda;
import com.hepta.martinelliadvogados.shared.entity.Filial;

public class DemandaDTO {
	Set<FilialDTO> filiais;
	Integer dias;
	String nomeDemandante;
	String emailDemandante;

	public DemandaDTO() {
		super();
	}

	public DemandaDTO(Demanda ent) {
		this.dias = ent.getDias();
		this.nomeDemandante = ent.getNomeDemandante();
		this.emailDemandante = ent.getEmailDemandante();
		this.filiais = new HashSet<>();
		for (Filial fEnt : ent.getFiliais()) {
			this.filiais.add(new FilialDTO(fEnt));
		}
	}

	public String getNomeDemandante() {
		return nomeDemandante;
	}

	public void setNomeDemandante(String nomeDemandante) {
		this.nomeDemandante = nomeDemandante;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public Set<FilialDTO> getFiliais() {
		return filiais;
	}

	public void setFiliais(Set<FilialDTO> filiais) {
		this.filiais = filiais;
	}

	public String getEmailDemandante() {
		return emailDemandante;
	}

	public void setEmailDemandante(String emailDemandante) {
		this.emailDemandante = emailDemandante;
	}

}