package br.com.martinelliadvogados.martinellifront.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.utils.CNPJUtil;

import br.com.martinelliadvogados.martinellifront.utils.OSValidator;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.TipoNaoImplementadoException;

public class PlanilhaDemanda {
	private static final Logger LOG = Logger.getLogger(PlanilhaDemanda.class);

	public XSSFWorkbook gerarXLSX(Map<String, Processo> processos) throws IOException, TipoNaoImplementadoException {
		String sRootPath = new File("").getAbsolutePath();
		if (OSValidator.isWindows()) {
			sRootPath += "\\modelo_demandas.xlsx";
		} else {
			sRootPath += "/modelo_demandas.xlsx";
		}

		FileInputStream excelFile = new FileInputStream(new File(sRootPath));
		XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
		workbook.setMissingCellPolicy(Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
		XSSFSheet sheet = workbook.getSheetAt(0);
		List<Processo> processosAux = new ArrayList<>(processos.values());
		for (int i = 0; i < processosAux.size(); i++) {
			Processo processoAtual = processosAux.get(i);
			XSSFRow row = sheet.createRow(i + 1);
			setCell(row, 0, processoAtual.getNumeroProcessoPrincipal());
			setCell(row, 1, processoAtual.getDataProtocolo());
			setCell(row, 2, processoAtual.getNumeroDocumentoOrigem());
			setCell(row, 3, processoAtual.getNomeProcedencia());
			setCell(row, 4, processoAtual.getNomeInteressado());
			String cnpjCpf = "";
			if (processoAtual.getIndicadorCpfCnpj() != null)
				cnpjCpf = processoAtual.getIndicadorCpfCnpj().equals(2) ? "CNPJ" : "CPF";
			setCell(row, 5, cnpjCpf);
			setCell(row, 6, processoAtual.getNumeroCpfCnpj());
			setCell(row, 7, processoAtual.getNomeOrgaoOrigem());
			setCell(row, 8, processoAtual.getNumeroDocumentoOrigem());
			setCell(row, 9, processoAtual.getNomeOrgaoDestino());
			setCell(row, 10, processoAtual.getSituacao());
			setCell(row, 11, processoAtual.getSiglaUfMovimento());
			setCell(row, 12, processoAtual.getDataConsulta());
			CNPJUtil utilCnpj = new CNPJUtil(processoAtual.getFilial().getCnpj());
			setCell(row, 13, utilCnpj.getPrefix());
//			setCell(row, (i+1),14, processoAtual.getnom);
			setCell(row, 15, utilCnpj.getFormatado());
			setCell(row, 16, processoAtual.getNomeAssunto());
			setCell(row, 17, processoAtual.getDataMovimento());
		}
		excelFile.close();
		return workbook;
	}

	public void setCell(XSSFRow row, int nCell, Object valor) throws TipoNaoImplementadoException {
		XSSFCell cell = row.createCell(nCell);
		if (valor == null)
			cell.setCellValue("NULL");
		else if (valor instanceof Double)
			cell.setCellValue((Double) valor);
		else if (valor instanceof Integer)
			cell.setCellValue((Integer) valor);
		else if (valor instanceof String)
			cell.setCellValue((String) valor);
		else if (valor instanceof LocalDate)
			cell.setCellValue(valor.toString());
		else
			throw new TipoNaoImplementadoException("Esse tipo ainda não foi implementado");
	}
}
