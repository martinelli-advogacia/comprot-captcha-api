package br.com.martinelliadvogados.martinellifront.dto;

import java.util.Set;

public class HealthCheckDTO {
	Set<CheckDTO> checks;

	public HealthCheckDTO() {
		super();
	}

	public Set<CheckDTO> getChecks() {
		return checks;
	}

	public void setChecks(Set<CheckDTO> checks) {
		this.checks = checks;
	}

}
