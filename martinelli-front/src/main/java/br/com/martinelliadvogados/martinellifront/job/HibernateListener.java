package br.com.martinelliadvogados.martinellifront.job;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;

public class HibernateListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        // Fechar SessionFactory do Hibernate ao parar aplicação
        HibernateUtil.shutdown();
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // Criar SessionFactory do Hibernate ao subir aplicação
        try {
            HibernateUtil.createEntityManagerFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
