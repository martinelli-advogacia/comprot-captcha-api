package br.com.martinelliadvogados.martinellifront.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.Cliente;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.persistence.ClienteDAO;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;
import com.hepta.martinelliadvogados.shared.utils.CNPJUtil;

public class ClienteExtDAO extends ClienteDAO {
	private static final Logger LOG = LogManager.getLogger(ClienteExtDAO.class);
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public Cliente porPrefixo(String cnpj) throws Exception {
		CNPJUtil util = new CNPJUtil(cnpj);

		EntityManager em = HibernateUtil.getEntityManager();
		Cliente res = null;
		try {
			String sql = " SELECT c.* from Cliente c "
					+ " left join Filial f on c.ID = f.FK_FILIAL_CLIENTE "
					+ " where c.DS_CNPJ_PREFIXO =:prefix ";
			Query query = em.createNativeQuery(sql, Cliente.class);
			res = (Cliente) query.setParameter("prefix", util.getPrefix()).getSingleResult();
			if (res != null) {
				String sql2 = " SELECT f.* from Filial f " + " where f.FK_FILIAL_CLIENTE =:idCliente ";
				Query query2 = em.createNativeQuery(sql2, Filial.class);
				List<Filial> filiais = query2.setParameter("idCliente", res.getId()).getResultList();
				res.setFiliais(filiais);
			}
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
		return res;
	}
}
