package br.com.martinelliadvogados.martinellifront.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import br.com.martinelliadvogados.martinellifront.cors.CorsFilter;

@ApplicationPath("/rs")
public class RESTApplication extends ResourceConfig {

    public RESTApplication() {
        packages("br.com.martinelliadvogados.martinellifront.rest");
        register(MultiPartFeature.class);
        register(CorsFilter.class);
    }
}

