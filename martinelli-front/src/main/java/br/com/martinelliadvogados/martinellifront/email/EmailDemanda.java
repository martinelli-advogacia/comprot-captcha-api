package br.com.martinelliadvogados.martinellifront.email;

import br.com.martinelliadvogados.martinellifront.dto.CamposEmailDemandaFinalizadaDTO;

public class EmailDemanda extends BaseEmail {
	private static String insereValores(CamposEmailDemandaFinalizadaDTO campos, StringBuilder str) {
		String template = str.toString();

		// Setando os parâmetros do template
		template = template.replaceAll("\\$\\{nomeFuncionario}", campos.nomeDestino);
		template = template.replaceAll("\\$\\{emailFuncionario}", campos.emailDestinatario);

		return template.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
	}

	public static String getTemplate(CamposEmailDemandaFinalizadaDTO campos) {

		StringBuilder str = new StringBuilder();

		//str.append(emailHeader());

		str.append("Resultados da atualização sob demanda");

		//str.append(emailFooter());

		return insereValores(campos, str);
	}
}
