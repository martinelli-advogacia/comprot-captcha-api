package br.com.martinelliadvogados.martinellifront.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.integration.ComprotRequest;

import br.com.martinelliadvogados.martinellifront.dto.DemandaDTO;
import br.com.martinelliadvogados.martinellifront.persistence.FilialExtDAO;
import br.com.martinelliadvogados.martinellifront.persistence.ProcessoExtDAO;
import br.com.martinelliadvogados.martinellifront.service.comprot.AtualizarProcessosServiceNIN;
import br.com.martinelliadvogados.martinellifront.service.comprot.MuitosProcessosServiceNIN;
import br.com.martinelliadvogados.martinellifront.service.comprot.NovosProcessosServiceNIN;
import br.com.martinelliadvogados.martinellifront.utils.exceptions.HeptaException;

public class ComprotService {

	private static final Logger LOG = Logger.getLogger(ComprotService.class);

	ComprotRequest requestComprot = new ComprotRequest();
	FilialExtDAO filialDAO = new FilialExtDAO();
	ProcessoExtDAO processoDAO = new ProcessoExtDAO();

	NovosProcessosServiceNIN novoService = new NovosProcessosServiceNIN();
	MuitosProcessosServiceNIN muitosService = new MuitosProcessosServiceNIN();
	AtualizarProcessosServiceNIN atualizarService = new AtualizarProcessosServiceNIN();

	public String buscaPorDemandaComprot(DemandaDTO demanda) throws Exception {
		if (demanda.getFiliais() == null || demanda.getFiliais().isEmpty())
			throw new HeptaException("Dados inválidos", Status.BAD_REQUEST);
		if (demanda.getDias() != null && demanda.getDias() < 0)
			throw new HeptaException("Dados inválidos", Status.BAD_REQUEST);
		Instant agora = Instant.now();
		Instant inicio;

		if (demanda.getDias() == null)
			inicio = Instant.ofEpochMilli(0L);
		else
			inicio = agora.minus(demanda.getDias(), ChronoUnit.DAYS);

		filialDAO.achaOuCria(demanda.getFiliais());

		int resNovos = novoService.pesquisarNovosProcessos(inicio.toEpochMilli(), agora.toEpochMilli(),
				demanda.getFiliais());
		int resMuitos = muitosService.pegarMuitosProcessos(demanda.getFiliais());
		int resCompletos = atualizarService.atualizarProcessos(demanda.getFiliais());

		return "Novos:" + resNovos + " Muitos:" + resMuitos + " Completos:" + resCompletos;
	}
}
