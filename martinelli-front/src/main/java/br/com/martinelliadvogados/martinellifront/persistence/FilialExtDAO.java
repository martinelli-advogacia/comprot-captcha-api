package br.com.martinelliadvogados.martinellifront.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.Cliente;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Erro;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.FilialDAO;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;
import com.hepta.martinelliadvogados.shared.utils.CNPJUtil;

import br.com.martinelliadvogados.martinellifront.dto.FilialDTO;

public class FilialExtDAO extends FilialDAO {
	private static final Logger LOG = LogManager.getLogger(FilialExtDAO.class);

	private static final long serialVersionUID = 1L;

	public void achaOuCria(Set<FilialDTO> set) throws Exception {
		Set<String> cnpjs = new HashSet<>();
		for (FilialDTO f : set) {
			cnpjs.add(f.getCnpjCompleto());
		}
		List<Filial> filiaisExistentes = porCNPJ(cnpjs);
		Map<String, Filial> mapFiliaisExistentes = new HashMap<>();
		for (Filial f : filiaisExistentes) {
			mapFiliaisExistentes.put(f.getCnpj(), f);
		}
		ClienteExtDAO daoCliente = new ClienteExtDAO();
		for (FilialDTO f : set) {
			if (!mapFiliaisExistentes.containsKey(f.getCnpjCompleto())) {
				Cliente clienteBanco = daoCliente.porPrefixo(f.getCnpjCompleto());
				CNPJUtil util = new CNPJUtil(f.getCnpjCompleto());
				if (clienteBanco == null) {
					clienteBanco = new Cliente();
					clienteBanco.setCnpjPrefixo(util.getPrefix());
					clienteBanco.setFiliais(new ArrayList<>());
					daoCliente.save(clienteBanco);
				}
				Filial novaF = new Filial();
				novaF.setCliente(clienteBanco);
				novaF.setCnpj(f.getCnpjCompleto());
				novaF.setCnpjSufixo(util.getSuffix());
				novaF.setDataConsulta(null);
				novaF.setTipo(TipoCliente.CASA);// TODO: deveria ser ecolhido pelo usuário?
				novaF.setErro(null);
				clienteBanco.getFiliais().add(novaF);
				daoCliente.update(clienteBanco);
			}
		}

	}

	@SuppressWarnings("unchecked")
	public List<Filial> porCNPJ(Set<String> cnpjs) throws Exception {
		final List<Filial> filiais = new ArrayList<>();
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			String sql = " SELECT f.* from Filial f " + " where f.DS_CNPJ in (:cnpjs)";

			filiais.addAll(em.createNativeQuery(sql, Filial.class).setParameter("cnpjs", cnpjs).getResultList());
			return filiais;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Filial> porErro(int range, Erro erro) throws Exception {
		final List<Filial> filiais = new ArrayList<>();
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			String sql = " SELECT f.* from Filial f "
					+ " left join Cliente c on c.ID = f.FK_FILIAL_CLIENTE "
					+ " where c.TP_ERRO = :erro"
					+ " order by c.TP_PRIORIDADE asc , c.DT_DATA_CONSULTA asc";

			filiais.addAll(em	.createNativeQuery(sql, Filial.class)
								.setParameter("erro", erro.pegarValor())
								.setFirstResult(0)
								.setMaxResults(range)
								.getResultList());
			return filiais;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Filial> porErroECNPJ(Set<String> cnpjs, Erro erro) throws Exception {
		final List<Filial> filiais = new ArrayList<>();
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			String sql = " SELECT f.* from Filial f "
					+ " left join Cliente c on c.ID = f.FK_FILIAL_CLIENTE "
					+ " where f.DS_CNPJ in (:cnpjs) "
					+ "and c.TP_ERRO = :erro ";

			filiais.addAll(em	.createNativeQuery(sql, Filial.class)
								.setParameter("cnpjs", cnpjs)
								.setParameter("erro", erro.pegarValor())
								.getResultList());
			return filiais;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	public Filial buscarPorCnpj(String cnpj) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			String sql = " SELECT f from Filial f " + " left join f.cliente c " + " where f.cnpj = :cnpj ";
			Query query = em.createQuery(sql).setParameter("cnpj", cnpj);
			Filial filial = (Filial) query.getSingleResult();
			return filial;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Filial> buscarPorCnpj(Set<String> cnpjs) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			String sql = " SELECT f from Filial f " + " where f.cnpj in(:cnpjs) ";
			Query query = em.createQuery(sql).setParameter("cnpjs", cnpjs);

			return query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	public void criarFilial(FilialDTO filial) {
		// TODO: criar filial
	}

	public void deletarPorCnpj(String cnpj) {
		// TODO: deletar logicamente filial por cnpj
	}

	public void editarFilial(FilialDTO filial) {
		// TODO: editar Filial
	}

}
