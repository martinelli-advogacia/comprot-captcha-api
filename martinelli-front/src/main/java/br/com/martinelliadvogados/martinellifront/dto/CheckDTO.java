package br.com.martinelliadvogados.martinellifront.dto;

public class CheckDTO {
	String status;
	String name;

	public CheckDTO() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}