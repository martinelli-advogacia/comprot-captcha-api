package br.com.martinelliadvogados.martinellifront.utils.exceptions;

public class TipoNaoImplementadoException extends Exception {
	private static final long serialVersionUID = 1L;

	public TipoNaoImplementadoException(String errorMessage) {
		super(errorMessage);
	}
}
