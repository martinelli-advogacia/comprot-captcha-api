package br.com.martinelliadvogados.martinellifront.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class FilialInexistenteException extends HeptaException {

	private static final long serialVersionUID = 1L;

	public FilialInexistenteException() {
		super("Não existe uma filial com esse cnpj", Status.NOT_FOUND);
	}

}