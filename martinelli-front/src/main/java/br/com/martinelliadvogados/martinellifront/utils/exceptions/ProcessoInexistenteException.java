package br.com.martinelliadvogados.martinellifront.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class ProcessoInexistenteException extends HeptaException {

	private static final long serialVersionUID = 1L;

	public ProcessoInexistenteException() {
		super("Nenhum processo encontrado", Status.NOT_FOUND);
	}

}