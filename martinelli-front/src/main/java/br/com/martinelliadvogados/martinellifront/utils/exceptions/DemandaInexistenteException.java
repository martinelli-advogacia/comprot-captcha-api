package br.com.martinelliadvogados.martinellifront.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class DemandaInexistenteException extends HeptaException {

	private static final long serialVersionUID = 1L;

	public DemandaInexistenteException() {
		super("Demanda não existe", Status.INTERNAL_SERVER_ERROR);
	}

}