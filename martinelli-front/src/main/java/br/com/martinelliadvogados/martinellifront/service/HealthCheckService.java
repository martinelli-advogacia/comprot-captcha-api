package br.com.martinelliadvogados.martinellifront.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hepta.martinelliadvogados.shared.entity.JobStatus;
import com.hepta.martinelliadvogados.shared.persistence.JobStatusDAO;

import br.com.martinelliadvogados.martinellifront.dto.CheckDTO;
import br.com.martinelliadvogados.martinellifront.dto.HealthCheckDTO;

public class HealthCheckService {
	JobStatusDAO dao = new JobStatusDAO();

	public HealthCheckDTO checkHealth() throws Exception {
		List<JobStatus> status = dao.getAll();

		Set<CheckDTO> checks = new HashSet<>();
		for (JobStatus s : status) {
			CheckDTO c = new CheckDTO();
			c.setName(s.getJobName());
			LocalDateTime agora = LocalDateTime.now();
			Duration dur = Duration.between(agora, s.getDataInicio());
			if (dur.get(ChronoUnit.SECONDS) < 900) {
				c.setStatus("UP");
			} else {
				c.setStatus("DOWN");
			}
			checks.add(c);
		}

		HealthCheckDTO health = new HealthCheckDTO();
		health.setChecks(checks);
		return health;
	}

}
