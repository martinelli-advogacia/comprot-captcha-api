package com.hepta.martinelliadvogados.shared.serializers;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;

public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

	@Override
	public LocalDate deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
		try {
			ObjectCodec codec = jp.getCodec();
			TextNode node = (TextNode) codec.readTree(jp);
			String dateString = node.textValue();
			return LocalDate.parse(dateString);
		} catch (Exception e) {
			return null;
		}
	}

}
