package com.hepta.martinelliadvogados.shared.serializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;

public class CNPJDeserializer extends JsonDeserializer<String> {

	@Override
	public String deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
		try {
			ObjectCodec codec = jp.getCodec();
			TextNode node = (TextNode) codec.readTree(jp);

			String str = node.textValue();
			return cnpjStringToString(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String cnpjStringToString(String str) {
		String semSymb = str.replaceAll("[-/.]", "");
		if (semSymb.length() == 14) {
			return semSymb;
		} else {
			return "0".repeat(14 - semSymb.length()) + semSymb;
		}
	}

}
