package com.hepta.martinelliadvogados.shared.serializers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.TextNode;

public class ComprotDateDeserializer extends JsonDeserializer<LocalDate> {

    @Override
    public LocalDate deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        try {
            ObjectCodec codec = jp.getCodec();
            String dateString = null;
            TreeNode node = codec.readTree(jp);
            if (node.isValueNode()) {
                try {
                    TextNode txtnode = (TextNode) node;
                    dateString = txtnode.textValue();
                } catch (Exception e) {
                    IntNode intNode = (IntNode) node;
                    dateString = intNode.asInt() + "";
                    if (dateString.length() == 7) {
                        dateString = "0".concat(dateString);
                    }
                }
            }

            if (dateString.isEmpty() || dateString == null)
                return null;
            try {
                return comprotDateToLocalDateTipo1(dateString);
            } catch (Exception e) {
                try {
                    return comprotDateToLocalDateTipo2(dateString);
                } catch (Exception e1) {
                    try {
                        return comprotDateToLocalDateTipo3(dateString);
                    } catch (Exception e3) {
                        throw e3;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static LocalDate comprotDateToLocalDateTipo1(String str) {
        if (str == null || str.equals("0"))
            return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        return LocalDate.parse(str, formatter);
    }

    public static LocalDate comprotDateToLocalDateTipo2(String str) {
        if (str == null || str.equals("0"))
            return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(str, formatter);
    }

    public static LocalDate comprotDateToLocalDateTipo3(String str) {
        if (str == null || str.equals("0"))
            return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
        return LocalDate.parse(str, formatter);
    }

}
