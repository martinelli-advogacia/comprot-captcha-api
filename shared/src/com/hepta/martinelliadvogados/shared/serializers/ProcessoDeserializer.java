package com.hepta.martinelliadvogados.shared.serializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;

public class ProcessoDeserializer extends JsonDeserializer<String> {

	@Override
	public String deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
		try {
			ObjectCodec codec = jp.getCodec();
			TextNode node = (TextNode) codec.readTree(jp);
			String str = node.textValue();
			return processoStringToString(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String processoStringToString(String str) {
		return str.replaceAll("[-/.]", "");
	}

}
