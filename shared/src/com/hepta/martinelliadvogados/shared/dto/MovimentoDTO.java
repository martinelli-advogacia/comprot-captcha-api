package com.hepta.martinelliadvogados.shared.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hepta.martinelliadvogados.shared.entity.Movimento;
import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.serializers.ComprotDateDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MovimentoDTO {

	@JsonDeserialize(using = ComprotDateDeserializer.class)
	LocalDate dataMovimentoEditada;

	String codigoTipoMovimento;
	String nomeTipoMovimento;
	String numeroSequencia;
	String numeroRMRAAJDJ;
	Long codigoOrgaoOrigem;
	String nomeOrgaoOrigem;
	Long codigoOrgaoDestino;
	String nomeOrgaoDestino;
	String nomeDespacho;
	Long codigoDespacho;
	String numeroCaixa;
	Long codigoTemporalidade;
	String nomeTemporalidade;
	String numeroProcessoPrincipal;

	public MovimentoDTO() {
		super();
	}

	public MovimentoDTO(Movimento m) {
		this.dataMovimentoEditada = m.getDataMovimentoEditada();
		this.codigoTipoMovimento = m.getCodigoTipoMovimento();
		this.nomeTipoMovimento = m.getNomeTipoMovimento();
		this.numeroSequencia = m.getNumeroSequencia();
		this.numeroRMRAAJDJ = m.getNumeroRMRAAJDJ();
		this.codigoOrgaoOrigem = m.getCodigoOrgaoOrigem();
		this.nomeOrgaoOrigem = m.getNomeOrgaoOrigem();
		this.codigoOrgaoDestino = m.getCodigoOrgaoDestino();
		this.nomeOrgaoDestino = m.getNomeOrgaoDestino();
		this.nomeDespacho = m.getNomeDespacho();
		this.codigoDespacho = m.getCodigoDespacho();
		this.numeroCaixa = m.getNumeroCaixa();
		this.codigoTemporalidade = m.getCodigoTemporalidade();
		this.nomeTemporalidade = m.getNomeTemporalidade();
		this.numeroProcessoPrincipal = m.getNumeroProcessoPrincipal();
	}

	@JsonIgnore
	public Movimento toEntity(Processo processo) {
		Movimento mov = new Movimento();
		mov.setProcesso(processo);
		mov.setDataMovimentoEditada(this.dataMovimentoEditada);
		mov.setCodigoOrgaoOrigem(this.codigoOrgaoOrigem);
		mov.setCodigoOrgaoDestino(this.codigoOrgaoDestino);
		mov.setCodigoDespacho(this.codigoDespacho);
		mov.setCodigoTemporalidade(this.codigoTemporalidade);
		mov.setCodigoTipoMovimento(this.codigoTipoMovimento);
		mov.setNomeTipoMovimento(this.nomeTipoMovimento);
		mov.setNumeroSequencia(this.numeroSequencia);
		mov.setNumeroRMRAAJDJ(this.numeroRMRAAJDJ);
		mov.setNomeOrgaoOrigem(this.nomeOrgaoOrigem);
		mov.setNomeOrgaoDestino(this.nomeOrgaoDestino);
		mov.setNomeDespacho(this.nomeDespacho);
		mov.setNumeroCaixa(this.numeroCaixa);
		mov.setNomeTemporalidade(this.nomeTemporalidade);
		mov.setNumeroProcessoPrincipal(this.numeroProcessoPrincipal);
		return mov;
	}

	public LocalDate getDataMovimentoEditada() {
		return dataMovimentoEditada;
	}

	public void setDataMovimentoEditada(LocalDate dataMovimentoEditada) {
		this.dataMovimentoEditada = dataMovimentoEditada;
	}

	public String getCodigoTipoMovimento() {
		return codigoTipoMovimento;
	}

	public void setCodigoTipoMovimento(String codigoTipoMovimento) {
		this.codigoTipoMovimento = codigoTipoMovimento;
	}

	public String getNomeTipoMovimento() {
		return nomeTipoMovimento;
	}

	public void setNomeTipoMovimento(String nomeTipoMovimento) {
		this.nomeTipoMovimento = nomeTipoMovimento;
	}

	public String getNumeroSequencia() {
		return numeroSequencia;
	}

	public void setNumeroSequencia(String numeroSequencia) {
		this.numeroSequencia = numeroSequencia;
	}

	public String getNumeroRMRAAJDJ() {
		return numeroRMRAAJDJ;
	}

	public void setNumeroRMRAAJDJ(String numeroRMRAAJDJ) {
		this.numeroRMRAAJDJ = numeroRMRAAJDJ;
	}

	public Long getCodigoOrgaoOrigem() {
		return codigoOrgaoOrigem;
	}

	public void setCodigoOrgaoOrigem(Long codigoOrgaoOrigem) {
		this.codigoOrgaoOrigem = codigoOrgaoOrigem;
	}

	public String getNomeOrgaoOrigem() {
		return nomeOrgaoOrigem;
	}

	public void setNomeOrgaoOrigem(String nomeOrgaoOrigem) {
		this.nomeOrgaoOrigem = nomeOrgaoOrigem;
	}

	public Long getCodigoOrgaoDestino() {
		return codigoOrgaoDestino;
	}

	public void setCodigoOrgaoDestino(Long codigoOrgaoDestino) {
		this.codigoOrgaoDestino = codigoOrgaoDestino;
	}

	public String getNomeOrgaoDestino() {
		return nomeOrgaoDestino;
	}

	public void setNomeOrgaoDestino(String nomeOrgaoDestino) {
		this.nomeOrgaoDestino = nomeOrgaoDestino;
	}

	public String getNomeDespacho() {
		return nomeDespacho;
	}

	public void setNomeDespacho(String nomeDespacho) {
		this.nomeDespacho = nomeDespacho;
	}

	public Long getCodigoDespacho() {
		return codigoDespacho;
	}

	public void setCodigoDespacho(Long codigoDespacho) {
		this.codigoDespacho = codigoDespacho;
	}

	public String getNumeroCaixa() {
		return numeroCaixa;
	}

	public void setNumeroCaixa(String numeroCaixa) {
		this.numeroCaixa = numeroCaixa;
	}

	public Long getCodigoTemporalidade() {
		return codigoTemporalidade;
	}

	public void setCodigoTemporalidade(Long codigoTemporalidade) {
		this.codigoTemporalidade = codigoTemporalidade;
	}

	public String getNomeTemporalidade() {
		return nomeTemporalidade;
	}

	public void setNomeTemporalidade(String nomeTemporalidade) {
		this.nomeTemporalidade = nomeTemporalidade;
	}

	public String getNumeroProcessoPrincipal() {
		return numeroProcessoPrincipal;
	}

	public void setNumeroProcessoPrincipal(String numeroProcessoPrincipal) {
		this.numeroProcessoPrincipal = numeroProcessoPrincipal;
	}

}
