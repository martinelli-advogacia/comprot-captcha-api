package com.hepta.martinelliadvogados.shared.dto;

import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

import com.hepta.martinelliadvogados.shared.entity.Filial;

public class ListaProcessoFilialDTO {

    CompletableFuture<HttpResponse<String>> httpResponse;
    Filial filial;

    public CompletableFuture<HttpResponse<String>> getHttpResponse() {
        return httpResponse;
    }

    public void setHttpResponse(CompletableFuture<HttpResponse<String>> httpResponse) {
        this.httpResponse = httpResponse;
    }

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }
}
