package com.hepta.martinelliadvogados.shared.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessoWrapper {
	ProcessoDTO processo;
	List<MovimentoDTO> movimentos;
	List<Object> posicionamentos;
	String mensagemErroMovimento;
	String mensagemErroPosicionamento;

	public ProcessoWrapper() {
		super();
	}

	public List<MovimentoDTO> getMovimentos() {
		return movimentos;
	}

	public void setMovimentos(List<MovimentoDTO> movimentos) {
		this.movimentos = movimentos;
	}

	public ProcessoDTO getProcesso() {
		return processo;
	}

	public void setProcesso(ProcessoDTO processo) {
		this.processo = processo;
	}

	public List<Object> getPosicionamentos() {
		return posicionamentos;
	}

	public void setPosicionamentos(List<Object> posicionamentos) {
		this.posicionamentos = posicionamentos;
	}

	public String getMensagemErroMovimento() {
		return mensagemErroMovimento;
	}

	public void setMensagemErroMovimento(String mensagemErroMovimento) {
		this.mensagemErroMovimento = mensagemErroMovimento;
	}

	public String getMensagemErroPosicionamento() {
		return mensagemErroPosicionamento;
	}

	public void setMensagemErroPosicionamento(String mensagemErroPosicionamento) {
		this.mensagemErroPosicionamento = mensagemErroPosicionamento;
	}

}
