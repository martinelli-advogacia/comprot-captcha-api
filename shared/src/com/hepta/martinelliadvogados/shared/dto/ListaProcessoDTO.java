package com.hepta.martinelliadvogados.shared.dto;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hepta.martinelliadvogados.shared.serializers.ComprotDateDeserializer;
import com.hepta.martinelliadvogados.shared.serializers.ProcessoDeserializer;

public class ListaProcessoDTO {
	String indicadorProcessoCancelado = "";
	String indicadorProcessoEliminado = "";
	String indicadorProcessoExcluido = "";
	String dataExclusaoProcesso = "";
	String numeroDocumentoExclusao = "";
	String codigoTipoMovimentoProcesso = "";
	String codigoTipoMovimentoMovimento = "";
	@JsonDeserialize(using = ProcessoDeserializer.class)
	String numeroProcessoEditado = "";
	String sgOtroClienteProc = "";
	String indicadorProcessoPrincipal = "";
	String leiFormacao = "";
	String numeroDocumentoOrigem = "";
	String numeroAntigoProcesso = "";
	String nomeInteressado = "";
	String numeroCpfCgc = "";
	String nomeAssunto = "";
	String sgOutroClienteAssunto = "";
	String dataJuntada = "";
	String numeroProcessoPrincipalRaiz = "";
	String numeroProcessoPrincipalAj = "";
	String nomeInteressadoRaiz = "";
	String nomeAssuntoRaiz = "";
	String numeroSequencia = "";
	String numeroRelacao = "";
	String nomeOrgaoOrigem = "";
	String nomeOrgaoDestino = "";
	String nomeOutroOrgao = "";
	String siglaUfMovimento = "";
	String nomeDespacho = "";
	String numeroSequenciaDisjuntada = "";
	String numeroProcessoPrincipal = "";
	String nomeOrgaoDisjuntada = "";
	String nomeTemporalidade = "";
	String dataProcessamentoBCI = "";
	String nomeOrgaoProtocolo = "";
	String moeda = "";
	String nomeMoeda = "";
	String valorRecursoFinanceiro = "";
	String textoObservacao = "";
	String nomeProcedencia = "";
	String codigoClientePesquisa = "";
	String situacao = "";
	String indicadorProfisc = "";
	String indicadorVirtual = "";

	Integer indicadorCpfCgc = -1;
	Integer indicadorProcessoTransito = -1;
	Integer numeroMalote = -1;
	Integer numeroRegiaoPostal = -1;
	Integer numeroAviso = -1;
	Integer numeroCaixaArquivo = -1;
	Integer indicadorSief = -1;
	Integer indicadorEProcesso = -1;

	@JsonDeserialize(using = ComprotDateDeserializer.class)
	LocalDate dataMovimento;
	@JsonDeserialize(using = ComprotDateDeserializer.class)
	LocalDate dataDisjuntada = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();
	@JsonDeserialize(using = ComprotDateDeserializer.class)
	LocalDate dataProtocolo = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();

	public ListaProcessoDTO() {
		super();
	}

	@JsonIgnore
	public String getNumeroProcesso() {
		if (numeroProcessoPrincipal == null || numeroProcessoPrincipal.isEmpty()) {
			return numeroProcessoEditado;
		} else
			return numeroProcessoPrincipal;
	}

	public String getIndicadorProcessoCancelado() {
		return indicadorProcessoCancelado;
	}

	public void setIndicadorProcessoCancelado(String indicadorProcessoCancelado) {
		this.indicadorProcessoCancelado = indicadorProcessoCancelado;
	}

	public String getIndicadorProcessoEliminado() {
		return indicadorProcessoEliminado;
	}

	public void setIndicadorProcessoEliminado(String indicadorProcessoEliminado) {
		this.indicadorProcessoEliminado = indicadorProcessoEliminado;
	}

	public String getIndicadorProcessoExcluido() {
		return indicadorProcessoExcluido;
	}

	public void setIndicadorProcessoExcluido(String indicadorProcessoExcluido) {
		this.indicadorProcessoExcluido = indicadorProcessoExcluido;
	}

	public String getDataExclusaoProcesso() {
		return dataExclusaoProcesso;
	}

	public void setDataExclusaoProcesso(String dataExclusaoProcesso) {
		this.dataExclusaoProcesso = dataExclusaoProcesso;
	}

	public String getNumeroDocumentoExclusao() {
		return numeroDocumentoExclusao;
	}

	public void setNumeroDocumentoExclusao(String numeroDocumentoExclusao) {
		this.numeroDocumentoExclusao = numeroDocumentoExclusao;
	}

	public String getCodigoTipoMovimentoProcesso() {
		return codigoTipoMovimentoProcesso;
	}

	public void setCodigoTipoMovimentoProcesso(String codigoTipoMovimentoProcesso) {
		this.codigoTipoMovimentoProcesso = codigoTipoMovimentoProcesso;
	}

	public String getCodigoTipoMovimentoMovimento() {
		return codigoTipoMovimentoMovimento;
	}

	public void setCodigoTipoMovimentoMovimento(String codigoTipoMovimentoMovimento) {
		this.codigoTipoMovimentoMovimento = codigoTipoMovimentoMovimento;
	}

	public String getNumeroProcessoEditado() {
		return numeroProcessoEditado;
	}

	public void setNumeroProcessoEditado(String numeroProcessoEditado) {
		this.numeroProcessoEditado = numeroProcessoEditado;
	}

	public String getSgOtroClienteProc() {
		return sgOtroClienteProc;
	}

	public void setSgOtroClienteProc(String sgOtroClienteProc) {
		this.sgOtroClienteProc = sgOtroClienteProc;
	}

	public String getIndicadorProcessoPrincipal() {
		return indicadorProcessoPrincipal;
	}

	public void setIndicadorProcessoPrincipal(String indicadorProcessoPrincipal) {
		this.indicadorProcessoPrincipal = indicadorProcessoPrincipal;
	}

	public String getLeiFormacao() {
		return leiFormacao;
	}

	public void setLeiFormacao(String leiFormacao) {
		this.leiFormacao = leiFormacao;
	}

	public String getNumeroDocumentoOrigem() {
		return numeroDocumentoOrigem;
	}

	public void setNumeroDocumentoOrigem(String numeroDocumentoOrigem) {
		this.numeroDocumentoOrigem = numeroDocumentoOrigem;
	}

	public String getNumeroAntigoProcesso() {
		return numeroAntigoProcesso;
	}

	public void setNumeroAntigoProcesso(String numeroAntigoProcesso) {
		this.numeroAntigoProcesso = numeroAntigoProcesso;
	}

	public String getNomeInteressado() {
		return nomeInteressado;
	}

	public void setNomeInteressado(String nomeInteressado) {
		this.nomeInteressado = nomeInteressado;
	}

	public Integer getIndicadorCpfCgc() {
		return indicadorCpfCgc;
	}

	public void setIndicadorCpfCgc(Integer indicadorCpfCgc) {
		this.indicadorCpfCgc = indicadorCpfCgc;
	}

	public String getNumeroCpfCgc() {
		return numeroCpfCgc;
	}

	public void setNumeroCpfCgc(String numeroCpfCgc) {
		this.numeroCpfCgc = numeroCpfCgc;
	}

	public String getNomeAssunto() {
		return nomeAssunto;
	}

	public void setNomeAssunto(String nomeAssunto) {
		this.nomeAssunto = nomeAssunto;
	}

	public String getSgOutroClienteAssunto() {
		return sgOutroClienteAssunto;
	}

	public void setSgOutroClienteAssunto(String sgOutroClienteAssunto) {
		this.sgOutroClienteAssunto = sgOutroClienteAssunto;
	}

	public String getDataJuntada() {
		return dataJuntada;
	}

	public void setDataJuntada(String dataJuntada) {
		this.dataJuntada = dataJuntada;
	}

	public String getNumeroProcessoPrincipalRaiz() {
		return numeroProcessoPrincipalRaiz;
	}

	public void setNumeroProcessoPrincipalRaiz(String numeroProcessoPrincipalRaiz) {
		this.numeroProcessoPrincipalRaiz = numeroProcessoPrincipalRaiz;
	}

	public String getNumeroProcessoPrincipalAj() {
		return numeroProcessoPrincipalAj;
	}

	public void setNumeroProcessoPrincipalAj(String numeroProcessoPrincipalAj) {
		this.numeroProcessoPrincipalAj = numeroProcessoPrincipalAj;
	}

	public String getNomeInteressadoRaiz() {
		return nomeInteressadoRaiz;
	}

	public void setNomeInteressadoRaiz(String nomeInteressadoRaiz) {
		this.nomeInteressadoRaiz = nomeInteressadoRaiz;
	}

	public String getNomeAssuntoRaiz() {
		return nomeAssuntoRaiz;
	}

	public void setNomeAssuntoRaiz(String nomeAssuntoRaiz) {
		this.nomeAssuntoRaiz = nomeAssuntoRaiz;
	}

	public Integer getIndicadorProcessoTransito() {
		return indicadorProcessoTransito;
	}

	public void setIndicadorProcessoTransito(Integer indicadorProcessoTransito) {
		this.indicadorProcessoTransito = indicadorProcessoTransito;
	}

	public LocalDate getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(LocalDate dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

	public String getNumeroSequencia() {
		return numeroSequencia;
	}

	public void setNumeroSequencia(String numeroSequencia) {
		this.numeroSequencia = numeroSequencia;
	}

	public String getNumeroRelacao() {
		return numeroRelacao;
	}

	public void setNumeroRelacao(String numeroRelacao) {
		this.numeroRelacao = numeroRelacao;
	}

	public Integer getNumeroMalote() {
		return numeroMalote;
	}

	public void setNumeroMalote(Integer numeroMalote) {
		this.numeroMalote = numeroMalote;
	}

	public Integer getNumeroRegiaoPostal() {
		return numeroRegiaoPostal;
	}

	public void setNumeroRegiaoPostal(Integer numeroRegiaoPostal) {
		this.numeroRegiaoPostal = numeroRegiaoPostal;
	}

	public String getNomeOrgaoOrigem() {
		return nomeOrgaoOrigem;
	}

	public void setNomeOrgaoOrigem(String nomeOrgaoOrigem) {
		this.nomeOrgaoOrigem = nomeOrgaoOrigem;
	}

	public String getNomeOrgaoDestino() {
		return nomeOrgaoDestino;
	}

	public void setNomeOrgaoDestino(String nomeOrgaoDestino) {
		this.nomeOrgaoDestino = nomeOrgaoDestino;
	}

	public String getNomeOutroOrgao() {
		return nomeOutroOrgao;
	}

	public void setNomeOutroOrgao(String nomeOutroOrgao) {
		this.nomeOutroOrgao = nomeOutroOrgao;
	}

	public String getSiglaUfMovimento() {
		return siglaUfMovimento;
	}

	public void setSiglaUfMovimento(String siglaUfMovimento) {
		this.siglaUfMovimento = siglaUfMovimento;
	}

	public String getNomeDespacho() {
		return nomeDespacho;
	}

	public void setNomeDespacho(String nomeDespacho) {
		this.nomeDespacho = nomeDespacho;
	}

	public LocalDate getDataDisjuntada() {
		return dataDisjuntada;
	}

	public void setDataDisjuntada(LocalDate dataDisjuntada) {
		this.dataDisjuntada = dataDisjuntada;
	}

	public String getNumeroSequenciaDisjuntada() {
		return numeroSequenciaDisjuntada;
	}

	public void setNumeroSequenciaDisjuntada(String numeroSequenciaDisjuntada) {
		this.numeroSequenciaDisjuntada = numeroSequenciaDisjuntada;
	}

	public Integer getNumeroAviso() {
		return numeroAviso;
	}

	public void setNumeroAviso(Integer numeroAviso) {
		this.numeroAviso = numeroAviso;
	}

	public String getNumeroProcessoPrincipal() {
		return numeroProcessoPrincipal;
	}

	public void setNumeroProcessoPrincipal(String numeroProcessoPrincipal) {
		this.numeroProcessoPrincipal = numeroProcessoPrincipal;
	}

	public String getNomeOrgaoDisjuntada() {
		return nomeOrgaoDisjuntada;
	}

	public void setNomeOrgaoDisjuntada(String nomeOrgaoDisjuntada) {
		this.nomeOrgaoDisjuntada = nomeOrgaoDisjuntada;
	}

	public Integer getNumeroCaixaArquivo() {
		return numeroCaixaArquivo;
	}

	public void setNumeroCaixaArquivo(Integer numeroCaixaArquivo) {
		this.numeroCaixaArquivo = numeroCaixaArquivo;
	}

	public String getNomeTemporalidade() {
		return nomeTemporalidade;
	}

	public void setNomeTemporalidade(String nomeTemporalidade) {
		this.nomeTemporalidade = nomeTemporalidade;
	}

	public LocalDate getDataProtocolo() {
		return dataProtocolo;
	}

	public void setDataProtocolo(LocalDate dataProtocolo) {
		this.dataProtocolo = dataProtocolo;
	}

	public String getDataProcessamentoBCI() {
		return dataProcessamentoBCI;
	}

	public void setDataProcessamentoBCI(String dataProcessamentoBCI) {
		this.dataProcessamentoBCI = dataProcessamentoBCI;
	}

	public String getNomeOrgaoProtocolo() {
		return nomeOrgaoProtocolo;
	}

	public void setNomeOrgaoProtocolo(String nomeOrgaoProtocolo) {
		this.nomeOrgaoProtocolo = nomeOrgaoProtocolo;
	}

	public String getMoeda() {
		return moeda;
	}

	public void setMoeda(String moeda) {
		this.moeda = moeda;
	}

	public String getNomeMoeda() {
		return nomeMoeda;
	}

	public void setNomeMoeda(String nomeMoeda) {
		this.nomeMoeda = nomeMoeda;
	}

	public String getValorRecursoFinanceiro() {
		return valorRecursoFinanceiro;
	}

	public void setValorRecursoFinanceiro(String valorRecursoFinanceiro) {
		this.valorRecursoFinanceiro = valorRecursoFinanceiro;
	}

	public String getTextoObservacao() {
		return textoObservacao;
	}

	public void setTextoObservacao(String textoObservacao) {
		this.textoObservacao = textoObservacao;
	}

	public String getNomeProcedencia() {
		return nomeProcedencia;
	}

	public void setNomeProcedencia(String nomeProcedencia) {
		this.nomeProcedencia = nomeProcedencia;
	}

	public String getCodigoClientePesquisa() {
		return codigoClientePesquisa;
	}

	public void setCodigoClientePesquisa(String codigoClientePesquisa) {
		this.codigoClientePesquisa = codigoClientePesquisa;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getIndicadorProfisc() {
		return indicadorProfisc;
	}

	public void setIndicadorProfisc(String indicadorProfisc) {
		this.indicadorProfisc = indicadorProfisc;
	}

	public Integer getIndicadorSief() {
		return indicadorSief;
	}

	public void setIndicadorSief(Integer indicadorSief) {
		this.indicadorSief = indicadorSief;
	}

	public String getIndicadorVirtual() {
		return indicadorVirtual;
	}

	public void setIndicadorVirtual(String indicadorVirtual) {
		this.indicadorVirtual = indicadorVirtual;
	}

	public Integer getIndicadorEProcesso() {
		return indicadorEProcesso;
	}

	public void setIndicadorEProcesso(Integer indicadorEProcesso) {
		this.indicadorEProcesso = indicadorEProcesso;
	}

}
