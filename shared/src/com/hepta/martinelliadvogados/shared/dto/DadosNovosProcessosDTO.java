package com.hepta.martinelliadvogados.shared.dto;

import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Erro;

public class DadosNovosProcessosDTO {
	Filial filial;
	HttpRequest request;
	HttpResponse<String> response;
	CompletableFuture<HttpResponse<String>> completableFuture;
	ListaProcessoWrapper wrapper;
	Erro erro;

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public HttpRequest getRequest() {
		return request;
	}

	public void setRequest(HttpRequest request) {
		this.request = request;
	}

	public CompletableFuture<HttpResponse<String>> getCompletableFuture() {
		return completableFuture;
	}

	public void setCompletableFuture(CompletableFuture<HttpResponse<String>> completableFuture) {
		this.completableFuture = completableFuture;
	}

	public ListaProcessoWrapper getWrapper() {
		return wrapper;
	}

	public void setWrapper(ListaProcessoWrapper wrapper) {
		this.wrapper = wrapper;
	}

	public Erro getErro() {
		return erro;
	}

	public void setErro(Erro erro) {
		this.erro = erro;
	}

	public HttpResponse<String> getResponse() {
		return response;
	}

	public void setResponse(HttpResponse<String> response) {
		this.response = response;
	}

}
