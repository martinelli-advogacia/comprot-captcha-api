package com.hepta.martinelliadvogados.shared.dto;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hepta.martinelliadvogados.shared.entity.Movimento;
import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.serializers.ComprotDateDeserializer;
import com.hepta.martinelliadvogados.shared.serializers.ProcessoDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessoDTO {
	@JsonDeserialize(using = ProcessoDeserializer.class)
	String numeroProcessoEditado = "";

	String numeroDocumentoOrigem = "";
	String nomeProcedencia = "";
	String nomeAssunto = "";
	String nomeInteressado = "";
	String numeroCpfCnpj = "";
	String nomeOrgaoOrigem = "";
	String nomeOrgaoDestino = "";
	String nomeOutroOrgao = "";
	String numeroSequencia = "";
	String numeroRelacao = "";
	String situacao = "";
	String siglaUfMovimento = "";
	String indicadorVirtual = "";
	String indicadorProfisc = "";
	String numeroSequenciaDisjuntada = "";
	String numeroProcessoPrincipal = "";
	String nomeOrgaoDisjuntada = "";
	String codigoTipoMovimentoProcesso = "";

	Integer indicadorCpfCnpj = -1;
	Integer indicadorEProcesso = -1;
	Integer indicadorSief = -1;
	Integer numeroAviso = -1;

	boolean dadosCompletos = false;

	@JsonDeserialize(using = ComprotDateDeserializer.class)
	LocalDate dataProtocolo = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();
	@JsonDeserialize(using = ComprotDateDeserializer.class)
	LocalDate dataMovimento = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();
	@JsonDeserialize(using = ComprotDateDeserializer.class)
	LocalDate dataDisjuntada = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();

	LocalDate dataConsulta;

	List<MovimentoDTO> movimentos;

	public ProcessoDTO() {
		super();
	}

	public ProcessoDTO(Processo p) {
		this.numeroProcessoEditado = p.getNumeroProcessoEditado();
		this.numeroDocumentoOrigem = p.getNumeroDocumentoOrigem();
		this.nomeProcedencia = p.getNomeProcedencia();
		this.nomeAssunto = p.getNomeAssunto();
		this.nomeInteressado = p.getNomeInteressado();
		this.numeroCpfCnpj = p.getNumeroCpfCnpj();
		this.nomeOrgaoOrigem = p.getNomeOrgaoOrigem();
		this.nomeOrgaoDestino = p.getNomeOrgaoDestino();
		this.nomeOutroOrgao = p.getNomeOutroOrgao();
		this.numeroSequencia = p.getNumeroSequencia();
		this.numeroRelacao = p.getNumeroRelacao();
		this.situacao = p.getSituacao();
		this.siglaUfMovimento = p.getSiglaUfMovimento();
		this.indicadorVirtual = p.getIndicadorVirtual();
		this.indicadorProfisc = p.getIndicadorProfisc();
		this.numeroSequenciaDisjuntada = p.getNumeroSequenciaDisjuntada();
		this.numeroProcessoPrincipal = p.getNumeroProcessoPrincipal();
		this.nomeOrgaoDisjuntada = p.getNomeOrgaoDisjuntada();
		this.codigoTipoMovimentoProcesso = p.getCodigoTipoMovimentoProcesso();
		this.dataProtocolo = p.getDataProtocolo();
		this.dataMovimento = p.getDataMovimento();
		this.dataDisjuntada = p.getDataDisjuntada();
		this.dataProtocolo = p.getDataProtocolo();
		this.indicadorCpfCnpj = p.getIndicadorCpfCnpj();
		this.indicadorEProcesso = p.getIndicadorEProcesso();
		this.indicadorSief = p.getIndicadorSief();
		this.numeroAviso = p.getNumeroAviso();
		this.dataConsulta = p.getDataConsulta();
		this.dadosCompletos = p.getDadosCompletos();

		this.movimentos = new ArrayList<>();
		if (p.getMovimentos() != null) {
			for (Movimento m : p.getMovimentos()) {
				movimentos.add(new MovimentoDTO(m));
			}
		}
	}

	@JsonIgnore
	public String getNumeroProcesso() {
		if (numeroProcessoPrincipal == null || numeroProcessoPrincipal.isEmpty()) {
			return numeroProcessoEditado;
		} else
			return numeroProcessoPrincipal;
	}

	public String getNumeroProcessoEditado() {
		return numeroProcessoEditado;
	}

	public void setNumeroProcessoEditado(String numeroProcessoEditado) {
		this.numeroProcessoEditado = numeroProcessoEditado;
	}

	public LocalDate getDataProtocolo() {
		return dataProtocolo;
	}

	public void setDataProtocolo(LocalDate dataProtocolo) {
		this.dataProtocolo = dataProtocolo;
	}

	public String getNumeroDocumentoOrigem() {
		return numeroDocumentoOrigem;
	}

	public void setNumeroDocumentoOrigem(String numeroDocumentoOrigem) {
		this.numeroDocumentoOrigem = numeroDocumentoOrigem;
	}

	public String getNomeProcedencia() {
		return nomeProcedencia;
	}

	public void setNomeProcedencia(String nomeProcedencia) {
		this.nomeProcedencia = nomeProcedencia;
	}

	public String getNomeAssunto() {
		return nomeAssunto;
	}

	public void setNomeAssunto(String nomeAssunto) {
		this.nomeAssunto = nomeAssunto;
	}

	public String getNomeInteressado() {
		return nomeInteressado;
	}

	public void setNomeInteressado(String nomeInteressado) {
		this.nomeInteressado = nomeInteressado;
	}

	public Integer getIndicadorCpfCnpj() {
		return indicadorCpfCnpj;
	}

	public void setIndicadorCpfCnpj(Integer indicadorCpfCnpj) {
		this.indicadorCpfCnpj = indicadorCpfCnpj;
	}

	public String getNumeroCpfCnpj() {
		return numeroCpfCnpj;
	}

	public void setNumeroCpfCnpj(String numeroCpfCnpj) {
		this.numeroCpfCnpj = numeroCpfCnpj;
	}

	public String getNomeOrgaoOrigem() {
		return nomeOrgaoOrigem;
	}

	public void setNomeOrgaoOrigem(String nomeOrgaoOrigem) {
		this.nomeOrgaoOrigem = nomeOrgaoOrigem;
	}

	public String getNomeOrgaoDestino() {
		return nomeOrgaoDestino;
	}

	public void setNomeOrgaoDestino(String nomeOrgaoDestino) {
		this.nomeOrgaoDestino = nomeOrgaoDestino;
	}

	public String getNomeOutroOrgao() {
		return nomeOutroOrgao;
	}

	public void setNomeOutroOrgao(String nomeOutroOrgao) {
		this.nomeOutroOrgao = nomeOutroOrgao;
	}

	public LocalDate getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(LocalDate dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

	public String getNumeroSequencia() {
		return numeroSequencia;
	}

	public void setNumeroSequencia(String numeroSequencia) {
		this.numeroSequencia = numeroSequencia;
	}

	public String getNumeroRelacao() {
		return numeroRelacao;
	}

	public void setNumeroRelacao(String numeroRelacao) {
		this.numeroRelacao = numeroRelacao;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getSiglaUfMovimento() {
		return siglaUfMovimento;
	}

	public void setSiglaUfMovimento(String siglaUfMovimento) {
		this.siglaUfMovimento = siglaUfMovimento;
	}

	public String getIndicadorVirtual() {
		return indicadorVirtual;
	}

	public void setIndicadorVirtual(String indicadorVirtual) {
		this.indicadorVirtual = indicadorVirtual;
	}

	public String getIndicadorProfisc() {
		return indicadorProfisc;
	}

	public void setIndicadorProfisc(String indicadorProfisc) {
		this.indicadorProfisc = indicadorProfisc;
	}

	public Integer getIndicadorEProcesso() {
		return indicadorEProcesso;
	}

	public void setIndicadorEProcesso(Integer indicadorEProcesso) {
		this.indicadorEProcesso = indicadorEProcesso;
	}

	public Integer getIndicadorSief() {
		return indicadorSief;
	}

	public void setIndicadorSief(Integer indicadorSief) {
		this.indicadorSief = indicadorSief;
	}

	public LocalDate getDataDisjuntada() {
		return dataDisjuntada;
	}

	public void setDataDisjuntada(LocalDate dataDisjuntada) {
		this.dataDisjuntada = dataDisjuntada;
	}

	public String getNumeroSequenciaDisjuntada() {
		return numeroSequenciaDisjuntada;
	}

	public void setNumeroSequenciaDisjuntada(String numeroSequenciaDisjuntada) {
		this.numeroSequenciaDisjuntada = numeroSequenciaDisjuntada;
	}

	public Integer getNumeroAviso() {
		return numeroAviso;
	}

	public void setNumeroAviso(Integer numeroAviso) {
		this.numeroAviso = numeroAviso;
	}

	public String getNumeroProcessoPrincipal() {
		return numeroProcessoPrincipal;
	}

	public void setNumeroProcessoPrincipal(String numeroProcessoPrincipal) {
		this.numeroProcessoPrincipal = numeroProcessoPrincipal;
	}

	public String getNomeOrgaoDisjuntada() {
		return nomeOrgaoDisjuntada;
	}

	public void setNomeOrgaoDisjuntada(String nomeOrgaoDisjuntada) {
		this.nomeOrgaoDisjuntada = nomeOrgaoDisjuntada;
	}

	public String getCodigoTipoMovimentoProcesso() {
		return codigoTipoMovimentoProcesso;
	}

	public void setCodigoTipoMovimentoProcesso(String codigoTipoMovimentoProcesso) {
		this.codigoTipoMovimentoProcesso = codigoTipoMovimentoProcesso;
	}

	public LocalDate getDataConsulta() {
		return dataConsulta;
	}

	public void setDataConsulta(LocalDate dataConsulta) {
		this.dataConsulta = dataConsulta;
	}

	public List<MovimentoDTO> getMovimentos() {
		return movimentos;
	}

	public void setMovimentos(List<MovimentoDTO> movimentos) {
		this.movimentos = movimentos;
	}

	public boolean isDadosCompletos() {
		return dadosCompletos;
	}

	public void setDadosCompletos(boolean dadosCompletos) {
		this.dadosCompletos = dadosCompletos;
	}

}
