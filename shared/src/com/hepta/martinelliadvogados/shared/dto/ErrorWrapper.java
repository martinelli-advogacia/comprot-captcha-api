package com.hepta.martinelliadvogados.shared.dto;

public class ErrorWrapper {
	String property;
	String message;

	public ErrorWrapper() {
		super();
	}

	public boolean isCpfCnpjErro() {
		return property.contains("cpfCnpj") && message.contains("CPF/CNPJ digitado inválido.");
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
