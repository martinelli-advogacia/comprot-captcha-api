package com.hepta.martinelliadvogados.shared.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ListaProcessoWrapper {
	List<ListaProcessoDTO> processos;
	Integer totalDeProcessosEncontrados;

	public ListaProcessoWrapper() {
		super();
	}

	@JsonIgnore
	public boolean isCorreto() {
		return totalDeProcessosEncontrados == processos.size();
	}

	public List<ListaProcessoDTO> getProcessos() {
		return processos;
	}

	public void setProcessos(List<ListaProcessoDTO> processos) {
		this.processos = processos;
	}

	public Integer getTotalDeProcessosEncontrados() {
		return totalDeProcessosEncontrados;
	}

	public void setTotalDeProcessosEncontrados(Integer totalDeProcessosEncontrados) {
		this.totalDeProcessosEncontrados = totalDeProcessosEncontrados;
	}

}
