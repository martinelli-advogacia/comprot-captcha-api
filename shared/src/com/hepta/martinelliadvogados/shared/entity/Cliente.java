package com.hepta.martinelliadvogados.shared.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Cliente", schema = "dbo")
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "DS_CNPJ_PREFIXO", unique = true)
	private String cnpjPrefixo;

	@OneToMany(mappedBy = "cliente", cascade = CascadeType.PERSIST)
	private List<Filial> filiais;

	public Cliente() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpjPrefixo() {
		return cnpjPrefixo;
	}

	public void setCnpjPrefixo(String cnpjPrefixo) {
		this.cnpjPrefixo = cnpjPrefixo;
	}

	public List<Filial> getFiliais() {
		return filiais;
	}

	public void setFiliais(List<Filial> filiais) {
		this.filiais = filiais;
	}

}
