package com.hepta.martinelliadvogados.shared.entity;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hepta.martinelliadvogados.shared.dto.ListaProcessoDTO;
import com.hepta.martinelliadvogados.shared.dto.MovimentoDTO;
import com.hepta.martinelliadvogados.shared.dto.ProcessoDTO;
import com.hepta.martinelliadvogados.shared.entity.logs.LogProcesso;

@Entity
@Table(name = "Processo", schema = "dbo")
public class Processo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	// Campos do Processo
	@Column(name = "DS_NUMERO_PROCESSO_EDITADO")
	private String numeroProcessoEditado = "";//TODO: indexar
	
	@Column(name = "DS_NUMERO_DOCUMENTO_ORIGEM")
	private String numeroDocumentoOrigem = "";
	@Column(name = "DS_NOME_PROCEDENCIA")
	private String nomeProcedencia = "";
	@Column(name = "DS_NOME_ASSUNTO")
	private String nomeAssunto = "";
	@Column(name = "DS_NOME_INTERESSADO")
	private String nomeInteressado = "";
	@Column(name = "DS_NUMERO_CPF_CNPJ")
	private String numeroCpfCnpj = "";
	@Column(name = "DS_NOME_ORGAO_ORIGEM")
	private String nomeOrgaoOrigem = "";
	@Column(name = "DS_NOME_ORGAO_DESTINO")
	private String nomeOrgaoDestino = "";
	@Column(name = "DS_NOME_OUTRO_ORGAO")
	private String nomeOutroOrgao = "";
	@Column(name = "DS_NUMERO_SEQUENCIA")
	private String numeroSequencia = "";
	@Column(name = "DS_NUMERO_RELACAO")
	private String numeroRelacao = "";
	
	@Column(name = "DS_SITUACAO")
	private String situacao = "";//TODO: indexar
	
	@Column(name = "DS_SIGLA_UF_MOVIMENTO")
	private String siglaUfMovimento = "";
	@Column(name = "DS_INDICADOR_VIRTUAL")
	private String indicadorVirtual = "";
	@Column(name = "DS_INDICADOR_PROFISC")
	private String indicadorProfisc = "";
	@Column(name = "DS_NUMERO_SEQUENCIA_DISJUNTADA")
	private String numeroSequenciaDisjuntada = "";
	
	@Column(unique = true, name = "DS_NUMERO_PROCESSO_PRINCIPAL")
	private String numeroProcessoPrincipal;//TODO: indexar
	
	@Column(name = "DS_NOME_ORGAO_DISJUNTADA")
	private String nomeOrgaoDisjuntada = "";
	@Column(name = "DS_CODIGO_TIPO_MOVIMENTO_PROCESSO")
	private String codigoTipoMovimentoProcesso = "";
	@Column(name = "TP_INDICADOR_CPF_CNPJ")
	private Integer indicadorCpfCnpj = -1;
	@Column(name = "TP_INDICADOR_E_PROCESSO")
	private Integer indicadorEProcesso = -1;
	@Column(name = "TP_INDICADOR_SIEF")
	private Integer indicadorSief = -1;
	@Column(name = "NU_NUMERO_AVISO")
	private Integer numeroAviso = -1;
	@Column(name = "DT_DATA_DISJUNTADA")
	private LocalDate dataDisjuntada = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();
	@Column(name = "DT_DATA_PROTOCOLO")
	private LocalDate dataProtocolo = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();
	@Column(name = "DT_DATA_MOVIMENTO")
	private LocalDate dataMovimento = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();
	@Column(name = "DT_DATA_CONSULTA")
	private LocalDate dataConsulta = Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate();//TODO: indexar

	@Column(name = "BL_DADOS_COMPLETOS")
	private Boolean dadosCompletos = false;

	// Campos do ListaProcesso
	@Column(name = "DS_INDICADOR_PROCESSO_CANCELADO")
	private String indicadorProcessoCancelado = "";
	@Column(name = "DS_INDICADORPROCESSOELIMINADO")
	private String indicadorProcessoEliminado = "";
	@Column(name = "DS_INDICADOR_PROCESS_EXCLUIDO")
	private String indicadorProcessoExcluido = "";
	@Column(name = "DS_NUMERO_DOCUMENTO_EXCLUSAO")
	private String numeroDocumentoExclusao = "";
	@Column(name = "DS_CODIGO_TIPO_MOVIMENTO_MOVIMENTO")
	private String codigoTipoMovimentoMovimento = "";
	@Column(name = "DS_SG_OTRO_CLIENTE_PROC")
	private String sgOtroClienteProc = "";
	@Column(name = "DS_INDICADOR_PROCESSO_PRINCIPAL")
	private String indicadorProcessoPrincipal = "";
	@Column(name = "DS_LEI_FORMACAO")
	private String leiFormacao = "";
	@Column(name = "DS_NUMERO_ANTIGO_PROCESSO")
	private String numeroAntigoProcesso = "";
	@Column(name = "DS_NUMERO_CPF_CGC")
	private String numeroCpfCgc = "";
	@Column(name = "DS_SG_OUTRO_CLIENTE_ASSUNTO")
	private String sgOutroClienteAssunto = "";
	@Column(name = "DS_NUMERO_PROCESSO_PRINCIPAL_RAIZ")
	private String numeroProcessoPrincipalRaiz = "";
	@Column(name = "DS_NUMERO_PROCESSO_PRINCIPAL_AJ")
	private String numeroProcessoPrincipalAj = "";
	@Column(name = "DS_NOME_INTERESSADO_RAIZ")
	private String nomeInteressadoRaiz = "";
	@Column(name = "DS_NOME_ASSUNTO_RAIZ")
	private String nomeAssuntoRaiz = "";
	@Column(name = "DS_NOME_DESPACHO")
	private String nomeDespacho = "";
	@Column(name = "DS_NOME_TEMPORALIDADE")
	private String nomeTemporalidade = "";
	@Column(name = "DS_NOME_ORGAO_PROTOCOLO")
	private String nomeOrgaoProtocolo = "";
	@Column(name = "DS_MOEDA")
	private String moeda = "";
	@Column(name = "DS_NOME_MOEDA")
	private String nomeMoeda = "";
	@Column(name = "DS_VALOR_RECURSO_FINANCEIRO")
	private String valorRecursoFinanceiro = "";
	@Column(name = "DS_TEXTO_OBSERVACAO")
	private String textoObservacao = "";
	@Column(name = "DS_CODIGO_CLIENTE_PESQUISA")
	private String codigoClientePesquisa = "";
	@Column(name = "NU_INDICADOR_CPF_CGC")
	private Integer indicadorCpfCgc = -1;
	@Column(name = "NU_INDICADOR_PROCESSO_TRANSITO")
	private Integer indicadorProcessoTransito = -1;
	@Column(name = "NU_NUMERO_MALOTE")
	private Integer numeroMalote = -1;
	@Column(name = "NU_NUMERO_REGIAO_POSTAL")
	private Integer numeroRegiaoPostal = -1;
	@Column(name = "NU_NUMERO_CAIXA_ARQUIVO")
	private Integer numeroCaixaArquivo = -1;
	@Column(name = "DS_DATA_EXCLUSAO_PROCESSO")
	private String dataExclusaoProcesso = "";
	@Column(name = "DS_DATA_JUNTADA")
	private String dataJuntada = "";
	@Column(name = "DS_DATA_PROCESSAMENTO_BCI")
	private String dataProcessamentoBCI = "";

	@ManyToOne
	@JoinColumn(name = "FK_PROCESSO_FILIAL", foreignKey = @ForeignKey(name = "FK_CONSTRAINT_PROCESSO_FILIAL"))
	private Filial filial;

	@OneToMany(mappedBy = "processo")
	private List<Movimento> movimentos;

	@OneToMany(mappedBy = "processo", cascade = CascadeType.PERSIST)
	private List<LogProcesso> log;

	public Processo() {
		super();
	}

	public String emptyIfNull(String str) {
		return str == null ? "" : str;
	}

	private LocalDate minDateIfNull(LocalDate dt) {
		return dt == null ? Instant.ofEpochMilli(0L).atOffset(ZoneOffset.UTC).toLocalDate() : dt;
	}

	private Integer negativeIfNull(Integer i) {
		return i == null ? -1 : i;
	}

	public void updateComProcesso(ProcessoDTO obj) {
		this.dadosCompletos = true;
		this.numeroProcessoPrincipal = emptyIfNull(obj.getNumeroProcesso());
		this.numeroProcessoEditado = emptyIfNull(obj.getNumeroProcessoEditado());
		this.numeroDocumentoOrigem = emptyIfNull(obj.getNumeroDocumentoOrigem());
		this.nomeProcedencia = emptyIfNull(obj.getNomeProcedencia());
		this.nomeAssunto = emptyIfNull(obj.getNomeAssunto());
		this.nomeInteressado = emptyIfNull(obj.getNomeInteressado());
		this.numeroCpfCnpj = emptyIfNull(obj.getNumeroCpfCnpj());
		this.nomeOrgaoOrigem = emptyIfNull(obj.getNomeOrgaoOrigem());
		this.nomeOrgaoDestino = emptyIfNull(obj.getNomeOrgaoDestino());
		this.nomeOutroOrgao = emptyIfNull(obj.getNomeOutroOrgao());
		this.numeroSequencia = emptyIfNull(obj.getNumeroSequencia());
		this.numeroRelacao = emptyIfNull(obj.getNumeroRelacao());
		this.situacao = emptyIfNull(obj.getSituacao());
		this.siglaUfMovimento = emptyIfNull(obj.getSiglaUfMovimento());
		this.indicadorVirtual = emptyIfNull(obj.getIndicadorVirtual());
		this.indicadorProfisc = emptyIfNull(obj.getIndicadorProfisc());
		this.numeroSequenciaDisjuntada = emptyIfNull(obj.getNumeroSequenciaDisjuntada());
		this.nomeOrgaoDisjuntada = emptyIfNull(obj.getNomeOrgaoDisjuntada());
		this.codigoTipoMovimentoProcesso = emptyIfNull(obj.getCodigoTipoMovimentoProcesso());
		this.dataProtocolo = minDateIfNull(obj.getDataProtocolo());
		this.dataMovimento = minDateIfNull(obj.getDataMovimento());
		this.dataDisjuntada = minDateIfNull(obj.getDataDisjuntada());
		this.dataProtocolo = minDateIfNull(obj.getDataProtocolo());

		this.indicadorCpfCnpj = negativeIfNull(obj.getIndicadorCpfCnpj());
		this.indicadorEProcesso = negativeIfNull(obj.getIndicadorEProcesso());
		this.indicadorSief = negativeIfNull(obj.getIndicadorSief());
		this.numeroAviso = negativeIfNull(obj.getNumeroAviso());

		this.dataConsulta = LocalDate.now();
	}

	public List<Movimento> updateMovimentos(List<MovimentoDTO> list) {
		List<Movimento> novos = new ArrayList<>();
		if (list.isEmpty()) {
			this.movimentos = null;
			return new ArrayList<>();
		}

		for (MovimentoDTO m : list) {
			novos.add(m.toEntity(this));
		}
		this.movimentos = novos;
		return novos;
	}

	public void insertComListaProcesso(ListaProcessoDTO obj, Filial filial) {
		this.dadosCompletos = false;
		this.indicadorProcessoCancelado = emptyIfNull(obj.getIndicadorProcessoCancelado());
		this.indicadorProcessoEliminado = emptyIfNull(obj.getIndicadorProcessoEliminado());
		this.indicadorProcessoExcluido = emptyIfNull(obj.getIndicadorProcessoExcluido());
		this.dataExclusaoProcesso = emptyIfNull(obj.getDataExclusaoProcesso());
		this.numeroDocumentoExclusao = emptyIfNull(obj.getNumeroDocumentoExclusao());
		this.codigoTipoMovimentoProcesso = emptyIfNull(obj.getCodigoTipoMovimentoProcesso());
		this.codigoTipoMovimentoMovimento = emptyIfNull(obj.getCodigoTipoMovimentoMovimento());
		this.numeroProcessoEditado = emptyIfNull(obj.getNumeroProcessoEditado());
		this.sgOtroClienteProc = emptyIfNull(obj.getSgOtroClienteProc());
		this.indicadorProcessoPrincipal = emptyIfNull(obj.getIndicadorProcessoPrincipal());
		this.leiFormacao = emptyIfNull(obj.getLeiFormacao());
		this.numeroDocumentoOrigem = emptyIfNull(obj.getNumeroDocumentoOrigem());
		this.numeroAntigoProcesso = emptyIfNull(obj.getNumeroAntigoProcesso());
		this.nomeInteressado = emptyIfNull(obj.getNomeInteressado());
		this.numeroCpfCgc = emptyIfNull(obj.getNumeroCpfCgc());
		this.nomeAssunto = emptyIfNull(obj.getNomeAssunto());
		this.sgOutroClienteAssunto = emptyIfNull(obj.getSgOutroClienteAssunto());
		this.dataJuntada = emptyIfNull(obj.getDataJuntada());
		this.numeroProcessoPrincipalRaiz = emptyIfNull(obj.getNumeroProcessoPrincipalRaiz());
		this.numeroProcessoPrincipalAj = emptyIfNull(obj.getNumeroProcessoPrincipalAj());
		this.nomeInteressadoRaiz = emptyIfNull(obj.getNomeInteressadoRaiz());
		this.numeroSequencia = emptyIfNull(obj.getNumeroSequencia());
		this.numeroRelacao = emptyIfNull(obj.getNumeroRelacao());
		this.nomeOrgaoOrigem = emptyIfNull(obj.getNomeOrgaoOrigem());
		this.nomeOrgaoDestino = emptyIfNull(obj.getNomeOrgaoDestino());
		this.nomeOutroOrgao = emptyIfNull(obj.getNomeOutroOrgao());
		this.siglaUfMovimento = emptyIfNull(obj.getSiglaUfMovimento());
		this.nomeDespacho = emptyIfNull(obj.getNomeDespacho());
		this.nomeAssuntoRaiz = emptyIfNull(obj.getNomeAssuntoRaiz());
		this.numeroSequenciaDisjuntada = emptyIfNull(obj.getNumeroSequenciaDisjuntada());
		this.numeroProcessoPrincipal = emptyIfNull(obj.getNumeroProcessoPrincipal());
		this.nomeOrgaoDisjuntada = emptyIfNull(obj.getNomeOrgaoDisjuntada());
		this.nomeTemporalidade = emptyIfNull(obj.getNomeTemporalidade());
		this.dataProcessamentoBCI = emptyIfNull(obj.getDataProcessamentoBCI());
		this.nomeOrgaoProtocolo = emptyIfNull(obj.getNomeOrgaoProtocolo());
		this.moeda = emptyIfNull(obj.getMoeda());
		this.nomeMoeda = emptyIfNull(obj.getNomeMoeda());
		this.valorRecursoFinanceiro = emptyIfNull(obj.getValorRecursoFinanceiro());
		this.textoObservacao = emptyIfNull(obj.getTextoObservacao());
		this.nomeProcedencia = emptyIfNull(obj.getNomeProcedencia());
		this.codigoClientePesquisa = emptyIfNull(obj.getCodigoClientePesquisa());
		this.situacao = emptyIfNull(obj.getSituacao());
		this.indicadorProfisc = emptyIfNull(obj.getIndicadorProfisc());
		this.indicadorVirtual = emptyIfNull(obj.getIndicadorVirtual());

		this.numeroCaixaArquivo = negativeIfNull(obj.getNumeroCaixaArquivo());
		this.indicadorSief = negativeIfNull(obj.getIndicadorSief());
		this.indicadorEProcesso = negativeIfNull(obj.getIndicadorEProcesso());
		this.indicadorCpfCgc = negativeIfNull(obj.getIndicadorCpfCgc());
		this.indicadorProcessoTransito = negativeIfNull(obj.getIndicadorProcessoTransito());
		this.numeroAviso = negativeIfNull(obj.getNumeroAviso());
		this.numeroMalote = negativeIfNull(obj.getNumeroMalote());
		this.numeroRegiaoPostal = negativeIfNull(obj.getNumeroRegiaoPostal());

		this.dataProtocolo = minDateIfNull(obj.getDataProtocolo());
		this.dataMovimento = minDateIfNull(obj.getDataMovimento());
		this.dataDisjuntada = minDateIfNull(obj.getDataDisjuntada());

		this.dataConsulta = LocalDate.now();
		this.filial = filial;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroProcessoEditado() {
		return numeroProcessoEditado;
	}

	public void setNumeroProcessoEditado(String numeroProcessoEditado) {
		this.numeroProcessoEditado = numeroProcessoEditado;
	}

	public String getNumeroDocumentoOrigem() {
		return numeroDocumentoOrigem;
	}

	public void setNumeroDocumentoOrigem(String numeroDocumentoOrigem) {
		this.numeroDocumentoOrigem = numeroDocumentoOrigem;
	}

	public String getNomeProcedencia() {
		return nomeProcedencia;
	}

	public void setNomeProcedencia(String nomeProcedencia) {
		this.nomeProcedencia = nomeProcedencia;
	}

	public String getNomeAssunto() {
		return nomeAssunto;
	}

	public void setNomeAssunto(String nomeAssunto) {
		this.nomeAssunto = nomeAssunto;
	}

	public String getNomeInteressado() {
		return nomeInteressado;
	}

	public void setNomeInteressado(String nomeInteressado) {
		this.nomeInteressado = nomeInteressado;
	}

	public String getNumeroCpfCnpj() {
		return numeroCpfCnpj;
	}

	public void setNumeroCpfCnpj(String numeroCpfCnpj) {
		this.numeroCpfCnpj = numeroCpfCnpj;
	}

	public String getNomeOrgaoOrigem() {
		return nomeOrgaoOrigem;
	}

	public void setNomeOrgaoOrigem(String nomeOrgaoOrigem) {
		this.nomeOrgaoOrigem = nomeOrgaoOrigem;
	}

	public String getNomeOrgaoDestino() {
		return nomeOrgaoDestino;
	}

	public void setNomeOrgaoDestino(String nomeOrgaoDestino) {
		this.nomeOrgaoDestino = nomeOrgaoDestino;
	}

	public String getNomeOutroOrgao() {
		return nomeOutroOrgao;
	}

	public void setNomeOutroOrgao(String nomeOutroOrgao) {
		this.nomeOutroOrgao = nomeOutroOrgao;
	}

	public String getNumeroSequencia() {
		return numeroSequencia;
	}

	public void setNumeroSequencia(String numeroSequencia) {
		this.numeroSequencia = numeroSequencia;
	}

	public String getNumeroRelacao() {
		return numeroRelacao;
	}

	public void setNumeroRelacao(String numeroRelacao) {
		this.numeroRelacao = numeroRelacao;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getSiglaUfMovimento() {
		return siglaUfMovimento;
	}

	public void setSiglaUfMovimento(String siglaUfMovimento) {
		this.siglaUfMovimento = siglaUfMovimento;
	}

	public String getIndicadorVirtual() {
		return indicadorVirtual;
	}

	public void setIndicadorVirtual(String indicadorVirtual) {
		this.indicadorVirtual = indicadorVirtual;
	}

	public String getIndicadorProfisc() {
		return indicadorProfisc;
	}

	public void setIndicadorProfisc(String indicadorProfisc) {
		this.indicadorProfisc = indicadorProfisc;
	}

	public String getNumeroSequenciaDisjuntada() {
		return numeroSequenciaDisjuntada;
	}

	public void setNumeroSequenciaDisjuntada(String numeroSequenciaDisjuntada) {
		this.numeroSequenciaDisjuntada = numeroSequenciaDisjuntada;
	}

	public String getNumeroProcessoPrincipal() {
		return numeroProcessoPrincipal;
	}

	public void setNumeroProcessoPrincipal(String numeroProcessoPrincipal) {
		this.numeroProcessoPrincipal = numeroProcessoPrincipal;
	}

	public String getNomeOrgaoDisjuntada() {
		return nomeOrgaoDisjuntada;
	}

	public void setNomeOrgaoDisjuntada(String nomeOrgaoDisjuntada) {
		this.nomeOrgaoDisjuntada = nomeOrgaoDisjuntada;
	}

	public String getCodigoTipoMovimentoProcesso() {
		return codigoTipoMovimentoProcesso;
	}

	public void setCodigoTipoMovimentoProcesso(String codigoTipoMovimentoProcesso) {
		this.codigoTipoMovimentoProcesso = codigoTipoMovimentoProcesso;
	}

	public Integer getIndicadorCpfCnpj() {
		return indicadorCpfCnpj;
	}

	public void setIndicadorCpfCnpj(Integer indicadorCpfCnpj) {
		this.indicadorCpfCnpj = indicadorCpfCnpj;
	}

	public Integer getIndicadorEProcesso() {
		return indicadorEProcesso;
	}

	public void setIndicadorEProcesso(Integer indicadorEProcesso) {
		this.indicadorEProcesso = indicadorEProcesso;
	}

	public Integer getIndicadorSief() {
		return indicadorSief;
	}

	public void setIndicadorSief(Integer indicadorSief) {
		this.indicadorSief = indicadorSief;
	}

	public Integer getNumeroAviso() {
		return numeroAviso;
	}

	public void setNumeroAviso(Integer numeroAviso) {
		this.numeroAviso = numeroAviso;
	}

	public LocalDate getDataDisjuntada() {
		return dataDisjuntada;
	}

	public void setDataDisjuntada(LocalDate dataDisjuntada) {
		this.dataDisjuntada = dataDisjuntada;
	}

	public LocalDate getDataProtocolo() {
		return dataProtocolo;
	}

	public void setDataProtocolo(LocalDate dataProtocolo) {
		this.dataProtocolo = dataProtocolo;
	}

	public LocalDate getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(LocalDate dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

	public LocalDate getDataConsulta() {
		return dataConsulta;
	}

	public void setDataConsulta(LocalDate dataConsulta) {
		this.dataConsulta = dataConsulta;
	}

	public Boolean getDadosCompletos() {
		return dadosCompletos;
	}

	public void setDadosCompletos(Boolean dadosCompletos) {
		this.dadosCompletos = dadosCompletos;
	}

	public String getIndicadorProcessoCancelado() {
		return indicadorProcessoCancelado;
	}

	public void setIndicadorProcessoCancelado(String indicadorProcessoCancelado) {
		this.indicadorProcessoCancelado = indicadorProcessoCancelado;
	}

	public String getIndicadorProcessoEliminado() {
		return indicadorProcessoEliminado;
	}

	public void setIndicadorProcessoEliminado(String indicadorProcessoEliminado) {
		this.indicadorProcessoEliminado = indicadorProcessoEliminado;
	}

	public String getIndicadorProcessoExcluido() {
		return indicadorProcessoExcluido;
	}

	public void setIndicadorProcessoExcluido(String indicadorProcessoExcluido) {
		this.indicadorProcessoExcluido = indicadorProcessoExcluido;
	}

	public String getNumeroDocumentoExclusao() {
		return numeroDocumentoExclusao;
	}

	public void setNumeroDocumentoExclusao(String numeroDocumentoExclusao) {
		this.numeroDocumentoExclusao = numeroDocumentoExclusao;
	}

	public String getCodigoTipoMovimentoMovimento() {
		return codigoTipoMovimentoMovimento;
	}

	public void setCodigoTipoMovimentoMovimento(String codigoTipoMovimentoMovimento) {
		this.codigoTipoMovimentoMovimento = codigoTipoMovimentoMovimento;
	}

	public String getSgOtroClienteProc() {
		return sgOtroClienteProc;
	}

	public void setSgOtroClienteProc(String sgOtroClienteProc) {
		this.sgOtroClienteProc = sgOtroClienteProc;
	}

	public String getIndicadorProcessoPrincipal() {
		return indicadorProcessoPrincipal;
	}

	public void setIndicadorProcessoPrincipal(String indicadorProcessoPrincipal) {
		this.indicadorProcessoPrincipal = indicadorProcessoPrincipal;
	}

	public String getLeiFormacao() {
		return leiFormacao;
	}

	public void setLeiFormacao(String leiFormacao) {
		this.leiFormacao = leiFormacao;
	}

	public String getNumeroAntigoProcesso() {
		return numeroAntigoProcesso;
	}

	public void setNumeroAntigoProcesso(String numeroAntigoProcesso) {
		this.numeroAntigoProcesso = numeroAntigoProcesso;
	}

	public String getNumeroCpfCgc() {
		return numeroCpfCgc;
	}

	public void setNumeroCpfCgc(String numeroCpfCgc) {
		this.numeroCpfCgc = numeroCpfCgc;
	}

	public String getSgOutroClienteAssunto() {
		return sgOutroClienteAssunto;
	}

	public void setSgOutroClienteAssunto(String sgOutroClienteAssunto) {
		this.sgOutroClienteAssunto = sgOutroClienteAssunto;
	}

	public String getNumeroProcessoPrincipalRaiz() {
		return numeroProcessoPrincipalRaiz;
	}

	public void setNumeroProcessoPrincipalRaiz(String numeroProcessoPrincipalRaiz) {
		this.numeroProcessoPrincipalRaiz = numeroProcessoPrincipalRaiz;
	}

	public String getNumeroProcessoPrincipalAj() {
		return numeroProcessoPrincipalAj;
	}

	public void setNumeroProcessoPrincipalAj(String numeroProcessoPrincipalAj) {
		this.numeroProcessoPrincipalAj = numeroProcessoPrincipalAj;
	}

	public String getNomeInteressadoRaiz() {
		return nomeInteressadoRaiz;
	}

	public void setNomeInteressadoRaiz(String nomeInteressadoRaiz) {
		this.nomeInteressadoRaiz = nomeInteressadoRaiz;
	}

	public String getNomeAssuntoRaiz() {
		return nomeAssuntoRaiz;
	}

	public void setNomeAssuntoRaiz(String nomeAssuntoRaiz) {
		this.nomeAssuntoRaiz = nomeAssuntoRaiz;
	}

	public String getNomeDespacho() {
		return nomeDespacho;
	}

	public void setNomeDespacho(String nomeDespacho) {
		this.nomeDespacho = nomeDespacho;
	}

	public String getNomeTemporalidade() {
		return nomeTemporalidade;
	}

	public void setNomeTemporalidade(String nomeTemporalidade) {
		this.nomeTemporalidade = nomeTemporalidade;
	}

	public String getNomeOrgaoProtocolo() {
		return nomeOrgaoProtocolo;
	}

	public void setNomeOrgaoProtocolo(String nomeOrgaoProtocolo) {
		this.nomeOrgaoProtocolo = nomeOrgaoProtocolo;
	}

	public String getMoeda() {
		return moeda;
	}

	public void setMoeda(String moeda) {
		this.moeda = moeda;
	}

	public String getNomeMoeda() {
		return nomeMoeda;
	}

	public void setNomeMoeda(String nomeMoeda) {
		this.nomeMoeda = nomeMoeda;
	}

	public String getValorRecursoFinanceiro() {
		return valorRecursoFinanceiro;
	}

	public void setValorRecursoFinanceiro(String valorRecursoFinanceiro) {
		this.valorRecursoFinanceiro = valorRecursoFinanceiro;
	}

	public String getTextoObservacao() {
		return textoObservacao;
	}

	public void setTextoObservacao(String textoObservacao) {
		this.textoObservacao = textoObservacao;
	}

	public String getCodigoClientePesquisa() {
		return codigoClientePesquisa;
	}

	public void setCodigoClientePesquisa(String codigoClientePesquisa) {
		this.codigoClientePesquisa = codigoClientePesquisa;
	}

	public Integer getIndicadorCpfCgc() {
		return indicadorCpfCgc;
	}

	public void setIndicadorCpfCgc(Integer indicadorCpfCgc) {
		this.indicadorCpfCgc = indicadorCpfCgc;
	}

	public Integer getIndicadorProcessoTransito() {
		return indicadorProcessoTransito;
	}

	public void setIndicadorProcessoTransito(Integer indicadorProcessoTransito) {
		this.indicadorProcessoTransito = indicadorProcessoTransito;
	}

	public Integer getNumeroMalote() {
		return numeroMalote;
	}

	public void setNumeroMalote(Integer numeroMalote) {
		this.numeroMalote = numeroMalote;
	}

	public Integer getNumeroRegiaoPostal() {
		return numeroRegiaoPostal;
	}

	public void setNumeroRegiaoPostal(Integer numeroRegiaoPostal) {
		this.numeroRegiaoPostal = numeroRegiaoPostal;
	}

	public Integer getNumeroCaixaArquivo() {
		return numeroCaixaArquivo;
	}

	public void setNumeroCaixaArquivo(Integer numeroCaixaArquivo) {
		this.numeroCaixaArquivo = numeroCaixaArquivo;
	}

	public String getDataExclusaoProcesso() {
		return dataExclusaoProcesso;
	}

	public void setDataExclusaoProcesso(String dataExclusaoProcesso) {
		this.dataExclusaoProcesso = dataExclusaoProcesso;
	}

	public String getDataJuntada() {
		return dataJuntada;
	}

	public void setDataJuntada(String dataJuntada) {
		this.dataJuntada = dataJuntada;
	}

	public String getDataProcessamentoBCI() {
		return dataProcessamentoBCI;
	}

	public void setDataProcessamentoBCI(String dataProcessamentoBCI) {
		this.dataProcessamentoBCI = dataProcessamentoBCI;
	}

	public Filial getFilial() {
		return filial;
	}

	public void setFilial(Filial filial) {
		this.filial = filial;
	}

	public List<Movimento> getMovimentos() {
		return movimentos;
	}

	public void setMovimentos(List<Movimento> movimentos) {
		this.movimentos = movimentos;
	}

}
