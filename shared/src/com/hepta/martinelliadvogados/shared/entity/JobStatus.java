package com.hepta.martinelliadvogados.shared.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hepta.martinelliadvogados.shared.entity.logs.LogJob;

@Entity
@Table(name = "JobStatus", schema = "dbo")
public class JobStatus {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "DS_JOB_NAME", unique=true)
	private String jobName;

	@Column(name = "DT_DATA_INICIO")
	private LocalDateTime dataInicio;
	@Column(name = "DT_DATA_FIM")
	private LocalDateTime dataFim;

	@OneToMany(mappedBy = "jobStatus", cascade = CascadeType.PERSIST)
	private List<LogJob> log;

	public JobStatus() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
	}

	public List<LogJob> getLog() {
		return log;
	}

	public void setLog(List<LogJob> log) {
		this.log = log;
	}

}
