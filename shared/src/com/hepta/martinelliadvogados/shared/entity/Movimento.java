package com.hepta.martinelliadvogados.shared.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Movimento", schema = "dbo")
public class Movimento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	@Column(name = "DT_DATA_MOVIMENTO_EDITADA")
	private LocalDate dataMovimentoEditada;
	@Column(name = "NU_CODIGO_ORGAO_ORIGEM")
	private Long codigoOrgaoOrigem;
	@Column(name = "NU_CODIGO_ORGAO_DESTINO")
	private Long codigoOrgaoDestino;
	@Column(name = "NU_CODIGO_DESPACHO")
	private Long codigoDespacho;
	@Column(name = "NU_CODIGO_TEMPORALIDADE")
	private Long codigoTemporalidade;
	@Column(name = "DS_CODIGO_TIPO_MOVIMENTO")
	private String codigoTipoMovimento;
	@Column(name = "DS_NOME_TIPO_MOVIMENTO")
	private String nomeTipoMovimento;
	@Column(name = "DS_NUMERO_SEQUENCIA")
	private String numeroSequencia;
	@Column(name = "DS_NUMERO_RMRAAJDJ")
	private String numeroRMRAAJDJ;
	@Column(name = "DS_NOME_ORGAO_ORIGEM")
	private String nomeOrgaoOrigem;
	@Column(name = "DS_NOME_ORGAO_DESTINO")
	private String nomeOrgaoDestino;
	@Column(name = "DS_NOME_DESPACHO")
	private String nomeDespacho;
	@Column(name = "DS_NUMERO_CAIXA")
	private String numeroCaixa;
	@Column(name = "DS_NOME_TEMPORALIDADE")
	private String nomeTemporalidade;
	@Column(name = "DS_NUMERO_PROCESSO_PRINCIPAL")
	private String numeroProcessoPrincipal;

	@ManyToOne
	@JoinColumn(name = "FK_MOVIMENTO_PROCESSO", foreignKey = @ForeignKey(name = "FK_CONSTRAINT_MOVIMENTO_PROCESSO"))
	private Processo processo;

	public Movimento() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDataMovimentoEditada() {
		return dataMovimentoEditada;
	}

	public void setDataMovimentoEditada(LocalDate dataMovimentoEditada) {
		this.dataMovimentoEditada = dataMovimentoEditada;
	}

	public Long getCodigoOrgaoOrigem() {
		return codigoOrgaoOrigem;
	}

	public void setCodigoOrgaoOrigem(Long codigoOrgaoOrigem) {
		this.codigoOrgaoOrigem = codigoOrgaoOrigem;
	}

	public Long getCodigoOrgaoDestino() {
		return codigoOrgaoDestino;
	}

	public void setCodigoOrgaoDestino(Long codigoOrgaoDestino) {
		this.codigoOrgaoDestino = codigoOrgaoDestino;
	}

	public Long getCodigoDespacho() {
		return codigoDespacho;
	}

	public void setCodigoDespacho(Long codigoDespacho) {
		this.codigoDespacho = codigoDespacho;
	}

	public Long getCodigoTemporalidade() {
		return codigoTemporalidade;
	}

	public void setCodigoTemporalidade(Long codigoTemporalidade) {
		this.codigoTemporalidade = codigoTemporalidade;
	}

	public String getCodigoTipoMovimento() {
		return codigoTipoMovimento;
	}

	public void setCodigoTipoMovimento(String codigoTipoMovimento) {
		this.codigoTipoMovimento = codigoTipoMovimento;
	}

	public String getNomeTipoMovimento() {
		return nomeTipoMovimento;
	}

	public void setNomeTipoMovimento(String nomeTipoMovimento) {
		this.nomeTipoMovimento = nomeTipoMovimento;
	}

	public String getNumeroSequencia() {
		return numeroSequencia;
	}

	public void setNumeroSequencia(String numeroSequencia) {
		this.numeroSequencia = numeroSequencia;
	}

	public String getNumeroRMRAAJDJ() {
		return numeroRMRAAJDJ;
	}

	public void setNumeroRMRAAJDJ(String numeroRMRAAJDJ) {
		this.numeroRMRAAJDJ = numeroRMRAAJDJ;
	}

	public String getNomeOrgaoOrigem() {
		return nomeOrgaoOrigem;
	}

	public void setNomeOrgaoOrigem(String nomeOrgaoOrigem) {
		this.nomeOrgaoOrigem = nomeOrgaoOrigem;
	}

	public String getNomeOrgaoDestino() {
		return nomeOrgaoDestino;
	}

	public void setNomeOrgaoDestino(String nomeOrgaoDestino) {
		this.nomeOrgaoDestino = nomeOrgaoDestino;
	}

	public String getNomeDespacho() {
		return nomeDespacho;
	}

	public void setNomeDespacho(String nomeDespacho) {
		this.nomeDespacho = nomeDespacho;
	}

	public String getNumeroCaixa() {
		return numeroCaixa;
	}

	public void setNumeroCaixa(String numeroCaixa) {
		this.numeroCaixa = numeroCaixa;
	}

	public String getNomeTemporalidade() {
		return nomeTemporalidade;
	}

	public void setNomeTemporalidade(String nomeTemporalidade) {
		this.nomeTemporalidade = nomeTemporalidade;
	}

	public String getNumeroProcessoPrincipal() {
		return numeroProcessoPrincipal;
	}

	public void setNumeroProcessoPrincipal(String numeroProcessoPrincipal) {
		this.numeroProcessoPrincipal = numeroProcessoPrincipal;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

}
