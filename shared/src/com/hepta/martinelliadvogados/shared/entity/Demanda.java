package com.hepta.martinelliadvogados.shared.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.hepta.martinelliadvogados.shared.enums.EstadoDemanda;

@Entity
@Table(name = "Demanda", schema = "dbo")
public class Demanda {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	@Column(name = "DS_NOME_DEMANDANTE")
	String nomeDemandante;
	@Column(name = "DS_EMAIL_DEMANDANTE")
	String emailDemandante;
	@Column(name = "DT_DATA_INI")
	LocalDateTime dataIni;
	@Column(name = "DT_DATA_FIM")
	LocalDateTime dataFim;
	@Column(name = "NU_QUANTIDADE")
	Integer quantidade;
	@Column(name = "NU_DIAS")
	Integer dias;

	@Column(name = "TP_ESTADO")
	EstadoDemanda estado;

	@ManyToMany
	@JoinTable(name = "demandas_filiais", joinColumns = @JoinColumn(name = "DEMANDA_ID"), inverseJoinColumns = @JoinColumn(name = "FILIAL_ID"))
	Set<Filial> filiais;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeDemandante() {
		return nomeDemandante;
	}

	public void setNomeDemandante(String nomeDemandante) {
		this.nomeDemandante = nomeDemandante;
	}

	public LocalDateTime getDataIni() {
		return dataIni;
	}

	public void setDataIni(LocalDateTime dataIni) {
		this.dataIni = dataIni;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public EstadoDemanda getEstado() {
		return estado;
	}

	public void setEstado(EstadoDemanda estado) {
		this.estado = estado;
	}

	public String getEmailDemandante() {
		return emailDemandante;
	}

	public void setEmailDemandante(String emailDemandante) {
		this.emailDemandante = emailDemandante;
	}

	public Set<Filial> getFiliais() {
		return filiais;
	}

	public void setFiliais(Set<Filial> filiais) {
		this.filiais = filiais;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

}
