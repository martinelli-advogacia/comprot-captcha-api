package com.hepta.martinelliadvogados.shared.entity.logs;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hepta.martinelliadvogados.shared.entity.Processo;

@Entity
@Table(name = "LogProcesso", schema = "dbo")
public class LogProcesso {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "DH_ERRO")
	private LocalDateTime dataErro;

	@Column(name = "DS_MSG_ERRO", length = 8000)
	private String msgErro;

	@ManyToOne
	@JoinColumn(name = "FK_LOGPROCESSO_PROCESSO", foreignKey = @ForeignKey(name = "FK_CONSTRAINT_LOGPROCESSO_PROCESSO"))
	private Processo processo;

	public LogProcesso() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getDataErro() {
		return dataErro;
	}

	public void setDataErro(LocalDateTime dataErro) {
		this.dataErro = dataErro;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	public Processo getProcesso() {
		return processo;
	}

	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

}
