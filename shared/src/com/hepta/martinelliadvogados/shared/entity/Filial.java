package com.hepta.martinelliadvogados.shared.entity;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hepta.martinelliadvogados.shared.entity.logs.LogFilial;
import com.hepta.martinelliadvogados.shared.enums.Erro;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;

@Entity
@Table(name = "Filial", schema = "dbo")
public class Filial {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "DS_CNPJ", unique = true)
	private String cnpj;

	@Column(name = "DS_CNPJ_SUFIXO")
	private String cnpjSufixo;

	@Column(name = "DS_NUM_ULTIMO_PROCESSO")
	private String numUltimoProcesso;

	@Column(name = "DT_DATA_CONSULTA")
	private LocalDate dataConsulta;

	@Column(name = "TP_TIPO")
	private TipoCliente tipo;

	@Column(name = "TP_ERRO")
	private Erro erro;

	@ManyToOne
	@JoinColumn(name = "FK_FILIAL_CLIENTE", foreignKey = @ForeignKey(name = "FK_CONSTRAINT_FILIAL_CLIENTE"))
	private Cliente cliente;

	@OneToMany(mappedBy = "filial", cascade = CascadeType.PERSIST)
	private List<Processo> processos;

	@OneToMany(mappedBy = "filial", cascade = CascadeType.PERSIST)
	private List<LogFilial> log;

	@ManyToMany
	Set<Demanda> demandas;

	public Filial() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCnpjSufixo() {
		return cnpjSufixo;
	}

	public void setCnpjSufixo(String cnpjSufixo) {
		this.cnpjSufixo = cnpjSufixo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(List<Processo> processos) {
		this.processos = processos;
	}

	public List<LogFilial> getLog() {
		return log;
	}

	public void setLog(List<LogFilial> log) {
		this.log = log;
	}

	public String getNumUltimoProcesso() {
		return numUltimoProcesso;
	}

	public void setNumUltimoProcesso(String numUltimoProcesso) {
		this.numUltimoProcesso = numUltimoProcesso;
	}

	public Set<Demanda> getDemandas() {
		return demandas;
	}

	public void setDemandas(Set<Demanda> demandas) {
		this.demandas = demandas;
	}

	public TipoCliente getTipo() {
		return tipo;
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}

	public LocalDate getDataConsulta() {
		return dataConsulta;
	}

	public void setDataConsulta(LocalDate dataConsulta) {
		this.dataConsulta = dataConsulta;
	}

	public Erro getErro() {
		return erro;
	}

	public void setErro(Erro erro) {
		this.erro = erro;
	}

}
