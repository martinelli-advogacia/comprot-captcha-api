package com.hepta.martinelliadvogados.shared.service;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hepta.martinelliadvogados.shared.dto.DadosNovosProcessosDTO;
import com.hepta.martinelliadvogados.shared.dto.ErrorWrapper;
import com.hepta.martinelliadvogados.shared.dto.ListaProcessoDTO;
import com.hepta.martinelliadvogados.shared.dto.ListaProcessoWrapper;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Erro;
import com.hepta.martinelliadvogados.shared.integration.ComprotRequest;
import com.hepta.martinelliadvogados.shared.persistence.FilialDAO;
import com.hepta.martinelliadvogados.shared.persistence.LogFilialDAO;
import com.hepta.martinelliadvogados.shared.persistence.ProcessoDAO;
import com.hepta.martinelliadvogados.shared.utils.DataHelper;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

public class ServiceUtils {

	private static Logger LOG = Logger.getLogger(ServiceUtils.class);

	protected ProcessoDAO daoProcesso = new ProcessoDAO();
	protected FilialDAO daoFilial = new FilialDAO();
	protected LogFilialDAO daoLog = new LogFilialDAO();
	protected boolean mostrarLogs = false;
	protected boolean guardarLogsErros = true;

	protected void info(String msg) {
		if (mostrarLogs)
			LOG.info(msg);
	}

	protected void warn(String msg) {
		LOG.warn(msg);
	}

	protected void error(String msg) {
		LOG.warn(msg);
	}

	public ServiceUtils() {
		super();
	}

	public ServiceUtils(Logger log, boolean guardarLogsErros) {
		super();
		LOG = log;
		this.guardarLogsErros = guardarLogsErros;
	}

	public void paginacaoNormal(int totalProcessos, DadosNovosProcessosDTO dado, List<ListaProcessoDTO> listaProcesso) {
		int qtdRequests = (int) Math.ceil(((totalProcessos - 30) / 30.0));
		for (int i = 0; i < qtdRequests; i++) {
			if (listaProcesso.isEmpty())
				continue;
			String ultimoProcesso = listaProcesso.get(listaProcesso.size() - 1).getNumeroProcessoPrincipal();
			DadosNovosProcessosDTO pagina = getDadosComprot(dado.getFilial(), DataHelper.getDataInicial(),
					DataHelper.getDataAtual(), ultimoProcesso);

			// tentamos repetidamente até sair uma response sem erro ou até estourar o
			// limite de 5 vezes
			int tentativasMaximas = 5;
			do {
				if (pagina.getErro().equals(Erro.SEM_ERRO)) {
					try {
						ListaProcessoWrapper listaProcWrapper = ComprotRequest.responseToListaProcessoWrapper(
								pagina.getResponse().body());
						pagina.setWrapper(listaProcWrapper);
						break;
					} catch (JsonProcessingException e) {
						e.printStackTrace();
						error("ERRO NO MAPEAMENTO DE JSON: " + dado.getResponse().body());
					}
				}
				tentativasMaximas--;
			} while (tentativasMaximas > 0);

			// caso dê erro depois de todas as tentativas...
			if (!pagina.getErro().equals(Erro.SEM_ERRO)) {
				dado.setErro(Erro.ERRO_NA_PAGINACAO); // falamos que deu erro na request principal
				break;// saimos do loop
			} else {
				listaProcesso.addAll(pagina.getWrapper().getProcessos());
			}
		}
	}

	protected Erro getResponseErro(HttpResponse<String> responseCompleted) {
		switch (responseCompleted.statusCode()) {
		case 200:
			if (!responseCompleted.body().isBlank() || !responseCompleted.body().isEmpty())
				return Erro.SEM_ERRO;
			break;
		case 204:
			// sem processos
			return Erro.SEM_ERRO;
		case 422:
			return Erro.CNPJ_INVALIDO;
		case 400:
			if (responseCompleted.body().contains("Limite da consulta excedido - restrinja o perído"))
				return Erro.PROCESSOS_DEMAIS;
			break;
		case 429:
			return Erro.CAPTCHA;
		default:
			break;
		}
		return Erro.DESCONHECIDO;
	}

	protected CompletableFuture<DadosNovosProcessosDTO> preparaFuturosDados(Filial filial, long dataInicial,
			long dataFinal, String ultimoProcesso, ExecutorService pool) {
		return CompletableFuture.supplyAsync(() -> {
			return getDadosComprot(filial, dataInicial, dataFinal, ultimoProcesso);
		}, pool);
	}

	protected int inserirNovosProcessosPesquisados(List<DadosNovosProcessosDTO> dados,
			Map<String, ProcessoWrapper> dadosCompletos) {
		int count = 0;
		try {
			for (DadosNovosProcessosDTO dado : dados) {
				Filial c = dado.getFilial();
				switch (dado.getErro()) {
				case CNPJ_INVALIDO:
					c.setErro(Erro.CNPJ_INVALIDO);
					break;
				case PROCESSOS_DEMAIS:
					c.setErro(Erro.PROCESSOS_DEMAIS);
					break;
				case SEM_ERRO:
					try {
						List<ListaProcessoDTO> processos = dado.getWrapper().getProcessos();
						count = daoProcesso.salvaNovosProcessosCount(processos, dado.getFilial(), dadosCompletos);

						c.setDataConsulta(LocalDate.now());
						c.setErro(Erro.SEM_ERRO);
						if (!processos.isEmpty()) {
							String numUltimo = processos.get(processos.size() - 1).getNumeroProcesso();
							Filial f = dado.getFilial();
							f.setNumUltimoProcesso(numUltimo);
							daoFilial.update(f);
						}
					} catch (Exception e) {
						e.printStackTrace();
						LOG.error("ERRO AO SALVAR PROCESSO: " + dado.getFilial().getCnpj());
						LOG.error(StacktraceToString.convert(e));
						if (guardarLogsErros)
							daoLog.log(dado.getFilial(), e);
					}
					break;
				case DESCONHECIDO:
					c.setErro(Erro.DESCONHECIDO);
					break;
				case ERRO_NA_PAGINACAO:
					c.setErro(Erro.ERRO_NA_PAGINACAO);
					break;
				default:
					c.setErro(Erro.DESCONHECIDO);
					break;
				}
				daoFilial.update(c);
			}
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			error("Erro ao inserir novos processos:" + StacktraceToString.convert(e));
			if (guardarLogsErros)
				daoLog.logDados(dados, e);
			return count;
		}

	}

	protected DadosNovosProcessosDTO getDadosComprot(Filial filial, long dataInicial, long dataFinal,
			String ultimoProcesso) {
		HttpRequest req = ComprotRequest.getProcessosPorCnpjRequest(filial.getCnpj(), dataInicial, dataFinal,
				ultimoProcesso);
		HttpClient client = HttpClient.newHttpClient();
		CompletableFuture<HttpResponse<String>> futureResponse = client.sendAsync(req,
				HttpResponse.BodyHandlers.ofString());

		DadosNovosProcessosDTO obj = new DadosNovosProcessosDTO();
		obj.setCompletableFuture(futureResponse);
		obj.setFilial(filial);
		ListaProcessoWrapper wrapper = new ListaProcessoWrapper();
		wrapper.setProcessos(new ArrayList<>());
		wrapper.setTotalDeProcessosEncontrados(0);
		obj.setWrapper(wrapper);
		obj.setRequest(req);
		try {
			HttpResponse<String> responseCompleted = futureResponse.get();
			obj.setResponse(responseCompleted);
			obj.setErro(getResponseErro(responseCompleted));
		} catch (Throwable e) {
			e.printStackTrace();
			String fullError = StacktraceToString.convert(e);
			if (!fullError.contains("204"))
				info("ERRO ao buscar dados, lista de processos setada como vazia: " + fullError);
			obj.setErro(Erro.DESCONHECIDO);
			if (guardarLogsErros)
				daoLog.log(filial, e);
		}
		return obj;
	}

	protected void prepararDados(List<DadosNovosProcessosDTO> dados) throws Exception {
		for (DadosNovosProcessosDTO dado : dados) {
			try {
				String body = "body vazio";
				if (dado.getResponse() != null && dado.getResponse().body() != null)
					body = dado.getResponse().body();
				switch (dado.getErro()) {
				case CNPJ_INVALIDO:
					ErrorWrapper erro = ComprotRequest.responseToErrorWrapper(body);
					if (erro.isCpfCnpjErro()) {
						Filial c = dado.getFilial();
						c.setErro(Erro.CNPJ_INVALIDO);
						c.setDataConsulta(LocalDate.now());
						daoFilial.update(c);
					} else
						error("ERRO NA REQUEST DE NOVOS PROCESSOS: " + body);
					break;
				case DESCONHECIDO:
					break;
				case PROCESSOS_DEMAIS:
					break;
				case SEM_ERRO:
					if (!body.isEmpty()) {
						ListaProcessoWrapper listaProcWrapper = ComprotRequest.responseToListaProcessoWrapper(body);
						dado.setWrapper(listaProcWrapper);
						dado.setErro(Erro.SEM_ERRO);
					}
					break;
				default:
					break;
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				error("ERRO NO MAPEAMENTO DE JSON: " + dado.getResponse().body());
				if (guardarLogsErros)
					daoLog.log(dado.getFilial(), e);
			}
		}
	}
}
