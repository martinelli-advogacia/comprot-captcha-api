package com.hepta.martinelliadvogados.shared.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.dto.DadosNovosProcessosDTO;
import com.hepta.martinelliadvogados.shared.dto.ListaProcessoDTO;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Erro;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

public class MuitosProcessosService extends ServiceUtils {
	private static final Logger LOG = Logger.getLogger(MuitosProcessosService.class);

	protected int numClientes;

	private static final ExecutorService pool = Executors.newFixedThreadPool(
			Application.getComprotJobsThreadpoolErros());

	public MuitosProcessosService(Logger log, boolean guardarLogsErros) {
		super(log, guardarLogsErros);
		this.mostrarLogs = false;
	}
	
	public int pegarMuitosProcessos() throws Exception {
		List<Filial> filiais = daoFilial.porErro(numClientes, Erro.PROCESSOS_DEMAIS);
		if (filiais.isEmpty()) {
			return 0;
		}
		return buscarAndSave(filiais);
	}

	protected int buscarAndSave(List<Filial> filiais) {
		List<DadosNovosProcessosDTO> processos = buscaDadosProcessosPorFilial(filiais);
		if (processos.isEmpty()) {
			info("Não há processos para atualizar.");
			return 0;
		}
		int count = 0;
		for (DadosNovosProcessosDTO d : processos) {
			int num = d.getWrapper().getProcessos().size();
			if (!d.getWrapper().isCorreto())
				error("Numero de processos desse cliente("
						+ num
						+ ") não é igual ao do comprot("
						+ d.getWrapper().getTotalDeProcessosEncontrados()
						+ ")");
			count += num;
		}
		System.out.println(count);
		//return inserirNovosProcessosPesquisados(processos);
		return 0;
	}

	/*
	 * Com os cnpjs buscados, fazemos as requisições de foma assíncrona, usando
	 * CompletableFuture A API do COMPROT página os processos de 30 em 30, o único
	 * modo de pegar os outros 30 é informando o número do último processo, dessa
	 * forma ele vai pegar +30 para frente.
	 */
	public List<DadosNovosProcessosDTO> buscaDadosProcessosPorFilial(List<Filial> filiais) {

		List<DadosNovosProcessosDTO> dados = getDados(filiais);
		try {
			prepararDados(dados);

			// Calculo para pegar e fazer novos requests caso a quantidade de processos seja
			// maior que 30.
			List<CompletableFuture<DadosNovosProcessosDTO>> futuros = new ArrayList<>();
			for (DadosNovosProcessosDTO dado : dados) {
				futuros.add(CompletableFuture.supplyAsync(() -> {
					if (dado.getErro().equals(Erro.SEM_ERRO)) {
						int totalProcessos = dado.getWrapper().getTotalDeProcessosEncontrados();
						List<ListaProcessoDTO> listaProcesso = dado.getWrapper().getProcessos();
						if (totalProcessos > 30) {
							paginacaoNormal(totalProcessos, dado, listaProcesso);
						}
						// se nada der errado, adicionamos à request principal todos os processos que
						// pegamos da paginação
						if (dado.getErro().equals(Erro.SEM_ERRO))
							dado.getWrapper().setProcessos(listaProcesso);
					}
					return dado;
				}, pool));
			}
			CompletableFuture<Void> allFutures = CompletableFuture.allOf(
					futuros.toArray(new CompletableFuture[futuros.size()]));
			CompletableFuture<List<DadosNovosProcessosDTO>> resultado = allFutures.thenApply(v -> {
				return futuros.stream().map(f -> f.join()).collect(Collectors.toList());
			});
			return resultado.get();
		} catch (Exception e) {
			error(StacktraceToString.convert(e));
			e.printStackTrace();
			if (guardarLogsErros)
				daoLog.logFiliais(filiais, e);
			return new ArrayList<>();
		}
	}

	private List<DadosNovosProcessosDTO> getDados(List<Filial> filiais) {
		List<DadosNovosProcessosDTO> dados = new ArrayList<>();
		try {
			List<CompletableFuture<DadosNovosProcessosDTO>> futuros = preparaDadosRequests(filiais, "");
			CompletableFuture<Void> allFutures = CompletableFuture.allOf(
					futuros.toArray(new CompletableFuture[futuros.size()]));
			CompletableFuture<List<DadosNovosProcessosDTO>> resultado = allFutures.thenApply(v -> {
				return futuros.stream().map(f -> f.join()).collect(Collectors.toList());
			});
			dados = resultado.get();
		} catch (Throwable e) {
			e.printStackTrace();
			warn("Erro ao pegar dados: " + StacktraceToString.convert(e));
			if (guardarLogsErros)
				daoLog.logFiliais(filiais, e);
		}
		return dados;
	}

	private List<CompletableFuture<DadosNovosProcessosDTO>> preparaDadosRequests(List<Filial> filiais,
			String numeroUltimoProcesso) {
		List<CompletableFuture<DadosNovosProcessosDTO>> futuresProcessos = new ArrayList<>();
		for (Filial filial : filiais) {
			Instant step = Instant.ofEpochMilli(0L).plus(365, ChronoUnit.DAYS);
			Instant fim = Instant.now();
			Instant ini = fim.minus(365, ChronoUnit.DAYS);
			while (true) {
				if (ini.compareTo(step) < 0) {
					CompletableFuture<DadosNovosProcessosDTO> obj = preparaFuturosDados(filial,
							Instant.ofEpochMilli(0L).toEpochMilli(), ini.toEpochMilli(), numeroUltimoProcesso, pool);
					futuresProcessos.add(obj);
					break;
				}
				CompletableFuture<DadosNovosProcessosDTO> obj = preparaFuturosDados(filial, ini.toEpochMilli(),
						fim.toEpochMilli(), numeroUltimoProcesso, pool);
				futuresProcessos.add(obj);

				fim = fim.minus(365, ChronoUnit.DAYS);
				ini = fim.minus(365, ChronoUnit.DAYS);
			}
		}
		return futuresProcessos;
	}

	public int getNumClientes() {
		return numClientes;
	}

	public void setNumClientes(int numClientes) {
		this.numClientes = numClientes;
	}

}
