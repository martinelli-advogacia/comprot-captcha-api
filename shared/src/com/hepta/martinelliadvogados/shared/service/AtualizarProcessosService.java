package com.hepta.martinelliadvogados.shared.service;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import com.hepta.martinelliadvogados.shared.Application;
import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.integration.ComprotRequest;
import com.hepta.martinelliadvogados.shared.persistence.LogProcessoDAO;
import com.hepta.martinelliadvogados.shared.persistence.ProcessoDAO;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

public class AtualizarProcessosService {
	private static final Logger LOG = Logger.getLogger(AtualizarProcessosService.class);
	protected ProcessoDAO daoProcesso = new ProcessoDAO();

	protected int numeroProcessos;
	protected boolean imprimirLogs;
	protected boolean novosProcessos;
	protected TipoCliente tipo;
	protected boolean guardarLogsErros;

	protected LogProcessoDAO daoLog = new LogProcessoDAO();

	protected static final ExecutorService pool = Executors.newFixedThreadPool(Application.getComprotJobsThreadpoolBusca());

	public AtualizarProcessosService(boolean novosProcessos, boolean imprimirLogs, int numeroProcessos,
			boolean guardarLogsErros) {
		this.numeroProcessos = numeroProcessos;
		this.imprimirLogs = imprimirLogs;
		this.novosProcessos = novosProcessos;
		this.guardarLogsErros = guardarLogsErros;
	}

	public AtualizarProcessosService() {
		super();
	}

	protected void log(String msg) {
		if (imprimirLogs)
			LOG.info(">>" + tipo.toString() + " " + msg);
	}

	public int atualizarProcessos() throws Exception {
		Map<String, Processo> processosEntity = puxarDados();
		return requestsAndUpdate(processosEntity);
	}

	protected int requestsAndUpdate(Map<String, Processo> processosEntity) throws Exception {
		log("requestsAndUpdate");
		Map<String, ProcessoWrapper> processos = new HashMap<>();
		long inicio;
		long fim;
		if (!processosEntity.isEmpty()) {
			log("gera requests");
			processos = geraRequests(processosEntity);
		} else {
			log("Não há processos para atualizar");
		}
		log(">>Atualizar processos - Inicio");
		int atualizados = 0;
		if (!processos.isEmpty()) {
			log(" >>Update - Inicio - qtde: "+processosEntity.size());
			atualizados = updateDados(processos, processosEntity);
		} else {
			log("Não foi possivel buscar processos no COMPROT ou ele não retornou nenhum processo.");
		}
		return atualizados;
	}

	public Map<String, Processo> puxarDados() throws Exception {
		if (novosProcessos)
			return daoProcesso.puxarDadosIncompletos(numeroProcessos, tipo);
		else
			return daoProcesso.puxarDadosDesatualizados(numeroProcessos, tipo);
	}

	public Map<String, ProcessoWrapper> geraRequests(Map<String, Processo> processosEntity) {
		List<ProcessoWrapper> wrappers = getDados(processosEntity.values());

		Map<String, ProcessoWrapper> map = new HashMap<>();
		for (ProcessoWrapper p : wrappers) {
			if (p != null) {
				String key = p.getProcesso().getNumeroProcessoEditado().replaceAll(" ","");
				if (!key.isEmpty() && !key.isBlank())
					map.put(key, p);
			}
		}
		return map;
	}

	public CompletableFuture<ProcessoWrapper> preparaRequests(Processo p) {
		return CompletableFuture.supplyAsync(() -> {
			try {
				HttpClient client = HttpClient.newHttpClient();
				HttpRequest req = ComprotRequest.getProcessoRequest(p.getNumeroProcessoPrincipal());
				HttpResponse<String> response = client.sendAsync(req, BodyHandlers.ofString()).get();
				return ComprotRequest.responseToProcessoWrapper(response.body());
			} catch (Throwable e) {
				e.printStackTrace();
				LOG.warn("Erro ao preparar requests" + StacktraceToString.convert(e));
				if (guardarLogsErros)
					daoLog.log(p, e);
				return null;
			}
		}, pool);
	}

	public List<ProcessoWrapper> getDados(Collection<Processo> processos) {
		List<ProcessoWrapper> dados = new ArrayList<>();
		try {
			List<CompletableFuture<ProcessoWrapper>> futuros = new ArrayList<>();
			for (Processo p : processos) {
				futuros.add(preparaRequests(p));
			}

			CompletableFuture<Void> allFutures = CompletableFuture.allOf(
					futuros.toArray(new CompletableFuture[futuros.size()]));
			CompletableFuture<List<ProcessoWrapper>> resultado = allFutures.thenApply(v -> {
				return futuros.stream().map(f -> f.join()).collect(Collectors.toList());
			});
			dados = resultado.get();
		} catch (Throwable t) {
			t.printStackTrace();
			LOG.warn("Erro ao pegar dados: " + StacktraceToString.convert(t));
			if (guardarLogsErros)
				daoLog.log(processos, t);
		}
		return dados;
	}

	public int updateDados(Map<String, ProcessoWrapper> processos, Map<String, Processo> processosEntity)
			throws Exception {
		return daoProcesso.update(processos, processosEntity);
	}


	public int getNumeroProcessos() {
		return numeroProcessos;
	}

	public void setNumeroProcessos(int numeroProcessos) {
		this.numeroProcessos = numeroProcessos;
	}

	public boolean isImprimirLogs() {
		return imprimirLogs;
	}

	public void setImprimirLogs(boolean imprimirLogs) {
		this.imprimirLogs = imprimirLogs;
	}

	public boolean isNovosProcessos() {
		return novosProcessos;
	}

	public void setNovosProcessos(boolean novosProcessos) {
		this.novosProcessos = novosProcessos;
	}

	public TipoCliente getTipo() {
		return tipo;
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}

}
