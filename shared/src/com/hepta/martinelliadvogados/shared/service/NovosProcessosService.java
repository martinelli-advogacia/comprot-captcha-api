package com.hepta.martinelliadvogados.shared.service;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.dto.DadosNovosProcessosDTO;
import com.hepta.martinelliadvogados.shared.dto.ListaProcessoDTO;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Erro;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.integration.ComprotRequest;
import com.hepta.martinelliadvogados.shared.utils.DataHelper;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

public class NovosProcessosService extends ServiceUtils {
	private static final Logger LOG = Logger.getLogger(NovosProcessosService.class);

	protected TipoCliente tipo;
	protected int numClientes;

	private static final ExecutorService pool = Executors.newFixedThreadPool(
			Application.getComprotJobsThreadpoolNovos());

	public NovosProcessosService(Logger log, boolean guardarLogsErros) {
		super(log, guardarLogsErros);
		this.mostrarLogs = true;
	}

	public int pesquisarNovosProcessos() throws Exception {
		List<Filial> filiais = puxarDados();
		if (filiais.isEmpty()) {
			return 0;
		}
		info("Achou " + filiais.size() + " filiais no banco");
		return buscarAndSave(filiais);
	}

	public List<Filial> puxarDados() throws Exception {
		LocalDate tempoMinimo = tipo == TipoCliente.CASA ? LocalDate.now()// .minusDays(7)
				: LocalDate.now();// .minusMonths(2);
		List<Filial> filiais = daoFilial.porTipo(numClientes, tipo, tempoMinimo);
		return filiais;
	}

	protected int buscarAndSave(List<Filial> filiais) {
		List<DadosNovosProcessosDTO> processos = buscaDadosProcessosPorFilial(DataHelper.getDataInicial(),
				DataHelper.getDataAtual(), filiais);
		if (processos.isEmpty()) {
			info("Não há processos para atualizar.");
			return 0;
		}
		int count = 0;
		for (DadosNovosProcessosDTO d : processos) {
			int num = d.getWrapper().getProcessos().size();
			if (!d.getWrapper().isCorreto())
				error("Numero de processos desse cliente("
						+ num
						+ ") não é igual ao do comprot("
						+ d.getWrapper().getTotalDeProcessosEncontrados()
						+ ")");
			count += num;
		}
		info("Achou " + count + " no comprot");
		info("Buscando dados completos no comprot");
		Map<String, ProcessoWrapper> dadosCompletos = geraRequestsDadosCompletos(processos);
		info("Fim busca dados completos no comprot");

		return inserirNovosProcessosPesquisados(processos,dadosCompletos);
	}

	public Map<String, ProcessoWrapper> geraRequestsDadosCompletos(List<DadosNovosProcessosDTO> processos) {
		List<ProcessoWrapper> wrappers = getDados(processos);

		Map<String, ProcessoWrapper> map = new HashMap<>();
		for (ProcessoWrapper p : wrappers) {
			if (p != null) {
				String key = p.getProcesso().getNumeroProcesso();
				if (!key.isEmpty() && !key.isBlank())
					map.put(key, p);
			}
		}
		return map;
	}

	public List<ProcessoWrapper> getDados(List<DadosNovosProcessosDTO> processos) {
		List<ProcessoWrapper> dados = new ArrayList<>();
		List<CompletableFuture<ProcessoWrapper>> futuros = new ArrayList<>();
		for (DadosNovosProcessosDTO p : processos) {
			try {

				for (ListaProcessoDTO proc : p.getWrapper().getProcessos()) {
					futuros.add(preparaRequests(proc));
				}

				CompletableFuture<Void> allFutures = CompletableFuture.allOf(
						futuros.toArray(new CompletableFuture[futuros.size()]));
				CompletableFuture<List<ProcessoWrapper>> resultado = allFutures.thenApply(v -> {
					return futuros.stream().map(f -> f.join()).collect(Collectors.toList());
				});
				dados = resultado.get();
			} catch (Throwable t) {
				t.printStackTrace();
				LOG.warn("Erro ao pegar dados: " + StacktraceToString.convert(t));
				if (guardarLogsErros)
					daoLog.log(p.getFilial(), t);
			}
		}
		return dados;

	}

	public CompletableFuture<ProcessoWrapper> preparaRequests(ListaProcessoDTO proc) {
		return CompletableFuture.supplyAsync(() -> {
			try {
				HttpClient client = HttpClient.newHttpClient();
				HttpRequest req = ComprotRequest.getProcessoRequest(proc.getNumeroProcesso());
				HttpResponse<String> response = client.sendAsync(req, BodyHandlers.ofString()).get();
				return ComprotRequest.responseToProcessoWrapper(response.body());
			} catch (Throwable e) {
				e.printStackTrace();
				LOG.warn("Erro ao preparar requests" + StacktraceToString.convert(e));
				return null;
			}
		}, pool);
	}

	public List<DadosNovosProcessosDTO> buscaDadosProcessosPorFilial(long ini, long fim, List<Filial> filiais) {

		List<DadosNovosProcessosDTO> dados = getDados(ini, fim, filiais);

		try {
			prepararDados(dados);

			// Calculo para pegar e fazer novos requests caso a quantidade de processos seja
			// maior que 30.
			List<CompletableFuture<DadosNovosProcessosDTO>> futuros = new ArrayList<>();
			for (DadosNovosProcessosDTO dado : dados) {
				futuros.add(CompletableFuture.supplyAsync(() -> {
					if (dado.getErro().equals(Erro.SEM_ERRO)) {
						int totalProcessos = dado.getWrapper().getTotalDeProcessosEncontrados();
						List<ListaProcessoDTO> listaProcesso = dado.getWrapper().getProcessos();
						if (totalProcessos > 30) {
							paginacaoNormal(totalProcessos, dado, listaProcesso);
						}
						// se nada der errado, adicionamos à request principal todos os processos que
						// pegamos da paginação
						if (dado.getErro().equals(Erro.SEM_ERRO))
							dado.getWrapper().setProcessos(listaProcesso);
					}
					return dado;
				}, pool));
			}
			CompletableFuture<Void> allFutures = CompletableFuture.allOf(
					futuros.toArray(new CompletableFuture[futuros.size()]));
			CompletableFuture<List<DadosNovosProcessosDTO>> resultado = allFutures.thenApply(v -> {
				return futuros.stream().map(f -> f.join()).collect(Collectors.toList());
			});
			return resultado.get();
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(StacktraceToString.convert(e));
			if (guardarLogsErros)
				daoLog.logFiliais(filiais, e);
			return new ArrayList<>();
		}
	}

	private List<DadosNovosProcessosDTO> getDados(long ini, long fim, List<Filial> filiais) {
		List<DadosNovosProcessosDTO> dados = new ArrayList<>();
		try {
			List<CompletableFuture<DadosNovosProcessosDTO>> futuros = new ArrayList<>();
			for (Filial f : filiais) {
				futuros.add(preparaFuturosDados(f, ini, fim, "", pool));
			}
			CompletableFuture<Void> allFutures = CompletableFuture.allOf(
					futuros.toArray(new CompletableFuture[futuros.size()]));
			CompletableFuture<List<DadosNovosProcessosDTO>> resultado = allFutures.thenApply(v -> {
				return futuros.stream().map(f -> f.join()).collect(Collectors.toList());
			});
			dados = resultado.get();
		} catch (Throwable e) {
			e.printStackTrace();
			warn("Erro ao pegar dados: " + StacktraceToString.convert(e));
			if (guardarLogsErros)
				daoLog.logFiliais(filiais, e);
		}
		return dados;
	}

	public int getNumClientes() {
		return numClientes;
	}

	public void setNumClientes(int numClientes) {
		this.numClientes = numClientes;
	}

	public TipoCliente getTipo() {
		return tipo;
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}
}
