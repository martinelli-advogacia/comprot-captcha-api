package com.hepta.martinelliadvogados.shared.persistence.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.hepta.martinelliadvogados.shared.Application;

public class HibernateUtil {
	private static EntityManagerFactory factory;

	public static EntityManagerFactory getEntityManagerFactory() throws Exception {
		if (factory == null) {
			createEntityManagerFactory();
		}
		return factory;
	}

	public static void shutdown() {
		if (factory != null) {
			factory.close();
		}
	}

	public static void createEntityManagerFactory() throws Exception {
		factory = Persistence.createEntityManagerFactory(Application.getDbPersistenceUnit());
	}

	public static EntityManager getEntityManager() throws Exception {
		return HibernateUtil.getEntityManagerFactory().createEntityManager();
	}

}