package com.hepta.martinelliadvogados.shared.persistence.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SuperDAO<T, I extends Serializable> {

	protected Class<T> persistedClass;
	private static final Logger LOGGER = LogManager.getLogger(SuperDAO.class);

	public SuperDAO() {
	}

	public SuperDAO(Class<T> persistedClass) {
		super();
		this.persistedClass = persistedClass;
	}

	public T save(@Valid T entity) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
			return entity;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	public T update(@Valid T newEntity) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		T entity = null;
		try {
			em.getTransaction().begin();
			entity = em.merge(newEntity);
			em.getTransaction().commit();
			return entity;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	public void delete(I id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			T entity = em.find(persistedClass, id);
			T mergedEntity = em.merge(entity);
			em.remove(mergedEntity);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}

	}

	public T find(I id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			T obj = null;
			obj = em.find(persistedClass, id);
			return obj;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

			throw e;
		} finally {
			em.close();
		}

	}

	public T getReff(I id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			T obj = null;
			obj = em.getReference(persistedClass, id);
			return obj;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

			throw e;
		} finally {
			em.close();
		}
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> List<T> getAll() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			Query query = em.createQuery("FROM " + persistedClass.getName(), persistedClass);
			return query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> List<T> getAllPaginated(Integer initialPage, Integer maxValue) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			List<T> list = new ArrayList<>();

			StringBuilder sql = new StringBuilder("FROM ");
			sql.append(persistedClass.getName());
			Query query = em.createQuery(sql.toString());
			query.setFirstResult(initialPage);
			query.setMaxResults(maxValue);
			list = query.getResultList();
			return list;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}
}
