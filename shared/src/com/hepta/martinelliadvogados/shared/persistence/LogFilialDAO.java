package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.dto.DadosNovosProcessosDTO;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.entity.logs.LogFilial;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

public class LogFilialDAO extends SuperDAO<LogFilial, Serializable> implements Serializable {
	private static final Logger LOGGER = LogManager.getLogger(LogFilialDAO.class);

	private static final long serialVersionUID = 1L;

	public LogFilialDAO() {
		super(LogFilial.class);
	}

	public void log(Filial f, String msg) throws Exception {
		LogFilial log = new LogFilial();
		String reduzida = msg.substring(0, msg.length() > 8000 ? 8000 : msg.length());
		log.setDataErro(LocalDateTime.now());
		log.setMsgErro(reduzida);
		log.setFilial(f);
		save(log);
	}

	public void log(Filial f, Throwable e) {
		try {

			log(f, StacktraceToString.convert(e));
		} catch (Exception e1) {
			e1.printStackTrace();
			LOGGER.error("ERRO AO SALVAR LOG DE FILIAL!!!" + StacktraceToString.convert(e1));
		}
	}

	public void logFiliais(List<Filial> filiais, Throwable e) {
		for (Filial f : filiais) {
			log(f, e);
		}

	}

	public void logDados(List<DadosNovosProcessosDTO> dados, Exception e) {
		for (DadosNovosProcessosDTO d : dados) {
			log(d.getFilial(), e);
		}
	}

}
