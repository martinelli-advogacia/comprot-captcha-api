package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.Demanda;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;

public class DemandaDAO extends SuperDAO<Demanda, Serializable> implements Serializable {
	private static final Logger LOG = LogManager.getLogger(DemandaDAO.class);

	private static final long serialVersionUID = 1L;

	public DemandaDAO() {
		super(Demanda.class);
	}

	@SuppressWarnings("unchecked")
	public List<Demanda> listar() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			String sql = " SELECT d.* from Demanda d "
					+ " left join demandas_filiais df on df.DEMANDA_ID = d.ID "
					+ " left join Filial f on df.FILIAL_ID = f.ID ";

			return em.createNativeQuery(sql, Demanda.class).getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	public Demanda buscarComFiliais(Long id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			String sql = " SELECT d from Demanda d "
					+ " join fetch d.filiais"
					+ " where d.id= :id";


			return (Demanda) em.createQuery(sql, Demanda.class).setParameter("id", id).getSingleResult();
		} catch (NoResultException ne) {
			return null;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}

	}
}
