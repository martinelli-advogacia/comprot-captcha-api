package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.JobStatus;
import com.hepta.martinelliadvogados.shared.enums.JobComprot;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;

public class JobStatusDAO extends SuperDAO<JobStatus, Serializable> implements Serializable {
	private static final Logger LOG = LogManager.getLogger(JobStatusDAO.class);

	private static final long serialVersionUID = 1L;

	public JobStatusDAO() {
		super(JobStatus.class);
	}

	public JobStatus achaOuCria(JobComprot job) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			String sql = " SELECT j.* from JobStatus j " + " where j.DS_JOB_NAME = :nome ";
			JobStatus status = (JobStatus) em	.createNativeQuery(sql, JobStatus.class)
									.setParameter("nome", job.toString())
									.getSingleResult();
			return status;
		} catch (NoResultException e) {
			JobStatus status = new JobStatus();
			status.setDataInicio(null);
			status.setDataFim(null);
			status.setJobName(job.toString());
			return save(status);
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

}
