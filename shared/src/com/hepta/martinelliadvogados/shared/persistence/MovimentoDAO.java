package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.Movimento;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;

public class MovimentoDAO extends SuperDAO<Movimento, Serializable> implements Serializable {
	private static final Logger LOGGER = LogManager.getLogger(MovimentoDAO.class);

	private static final long serialVersionUID = 1L;

	public MovimentoDAO() {
		super(Movimento.class);
	}
}