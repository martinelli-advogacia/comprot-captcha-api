package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import com.hepta.martinelliadvogados.shared.dto.ListaProcessoDTO;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;

public class ProcessoDAO extends SuperDAO<Processo, Serializable> implements Serializable {
	private static final Logger LOG = LogManager.getLogger(ProcessoDAO.class);

	private static final long serialVersionUID = 1L;

	public ProcessoDAO() {
		super(Processo.class);
	}

	private final String DATA_INICIAL = System.getenv("DATA_INICIAL");
	private final String DATA_FINAL = System.getenv("DATA_FINAL");

	@SuppressWarnings("unchecked")
	public Map<String, Processo> puxarDadosDesatualizados(int end, TipoCliente tipo) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Map<String, Processo> map = new HashMap<>();
		try {
			String sql = " SELECT p.* from Processo p "
					+ " left join Filial f on f.ID = p.FK_PROCESSO_FILIAL "
					+ " where p.DT_DATA_CONSULTA <=:hoje "
					+ " and p.BL_DADOS_COMPLETOS  =:completo "
					+ " and f.TP_TIPO =:tipo "
					+ " and p.DS_SITUACAO IN ('EM ANDAMENTO','EM TRANSITO') "
					+ " order by p.DT_DATA_CONSULTA asc ";

			LocalDate periodo = tipo == TipoCliente.CASA ? LocalDate.now().minusDays(7)
					: LocalDate.now().minusMonths(2);

			List<Processo> processos = em	.createNativeQuery(sql, Processo.class)
											.setParameter("completo", true)
											.setParameter("hoje", periodo.toString())
											.setParameter("tipo", tipo.pegarValor())
											.setFirstResult(0)
											.setMaxResults(end)
											.getResultList();

			for (Processo p : processos) {
				map.put(p.getNumeroProcessoEditado().replaceAll(" ", ""), p);
			}

		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Processo> puxarDadosIncompletos(int end, TipoCliente tipo) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Map<String, Processo> map = new HashMap<>();

		try {
			String sql = " SELECT p.* from Processo p "
					+ " left join Filial f on f.ID = p.FK_PROCESSO_FILIAL "
					+ " where p.BL_DADOS_COMPLETOS  =:completo "
					+ " and f.TP_TIPO =:tipo "
					+ " and p.DT_DATA_CONSULTA BETWEEN :dataInicial AND :dataFinal "
					+ " order by p.DT_DATA_CONSULTA desc ";

			List<Processo> processos = em	.createNativeQuery(sql, Processo.class)
											.setParameter("completo", false)
											.setParameter("tipo", tipo.pegarValor())
											.setParameter("dataInicial", DATA_INICIAL)
											.setParameter("dataFinal", DATA_FINAL)
											.setFirstResult(0)
											.setMaxResults(end)
											.getResultList();

			for (Processo p : processos) {
				map.put(p.getNumeroProcessoEditado().replaceAll("\\s", ""), p);
			}

		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			LOG.info(" puxar dados incompletos - fim");
			em.close();
		}
		return map;
	}

	public Processo updateProcesso(Processo ent, ProcessoWrapper wrp) throws Exception {
		ent.updateComProcesso(wrp.getProcesso());
		ent.updateMovimentos(wrp.getMovimentos());
		return update(ent);
	}

	public List<Processo> salvaNovosProcessos(List<ListaProcessoDTO> processos, Filial filial) throws Exception {
		List<Processo> res = new ArrayList<>();
		EntityManager em = HibernateUtil.getEntityManager();
		Session session = em.unwrap(Session.class);
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		int count = 0;
		for (ListaProcessoDTO p : processos) {
			Processo processo = new Processo();
			processo.insertComListaProcesso(p, filial);
			save(processo);
			res.add(processo);
			count++;
			if (count == 100) {
				count = 0;
				em.flush();
				em.clear();
			}
		}
		tx.commit();
		session.close();
		em.close();
		return res;
	}

	public List<String> salvaNovosProcessosNonBatch(List<ListaProcessoDTO> processo, Filial filial) throws Exception {
		List<Processo> processosList = new ArrayList<>();
		List<String> numProcessos = new ArrayList<>();
		for (ListaProcessoDTO p : processo) {
			Processo processoEnt = new Processo();
			processoEnt.insertComListaProcesso(p, filial);
			save(processoEnt);
			numProcessos.add(processoEnt.getNumeroProcessoEditado());
		}
		return numProcessos;
	}

	public int salvaNovosProcessosCount(List<ListaProcessoDTO> processos, Filial filial,
			Map<String, ProcessoWrapper> dadosCompletos) throws Exception {
		int novos = 0;
		int count = 0;
		EntityManager em = HibernateUtil.getEntityManager();
		Session session = em.unwrap(Session.class);
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		for (ListaProcessoDTO p : processos) {
			Processo processo = new Processo();
			String key = p.getNumeroProcessoEditado();
			if (dadosCompletos.containsKey(key)) {
				ProcessoWrapper wrapper = dadosCompletos.get(key);
				processo.insertComListaProcesso(p, filial);
				processo.updateComProcesso(wrapper.getProcesso());
				processo.updateMovimentos(wrapper.getMovimentos());
				save(processo);
				novos++;
				count++;
				if (count == 100) {
					count = 0;
					em.flush();
					em.clear();
				}
			}
		}
		tx.commit();
		session.close();
		em.close();
		return novos;
	}

	public int update(Map<String, ProcessoWrapper> processos, Map<String, Processo> processosEntity) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Session session = em.unwrap(Session.class);
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		int numAtualizados = 0;
		int count = 0;
		for (String key : processos.keySet()) {
			if (processosEntity.keySet().contains(key)) {
				Processo ent = processosEntity.get(key);
				ProcessoWrapper wrp = processos.get(key);
				ent.updateComProcesso(wrp.getProcesso());
				ent.updateMovimentos(wrp.getMovimentos());
				update(ent);
				numAtualizados++;
				count++;
				if (count == 100) {
					count = 0;
					em.flush();
					em.clear();
				}
			}
		}
		tx.commit();
		session.close();
		em.close();
		return numAtualizados;
	}

	public Processo findByNumProcessoEditado(String numeroProcessoEditado) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Processo processo = null;
		try {
			String sql = " SELECT p.* from Processo p "
					+ " where p.DS_NUMERO_PROCESSO_EDITADO =:numeroProcessoEditado ";

			processo = (Processo) em.createNativeQuery(sql, Processo.class)
									.setParameter("numeroProcessoEditado", numeroProcessoEditado)
									.getResultList().get(0);

		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
		return processo;
	}

//	public Processo salvaNovosProcessos(ListaProcessoDTO processo, Filial filial) throws Exception {
//		Processo processoEnt = new Processo();
//		processoEnt.insertComListaProcesso(processo, filial);
//		return save(processoEnt);
//	}
}
