package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.Cliente;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;

public class ClienteDAO extends SuperDAO<Cliente, Serializable> implements Serializable {
	private static final Logger LOG = LogManager.getLogger(ClienteDAO.class);

	private static final long serialVersionUID = 1L;

	public ClienteDAO() {
		super(Cliente.class);
	}
}
