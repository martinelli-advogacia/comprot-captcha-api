package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Erro;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;

public class FilialDAO extends SuperDAO<Filial, Serializable> implements Serializable {
	private static final Logger LOG = LogManager.getLogger(FilialDAO.class);

	private static final long serialVersionUID = 1L;

	public FilialDAO() {
		super(Filial.class);
	}

	@SuppressWarnings("unchecked")
	public List<Filial> porTipo(int range, TipoCliente tipoCliente, LocalDate periodo) throws Exception {
		final List<Filial> filiais = new ArrayList<>();
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			String sql = " SELECT f.* from Filial f "
					+ " where f.TP_TIPO = :tipo "
					+ " and f.DT_DATA_CONSULTA < :dataConsulta "
					+ " order by f.DT_DATA_CONSULTA asc";
			filiais.addAll(em	.createNativeQuery(sql, Filial.class)
								.setParameter("tipo", tipoCliente.pegarValor())
								.setParameter("dataConsulta", periodo.toString())
								.setFirstResult(0)
								.setMaxResults(range)
								.getResultList());
			return filiais;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Filial> porErro(int range, Erro erro) throws Exception {
		final List<Filial> filiais = new ArrayList<>();
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			String sql = " SELECT f.* from Filial f "
					+ " where f.TP_ERRO = :erro"
					+ " order by f.DT_DATA_CONSULTA asc";

			filiais.addAll(em	.createNativeQuery(sql, Filial.class)
								.setParameter("erro", erro.pegarValor())
								.setFirstResult(0)
								.setMaxResults(range)
								.getResultList());
			return filiais;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	public Filial find(String cnpj) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			String sql = " SELECT * from Filial f " + " where f.DS_CNPJ = :cnpj";

			return (Filial) em.createNativeQuery(sql, Filial.class).setParameter("cnpj", cnpj).getSingleResult();
		} catch (NoResultException e) {
			throw e;
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.error(e.getMessage());
			throw new Exception(e);
		} finally {
			em.close();
		}
	}
}
