package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.entity.logs.LogProcesso;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

public class LogProcessoDAO extends SuperDAO<LogProcesso, Serializable> implements Serializable {
	private static final Logger LOGGER = LogManager.getLogger(LogProcessoDAO.class);

	private static final long serialVersionUID = 1L;

	public LogProcessoDAO() {
		super(LogProcesso.class);
	}

	public void log(Processo p, Throwable e) {
		try {
			LogProcesso log = new LogProcesso();
			log.setDataErro(LocalDateTime.now());
			String reduzida = StacktraceToString.convert(e);
			log.setMsgErro(reduzida.substring(0, reduzida.length() > 8000 ? 8000 : reduzida.length()));
			log.setProcesso(p);
			save(log);
		} catch (Exception e1) {
			e1.printStackTrace();
			LOGGER.error("ERRO AO SALVAR LOG DE PROCESSO!!!" + StacktraceToString.convert(e1));
		}
	}

	public void log(Collection<Processo> processos, Throwable e) {
		for (Processo p : processos) {
			log(p, e);
		}

	}
}
