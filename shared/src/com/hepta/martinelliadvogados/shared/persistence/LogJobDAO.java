package com.hepta.martinelliadvogados.shared.persistence;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hepta.martinelliadvogados.shared.entity.JobStatus;
import com.hepta.martinelliadvogados.shared.entity.logs.LogJob;
import com.hepta.martinelliadvogados.shared.persistence.utils.SuperDAO;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

public class LogJobDAO extends SuperDAO<LogJob, Serializable> implements Serializable {
	private static final Logger LOGGER = LogManager.getLogger(LogJobDAO.class);

	private static final long serialVersionUID = 1L;

	public LogJobDAO() {
		super(LogJob.class);
	}

	public void log(JobStatus s, String msg) throws Exception {
		LogJob log = new LogJob();
		String reduzida = msg.substring(0, msg.length() > 8000 ? 8000 : msg.length());
		log.setDataErro(LocalDateTime.now());
		log.setMsgErro(reduzida);
		log.setJobStatus(s);
		save(log);
	}

	public void log(JobStatus s, Throwable e) {
		try {
			log(s, StacktraceToString.convert(e));
		} catch (Exception e1) {
			e1.printStackTrace();
			LOGGER.error("ERRO AO SALVAR LOG DE JOB!!!" + StacktraceToString.convert(e1));
		}
	}
}
