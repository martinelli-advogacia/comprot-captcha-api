package com.hepta.martinelliadvogados.shared.integration;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hepta.martinelliadvogados.shared.dto.ErrorWrapper;
import com.hepta.martinelliadvogados.shared.dto.ListaProcessoWrapper;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;

public class ComprotRequest {

	static final String URL_COMPROT = "https://comprot.fazenda.gov.br/comprotegov/api/processo";
	private static ObjectMapper objectMapper = new ObjectMapper();
	private static final HttpClient httpClient = HttpClient	.newBuilder()
															.version(HttpClient.Version.HTTP_1_1)
															.connectTimeout(Duration.ofSeconds(30))
															.build();

	@Deprecated
	public static HttpRequest getProcessosPorCnpjRequest(String cnpj, long dataInicial, long dataFinal,
			String numUltimoProcesso) {
		String request = URL_COMPROT
				+ "?cpfCnpj="
				+ cnpj
				+ "&tipoPesquisa=cnpj"
				+ "&dataInicial="
				+ dataInicial
				+ "&dataFinal="
				+ dataFinal
				+ "&numeroUltimoProcesso="
				+ numUltimoProcesso;
		return HttpRequest.newBuilder().GET().uri(URI.create(request)).build();
	}

	@Deprecated
	public ListaProcessoWrapper buscaProcessosPorCnpj(String cnpj, long dataInicial, long dataFinal,
			String numUltimoProcesso) throws IOException, InterruptedException {

		HttpRequest request = getProcessosPorCnpjRequest(cnpj, dataInicial, dataFinal, numUltimoProcesso);
		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		if (response.statusCode() != 200)
			throw new ConnectException("ERRO AO TENTAR ENVIAR REQUEST buscaProcessosPorCnpj PARA COMPROT: \nSTATUS"
					+ response.statusCode()
					+ "\nBODY:"
					+ response.body());
		else
			return responseToListaProcessoWrapper(response.body());
	}

	public static ListaProcessoWrapper responseToListaProcessoWrapper(String body)
			throws JsonMappingException, JsonProcessingException {
		return objectMapper.readValue(body, new TypeReference<ListaProcessoWrapper>() {
		});
	}

	@Deprecated
	public static HttpRequest getProcessoRequest(String numProcesso) {
		return HttpRequest.newBuilder().GET().uri(URI.create(URL_COMPROT + "/" + numProcesso)).build();
	}

	@Deprecated
	public ProcessoWrapper buscaProcesso(String numProcesso) throws IOException, InterruptedException {
		HttpRequest request = getProcessoRequest(numProcesso);
		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		if (response.statusCode() != 200)
			throw new ConnectException("ERRO AO TENTAR ENVIAR REQUEST buscaProcesso PARA COMPROT: \nSTATUS"
					+ response.statusCode()
					+ "\nBODY:"
					+ response.body());
		else
			return responseToProcessoWrapper(response.body());
	}

	public static ProcessoWrapper responseToProcessoWrapper(String body)
			throws JsonMappingException, JsonProcessingException {
		return objectMapper.readValue(body, new TypeReference<ProcessoWrapper>() {
		});
	}

	public static ErrorWrapper responseToErrorWrapper(String body)
			throws JsonMappingException, JsonProcessingException {
		body = body.replace("[", "").replace("]", "");
		return objectMapper.readValue(body, new TypeReference<ErrorWrapper>() {
		});
	}

	public static HttpRequest getProcessosPorCnpjRequest(String cnpj, long dataInicial, long dataFinal,
			String numUltimoProcesso, String token, String captcha) {
		String request = URL_COMPROT
				+ "?cpfCnpj="
				+ cnpj
				+ "&tipoPesquisa=cnpj"
				+ "&dataInicial="
				+ dataInicial
				+ "&dataFinal="
				+ dataFinal
				+ "&numeroUltimoProcesso="
				+ numUltimoProcesso;
		String captchaToken = "{\"token\":\"" + token + "\",\"texto\":\"" + captcha + "\"}";
		return HttpRequest.newBuilder().GET().uri(URI.create(request)).header("Captcha", captchaToken).build();
	}

	public ListaProcessoWrapper buscaProcessosPorCnpj(String cnpj, long dataInicial, long dataFinal,
			String numUltimoProcesso, String token, String captcha) throws IOException, InterruptedException {

		HttpRequest request = getProcessosPorCnpjRequest(cnpj, dataInicial, dataFinal, numUltimoProcesso, token,
				captcha);
		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		if (response.statusCode() != 200)
			throw new ConnectException("ERRO AO TENTAR ENVIAR REQUEST buscaProcessosPorCnpj PARA COMPROT: \nSTATUS"
					+ response.statusCode()
					+ "\nBODY:"
					+ response.body());
		else
			return responseToListaProcessoWrapper(response.body());
	}

	public static HttpRequest getProcessoRequest(String numProcesso, String token, String captcha) {
		String captchaToken = "{\"token\":\"" + token + "\",\"texto\":\"" + captcha + "\"}";
		return HttpRequest	.newBuilder()
							.GET()
							.uri(URI.create(URL_COMPROT + "/" + numProcesso.strip().trim()))
							.header("Captcha", captchaToken)
							.build();
	}

	public ProcessoWrapper buscaProcesso(String numProcesso, String token, String captcha)
			throws IOException, InterruptedException {
		HttpRequest request = getProcessoRequest(numProcesso, token, captcha);
		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		if (response.statusCode() != 200)
			throw new ConnectException("ERRO AO TENTAR ENVIAR REQUEST buscaProcesso PARA COMPROT: \nSTATUS"
					+ response.statusCode()
					+ "\nBODY:"
					+ response.body());
		else
			return responseToProcessoWrapper(response.body());
	}

}
