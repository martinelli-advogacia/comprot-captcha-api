package com.hepta.martinelliadvogados.shared;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

	private Application() {
	}

	private static final Logger LOGGER = LogManager.getLogger(Application.class);

	private static final Properties mapProperties = loadProperties();

	public static Properties loadProperties() {
		try {
			LOGGER.info("Carregando informações");
			InputStream inputStream = Application.class.getClassLoader().getResourceAsStream("config.properties");
			Properties properties = new Properties();
			properties.load(inputStream);
			return properties;
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return null;
	}

	public static String getStringAppProp(String prop) {
		return (String) mapProperties.get(prop);
	}

	public static Integer getIntegerAppProp(String prop) {
		try {
			return Integer.valueOf((String) mapProperties.get(prop));
		} catch (NumberFormatException e) {
			LOGGER.error("Numero inválido no arquivo de configuração!!! Retornando como 0.");
			e.printStackTrace();
			return 0;
		}
	}

	public static String getApplicationEnv() {
		return getStringAppProp("application.env");
	}

	public static String getApplicationDomain() {
		return getStringAppProp("application.domain");
	}

	public static String getApplicationContextPath() {
		return getStringAppProp("application.contextPath");
	}

	public static String getApplicationHttpProtocolo() {
		return getStringAppProp("application.http.protocolo");
	}

	public static String getApplicationDomainFront() {
		return getStringAppProp("application.domain.front");
	}

	public static String getDbPersistenceUnit() {
		return getStringAppProp("db.persistenceUnit");
	}

	public static String getDbDriverClassName() {
		return getStringAppProp("db.driverClassName");
	}

	public static String getDbUrl() {
		return getStringAppProp("db.url");
	}

	public static String getDbUsername() {
		return getStringAppProp("db.username");
	}

	public static String getDbPassword() {
		return getStringAppProp("db.password");
	}

	public static String getHibernateDialect() {
		return getStringAppProp("hibernate.dialect");
	}

	public static String getHibernateShow_sql() {
		return getStringAppProp("hibernate.show_sql");
	}

	public static String getHibernateFormat_sql() {
		return getStringAppProp("hibernate.format_sql");
	}

	public static String getHibernateHbm2ddlAuto() {
		return getStringAppProp("hibernate.hbm2ddl.auto");
	}

	public static String getHibernateHikariConnectionTimeout() {
		return getStringAppProp("hibernate.hikari.connectionTimeout");
	}

	public static String getHibernateHikariMinimumIdle() {
		return getStringAppProp("hibernate.hikari.minimumIdle");
	}

	public static String getHibernateHikariMaximumPoolSize() {
		return getStringAppProp("hibernate.hikari.maximumPoolSize");
	}

	public static String getHibernateHikariIdleTimeout() {
		return getStringAppProp("hibernate.hikari.idleTimeout");
	}

	public static Integer getComprotJobsThreadpoolNovos() {
		return getIntegerAppProp("comprot.jobs.threadpool.novos");
	}

	public static Integer getComprotJobsThreadpoolBusca() {
		return getIntegerAppProp("comprot.jobs.threadpool.busca");
	}

	public static Integer getComprotJobsThreadpoolAtualiza() {
		return getIntegerAppProp("comprot.jobs.threadpool.atualiza");
	}

	public static Integer getComprotJobsThreadpoolErros() {
		return getIntegerAppProp("comprot.jobs.threadpool.erros");
	}

	public static Integer getComprotJobsQuantidadeNovosCasa() {
		return getIntegerAppProp("comprot.jobs.quantidade.novos.casa");
	}

	public static Integer getComprotJobsQuantidadeNovosProspeccao() {
		return getIntegerAppProp("comprot.jobs.quantidade.novos.prospeccao");
	}

	public static Integer getComprotJobsQuantidadeBuscaCasa() {
		return getIntegerAppProp("comprot.jobs.quantidade.busca.casa");
	}

	public static Integer getComprotJobsQuantidadeBuscaProspeccao() {
		return getIntegerAppProp("comprot.jobs.quantidade.busca.prospeccao");
	}

	public static Integer getComprotJobsQuantidadeAtualizaCasa() {
		return getIntegerAppProp("comprot.jobs.quantidade.atualiza.casa");
	}

	public static Integer getComprotJobsQuantidadeAtualizaProspeccao() {
		return getIntegerAppProp("comprot.jobs.quantidade.atualiza.prospeccao");
	}

	public static Integer getComprotJobsQuantidadeErros() {
		return getIntegerAppProp("comprot.jobs.quantidade.erros");
	}

	public static String getSmtpHost() {
		return getStringAppProp("smtp.host");
	}

	public static String getSmtpPort() {
		return getStringAppProp("smtp.port");
	}

	public static String getSmtpUser() {
		return getStringAppProp("smtp.user");
	}

	public static String getSmtpPassword() {
		return getStringAppProp("smtp.password");
	}

	public static String getSmtpContato() {
		return getStringAppProp("smtp.contato");
	}

	public static String getDataInicial() {
		return getStringAppProp("data.inicial");
	}

	public static String getDataFinal() {
		return getStringAppProp("data.final");
	}

	public static String getQtdBusca() {
		return getStringAppProp("qtd.busca");
	}

	public static String getExecutavelPython() {
		return getStringAppProp("executavel.python");
	}

	public static String getCaminhoPastaDownloadsCaptcha() {
		return getStringAppProp("caminho.downloads.captcha");
	}

	public static String getCaminhoPastaCaptchaResolvidos() {
		return getStringAppProp("caminho.resolvidos.captcha");
	}

	public static String getScriptPathPython() {
		return getStringAppProp("script.path.captcha.comprot");
	}

	public static String getChromedriverPath() {
		return getStringAppProp("chromedriver.path");
	}
}
