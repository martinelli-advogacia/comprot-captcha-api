package com.hepta.martinelliadvogados.shared.enums;

public enum Erro {
	SEM_ERRO(0),
	CNPJ_INVALIDO(1),
	PROCESSOS_DEMAIS(2),
	DESCONHECIDO(3),
	ERRO_NA_PAGINACAO(4),
	FALTA_PROCESSOS(5), CAPTCHA(6);

	private final int valor;

	Erro(final int valor) {
		this.valor = valor;
	}

	public int pegarValor() {
		return valor;
	}
}
