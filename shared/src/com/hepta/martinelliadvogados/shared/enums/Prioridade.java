package com.hepta.martinelliadvogados.shared.enums;

public enum Prioridade {
	IMEDIATO(0),
	CRITICO(1),
	ALTO(2),
	MEDIO(3),
	BAIXO(4),
	MINIMO(5);
	
	private final int valor;
	Prioridade(final int valor) {
		this.valor = valor;
	}

	public int pegarValor() {
		return valor;
	}
}
