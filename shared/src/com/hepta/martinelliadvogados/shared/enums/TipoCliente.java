package com.hepta.martinelliadvogados.shared.enums;

public enum TipoCliente {
	CASA(0),
	PROSPECCAO(1);

	private final int valor;

	TipoCliente(final int valor) {
		this.valor = valor;
	}

	public int pegarValor() {
		return valor;
	}
}
