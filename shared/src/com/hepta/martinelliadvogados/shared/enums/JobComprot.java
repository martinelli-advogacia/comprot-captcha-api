package com.hepta.martinelliadvogados.shared.enums;

public enum JobComprot {
	ACHA_NOVOS_CASA(0),
	BUSCA_DADOS_CASA(1),
	ATUALIZA_CASA(2),
	ACHA_NOVOS_PROSPECCAO(3),
	BUSCA_DADOS_PROSPECCAO(4),
	ATUALIZA_PROSPECCAO(5),
	ERROS(6);

	private final int valor;

	JobComprot(final int valor) {
		this.valor = valor;
	}

	public int pegarValor() {
		return valor;
	}

}
