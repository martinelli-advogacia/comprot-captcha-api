package com.hepta.martinelliadvogados.shared.enums;

public enum EstadoDemanda {
	EM_ANDAMENTO(0),
	FINALIZADA(1);
	
	private final int valor;

	EstadoDemanda(final int valor) {
		this.valor = valor;
	}

	public int pegarValor() {
		return valor;
	}
}
