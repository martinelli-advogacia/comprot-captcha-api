package com.hepta.martinelliadvogados.shared.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.hibernate.Session;

import com.hepta.martinelliadvogados.shared.entity.Cliente;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Prioridade;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;

public class CriaClienteFiliais {
	public void criar() throws Exception {
		Set<String> cnpjs = buscaArquivo("cnpjs_todos");
		Set<String> cnpjsCarteira = buscaArquivo("cnpjs_carteira");
		Map<String, Cliente> mapaCnpjs = new HashMap<>();
		// removendo duplicados
		for (String c : cnpjsCarteira) {
			if (cnpjs.contains(c))
				cnpjs.remove(c);
		}

		criaClienteFilial(cnpjs, mapaCnpjs, Prioridade.CRITICO, TipoCliente.PROSPECCAO);
		criaClienteFilial(cnpjsCarteira, mapaCnpjs, Prioridade.MEDIO, TipoCliente.CASA);

		System.out.println(mapaCnpjs.keySet().size());
		System.out.println(cnpjs.size());
		System.out.println(cnpjsCarteira.size());
		System.out.println("Inicio");
		CronometroExecucao<String> cronometro1 = new CronometroExecucao<>();
		cronometro1.cronometrar(() -> {
			EntityManager em = HibernateUtil.getEntityManager();
			Session session = em.unwrap(Session.class);
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			int count = 0;
			for (Cliente c : mapaCnpjs.values()) {
				em.persist(c);
				count++;
				if (count == 100) {
					count = 0;
					em.flush();
					em.clear();
				}
			}
			tx.commit();
			session.close();
			em.close();
			return "";
		}, "salvando no banco");
		System.out.println("Completado");
	}

	private void criaClienteFilial(Set<String> cnpjs, Map<String, Cliente> mapaCnpjs, Prioridade prioridade,
			TipoCliente tipo) {
		for (String c : cnpjs) {
			if (c.equals("NULL"))
				break;
			CNPJUtil cnpj = new CNPJUtil(c);
			String prefix = cnpj.getPrefix();
			String suffix = cnpj.getSuffix();
			if (mapaCnpjs.containsKey(prefix)) {
				Filial filial = new Filial();
				filial.setCliente(mapaCnpjs.get(prefix));
				filial.setCnpj(c);
				filial.setCnpjSufixo(suffix);
				filial.setTipo(tipo);
				mapaCnpjs.get(prefix).getFiliais().add(filial);
			} else {
				Cliente cliente = new Cliente();
				cliente.setCnpjPrefixo(prefix);
				cliente.setFiliais(new ArrayList<>());
				Filial filial = new Filial();
				filial.setCliente(cliente);
				filial.setCnpj(c);
				filial.setCnpjSufixo(suffix);
				filial.setTipo(tipo);
				cliente.getFiliais().add(filial);
				mapaCnpjs.put(prefix, cliente);
			}
		}
	}

	private Set<String> buscaArquivo(String nomeArquivo) throws FileNotFoundException {
		System.out.println("Working Directory = " + System.getProperty("user.dir"));
		Scanner sc = new Scanner(new File(System.getProperty("user.dir") + "/" + nomeArquivo));
		Set<String> lines = new HashSet<>();
		while (sc.hasNextLine()) {
			lines.add(sc.nextLine());
		}
		sc.close();
		return lines;
	}
}
