package com.hepta.martinelliadvogados.shared.utils;

import java.time.Instant;
import java.time.LocalDateTime;

public class DataHelper {

	public static final long getDataInicial() {
		return 0L;
	}

	public static final long getDataAtual() {
		return Instant.now().toEpochMilli();
	}

	public static final boolean isHorarioFuncionamentoComprot() {
		LocalDateTime dataHoraAtual = LocalDateTime.now();
		int diaDaSemana = dataHoraAtual.getDayOfWeek().getValue();
		return diaDaSemana != 6 && diaDaSemana != 7 && dataHoraAtual.getHour() > 7 && dataHoraAtual.getHour() < 23;
	}

}
