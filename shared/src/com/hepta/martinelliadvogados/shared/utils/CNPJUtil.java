package com.hepta.martinelliadvogados.shared.utils;

public class CNPJUtil {
	String prefix;
	String suffix;
	String formatado;

	public CNPJUtil(String cnpj) {
		super();
		prefix = cnpj.substring(0, 8);
		suffix = cnpj.substring(8);
		formatado = cnpj.substring(0,1) + "." + cnpj.substring(2,4) + "." + cnpj.substring(5,7) + "/" + cnpj.substring(8,11) + "-" ;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getFormatado() {
		return formatado;
	}

	public void setFormatado(String formatado) {
		this.formatado = formatado;
	}
}
