package com.hepta.martinelliadvogados.shared.utils;

import java.util.concurrent.Callable;

import org.jboss.logging.Logger;

public class CronometroExecucao<T> {
	
	private static final Logger LOG = Logger.getLogger(CronometroExecucao.class);

	public T cronometrar(final Callable<T> funcao, final String message) {
		long start, end;
		long duracao;
		start = System.currentTimeMillis();
		T retorno = null;
		try {
			retorno = funcao.call();
		} catch (final Exception e) {
			LOG.error("Houve um erro durante a contagem do " + message, e);
			e.printStackTrace();
		}
		end = System.currentTimeMillis();
		duracao = end - start;
		long minutos = (duracao / 1000) / 60;
		long segundos = (duracao / 1000) % 60;
		long mili = (duracao % 1000) % 60;
		LOG.info(String.format("%s: %dm:%ds:%dms", message, minutos, segundos, mili));

		return retorno;
	}
}