package com.hepta.martinelliadvogados.shared.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.hibernate.Session;

import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.FilialDAO;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;

public class CheckClientesFiliais {
	public void check() throws Exception {
		FilialDAO dao = new FilialDAO();
		Set<Filial> filiais = new HashSet<>(dao.getAll());

		Set<Filial> mapaCnpjs = carteira(filiais);
		//Set<Filial> mapaCnpjs = prospeccao(filiais);

		System.out.println(mapaCnpjs.size());
		System.out.println("Inicio");
		CronometroExecucao<String> cronometro1 = new CronometroExecucao<>();
		cronometro1.cronometrar(() -> {
			EntityManager em = HibernateUtil.getEntityManager();
			Session session = em.unwrap(Session.class);
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			int count = 0;
			for (Filial c : mapaCnpjs) {
				dao.update(c);
				count++;
				if (count == 100) {
					count = 0;
					em.flush();
					em.clear();
				}
			}
			tx.commit();
			session.close();
			em.close();
			return "";
		}, "salvando no banco");
		System.out.println("Completado");
	}

	protected Set<Filial> carteira(Set<Filial> clientes) throws FileNotFoundException {
		Set<String> cnpjsCarteira = buscaArquivo("cnpjs_carteira");

		Set<Filial> mapaCnpjs = new HashSet<>();
		for (Filial c : clientes) {
			if (cnpjsCarteira.contains(c.getCnpj()) && (c.getTipo() == null || !c.getTipo().equals(TipoCliente.CASA))) {
				c.setTipo(TipoCliente.CASA);
				mapaCnpjs.add(c);
			}
		}
		return mapaCnpjs;
	}

	
	protected Set<Filial> prospeccao(Set<Filial> filiais) throws FileNotFoundException {
		Set<String> cnpjsCarteira = buscaArquivo("cnpjs_carteira");
		Set<String> cnpjsTodos = buscaArquivo("cnpjs_todos");
		Set<String> cnpjsProspeccao = new HashSet<>();
		for (String s : cnpjsTodos) {
			if (!cnpjsCarteira.contains(s)) {
				cnpjsProspeccao.add(s);
			}
		}

		Set<Filial> mapaCnpjs = new HashSet<>();
		for (Filial c : filiais) {
			if (cnpjsProspeccao.contains(c.getCnpj()) && (c.getTipo() == null || !c.getTipo().equals(TipoCliente.PROSPECCAO))) {
				c.setTipo(TipoCliente.PROSPECCAO);
				mapaCnpjs.add(c);
			}
		}
		return mapaCnpjs;
	}

	private Set<String> buscaArquivo(String nomeArquivo) throws FileNotFoundException {
		System.out.println("Working Directory = " + System.getProperty("user.dir"));
		Scanner sc = new Scanner(new File(System.getProperty("user.dir") + "/" + nomeArquivo));
		Set<String> lines = new HashSet<>();
		while (sc.hasNextLine()) {
			lines.add(sc.nextLine());
		}
		sc.close();
		return lines;
	}
}
