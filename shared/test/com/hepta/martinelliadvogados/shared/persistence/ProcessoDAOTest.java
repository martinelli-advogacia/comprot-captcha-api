package com.hepta.martinelliadvogados.shared.persistence;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.ProcessoDAO;

class ProcessoDAOTest {
	ProcessoDAO dao = new ProcessoDAO();
	int range = 250;
	@Test
	void testFindSetOfString() {
		try {
			Set<String> ids = new HashSet<>(); 
			List<Processo> list = dao.find(ids);
			System.out.println(list.size());
			assert (!list.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testPuxarDadosDesatualizados() {
		try {
			Map<String, Processo> list = dao.puxarDadosDesatualizados(range, TipoCliente.CASA);
			System.out.println(list.size());
			assert (!list.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testPuxarDadosIncompletos() {
		try {
			Map<String, Processo> list = dao.puxarDadosIncompletos(range,  TipoCliente.CASA);
			System.out.println(list.size());
			assert (!list.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testUpdateProcesso() {
		fail();
	}

	@Test
	void testSalvaNovoProcesso() {
		fail();
	}

}
