package com.hepta.martinelliadvogados.shared.etc;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

public class CompletableFutureTest {
	private static final ExecutorService pool = Executors.newFixedThreadPool(10);

	@Test
	void testCompletableFuture() {
		try {
			System.out.println("Processors = " + Runtime.getRuntime().availableProcessors());

			List<CompletableFuture<String>> futuros = new ArrayList<>();
			
			futuros.add(getData(0));
			futuros.add(getData(1));
			CompletableFuture<Void> allFutures = CompletableFuture.allOf(
					futuros.toArray(new CompletableFuture[futuros.size()])
			);
			CompletableFuture<List<String>> resultado = allFutures.thenApply(v -> {
				   return futuros.stream()
				           .map(f -> f.join())
				           .collect(Collectors.toList());
				});
			List<String> res = resultado.get();
			System.out.println(res);

			pool.shutdown();
			assert (true);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	public static CompletableFuture<String> getData(final int index) throws InterruptedException {
		return CompletableFuture.supplyAsync(() -> {
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				throw new IllegalStateException(e);
			}
			return "Result of the asynchronous computation "+index;
		},pool);
	}
}
