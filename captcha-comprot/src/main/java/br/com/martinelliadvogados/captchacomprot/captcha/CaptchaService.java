package br.com.martinelliadvogados.captchacomprot.captcha;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.persistence.NoResultException;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.dto.ListaProcessoWrapper;
import com.hepta.martinelliadvogados.shared.dto.ProcessoDTO;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.integration.ComprotRequest;
import com.hepta.martinelliadvogados.shared.persistence.FilialDAO;
import com.hepta.martinelliadvogados.shared.persistence.ProcessoDAO;
import com.hepta.martinelliadvogados.shared.utils.DataHelper;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

import br.com.martinelliadvogados.captchacomprot.dto.CaptchaDTO;
import br.com.martinelliadvogados.captchacomprot.dto.CaptchaProcessoDTO;
import br.com.martinelliadvogados.captchacomprot.dto.ResponseListaProcessosDTO;
import br.com.martinelliadvogados.captchacomprot.exceptions.CaptchaErradoComprotException;
import br.com.martinelliadvogados.captchacomprot.exceptions.HeptaException;
import br.com.martinelliadvogados.captchacomprot.exceptions.SemDadosComprotException;

public class CaptchaService {
	private static Logger LOG = Logger.getLogger(CaptchaService.class);

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		try {
			CaptchaDTO aux = CaptchaService.getCaptchaListaProcesso();
			long dataInicial = DataHelper.getDataInicial();
			long dataFinal = DataHelper.getDataAtual();
			Scanner myObj = new Scanner(System.in);
			String resposta = myObj.nextLine();
			ListaProcessoWrapper dados = getListaProcessosComprot("92012467000170", dataInicial, dataFinal, "",
					aux.getToken(), resposta);
			System.out.println(dados.getTotalDeProcessosEncontrados());
		} catch (Exception e) {
			LOG.error(StacktraceToString.convert(e));
			e.printStackTrace();
		}
	}

	public static CaptchaProcessoDTO getCaptchaProcesso() throws IOException {
		List<String> results = rodaPython();
		if (results.size() < 3) {
			System.out.println(results);
			LOG.error(results);
			return null;
		}
		String imgPath = results.get(results.size() - 3);
		String somPath = results.get(results.size() - 2);
		String token = results.get(results.size() - 1);
		String imagem = encodeFileToBase64Binary(new File(imgPath));
		String som = encodeFileToBase64Binary(new File(somPath));

		CaptchaProcessoDTO captcha = new CaptchaProcessoDTO();
		captcha.setImgCaptcha64(imagem);
		captcha.setSomCaptcha64(som);
		captcha.setToken(token);
		return captcha;
	}

	public static CaptchaDTO getCaptchaListaProcesso() throws IOException {
		List<String> results = rodaPython();
		if (results.size() < 3) {
			System.out.println(results);
			LOG.error(results);
			return null;
		}
		String imgPath = results.get(results.size() - 3);
		String somPath = results.get(results.size() - 2);
		String token = results.get(results.size() - 1);
		String imagem = encodeFileToBase64Binary(new File(imgPath));
		String som = encodeFileToBase64Binary(new File(somPath));

		CaptchaDTO captcha = new CaptchaDTO();
		captcha.setImgCaptcha64(imagem);
		captcha.setSomCaptcha64(som);
		captcha.setToken(token);
		return captcha;
	}

	public static List<String> rodaPython() throws IOException {
		File file = new File(Application.getScriptPathPython() + "captchaDownloader.py");
		ProcessBuilder processBuilder = new ProcessBuilder(Application.getExecutavelPython(), file.getAbsolutePath(),
				"primeira");
		processBuilder.redirectErrorStream(true);
		Process process = processBuilder.start();
		return readProcessOutput(process.getInputStream());
	}

	private static List<String> readProcessOutput(InputStream inputStream) throws IOException {
		try (BufferedReader output = new BufferedReader(new InputStreamReader(inputStream))) {
			return output.lines().collect(Collectors.toList());
		}
	}

	private static String encodeFileToBase64Binary(File file) throws IOException {
		String encodedfile = null;
		FileInputStream fileInputStreamReader = new FileInputStream(file);
		byte[] bytes = new byte[(int) file.length()];
		fileInputStreamReader.read(bytes);
		encodedfile = new String(Base64.getEncoder().encode(bytes), "UTF-8");
		fileInputStreamReader.close();
		return encodedfile;
	}

	public static ResponseListaProcessosDTO salvarListaProcessos(String cnpj, ListaProcessoWrapper dados) throws Exception {
		FilialDAO daoFilial = new FilialDAO();
		ProcessoDAO daoProcesso = new ProcessoDAO();
		Filial filial = null;
		try {
			filial = daoFilial.find(cnpj);
		} catch (NoResultException e) {
			filial = new Filial();
			filial.setCnpj(cnpj);
			filial = daoFilial.save(filial);
		}
		List<ProcessoDTO> processos = new ArrayList<>();
		List<String> numProcessos = daoProcesso.salvaNovosProcessosNonBatch(dados.getProcessos(), filial);
		List<Processo> processosEnt = new ArrayList<>();
		for (String num : numProcessos) {
			processosEnt.add(daoProcesso.findByNumProcessoEditado(num));
		}

		for (Processo ent : processosEnt) {
			ent.setMovimentos(null);
			processos.add(new ProcessoDTO(ent));
		}
		ResponseListaProcessosDTO res = new ResponseListaProcessosDTO();
		res.setProcessos(processos);
		res.setNumProcessoTotal(dados.getTotalDeProcessosEncontrados());
		
		int ultimoIndex = processos.size()-1;
		String ultimo = processos.get(ultimoIndex).getNumeroProcessoEditado();
		res.setNumUltimoProcesso(ultimo);
		return res;
	}

	public static ListaProcessoWrapper getListaProcessosComprot(String cnpj, long dataInicial, long dataFinal,
			String ultimoProcesso, String token, String captcha)
			throws IOException, InterruptedException, HeptaException{
		HttpRequest req = ComprotRequest.getProcessosPorCnpjRequest(cnpj, dataInicial, dataFinal, ultimoProcesso, token,
				captcha);
		HttpClient client = HttpClient.newHttpClient();
		HttpResponse<String> responseCompleted = client.send(req, HttpResponse.BodyHandlers.ofString());
		ListaProcessoWrapper res = null;
		if (responseCompleted.statusCode() == 200) {
			res = ComprotRequest.responseToListaProcessoWrapper(responseCompleted.body());
		} else if (responseCompleted.statusCode() == 204) {
			System.out.println(responseCompleted.statusCode());
			System.out.println(responseCompleted.body());
			LOG.error(responseCompleted.statusCode());
			LOG.error(responseCompleted.body());
			throw new SemDadosComprotException();
		} else if (responseCompleted.statusCode() == 422) {
			System.out.println(responseCompleted.statusCode());
			System.out.println(responseCompleted.body());
			LOG.error(responseCompleted.statusCode());
			LOG.error(responseCompleted.body());
			throw new CaptchaErradoComprotException();
		} else {
			System.out.println(responseCompleted.statusCode());
			System.out.println(responseCompleted.body());
			LOG.error(responseCompleted.statusCode());
			LOG.error(responseCompleted.body());
		}

		return res;
	}

	public static ProcessoWrapper getProcesso(String numProcesso, String token, String captcha)
			throws IOException, InterruptedException, HeptaException {
		HttpRequest req = ComprotRequest.getProcessoRequest(numProcesso, token, captcha);
		HttpClient client = HttpClient.newHttpClient();
		HttpResponse<String> responseCompleted = client.send(req, HttpResponse.BodyHandlers.ofString());
		ProcessoWrapper res = null;
		if (responseCompleted.statusCode() == 200) {
			res = ComprotRequest.responseToProcessoWrapper(responseCompleted.body());
		} else if (responseCompleted.statusCode() == 204) {
			throw new SemDadosComprotException();
		} else if (responseCompleted.statusCode() == 422) {
			throw new CaptchaErradoComprotException();
		} else {
			System.out.println(responseCompleted.statusCode());
			System.out.println(responseCompleted.body());
			LOG.error(responseCompleted.statusCode());
			LOG.error(responseCompleted.body());
		}
		return res;
	}

	public static ProcessoDTO salvarProcesso(ProcessoWrapper dados) throws Exception {
		ProcessoDAO daoProcesso = new ProcessoDAO();
		Processo ent = daoProcesso.findByNumProcessoEditado(dados.getProcesso().getNumeroProcessoEditado());
		ent = daoProcesso.updateProcesso(ent, dados);
		ent.setMovimentos(null);
		return new ProcessoDTO(ent);
	}

	public static void salvaCaptchaCorreto(CaptchaProcessoDTO msg) throws IOException {
		base64ToPng(msg.getImgCaptcha64(), msg.getRespostaCaptcha());
		base64ToWav(msg.getSomCaptcha64(), msg.getRespostaCaptcha());
	}

	public static void salvaCaptchaCorreto(CaptchaDTO msg) throws IOException {
		base64ToPng(msg.getImgCaptcha64(), msg.getRespostaCaptcha());
		base64ToWav(msg.getSomCaptcha64(), msg.getRespostaCaptcha());
	}

	private static void base64ToPng(String base64Img, String filename) throws IOException {
		byte[] imageBytes = Base64.getDecoder().decode(base64Img);
		BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageBytes));
		File outputfile = new File(Application.getCaminhoPastaCaptchaResolvidos() + filename + ".png");
		ImageIO.write(image, "png", outputfile);
	}

	private static void base64ToWav(String base64Wav, String filename) throws IOException {
		byte[] imageBytes = Base64.getDecoder().decode(base64Wav);
		File file2 = new File(Application.getCaminhoPastaCaptchaResolvidos() + filename + ".wav");
		FileOutputStream os = new FileOutputStream(file2, true);
		os.write(imageBytes);
		os.close();
	}
}
