package br.com.martinelliadvogados.captchacomprot.dto;

public class CaptchaDTO {
	RequestListaProcessosDTO request;
	String token;
	String respostaCaptcha;
	String imgCaptcha64;
	String somCaptcha64;

	public CaptchaDTO() {
		super();
	}

	public CaptchaDTO(RequestListaProcessosDTO request, String token, String respostaCaptcha, String imgCaptcha64,
			String somCaptcha64) {
		super();
		this.request = request;
		this.token = token;
		this.respostaCaptcha = respostaCaptcha;
		this.imgCaptcha64 = imgCaptcha64;
		this.somCaptcha64 = somCaptcha64;
	}

	public RequestListaProcessosDTO getRequest() {
		return request;
	}

	public void setRequest(RequestListaProcessosDTO request) {
		this.request = request;
	}

	public String getRespostaCaptcha() {
		return respostaCaptcha;
	}

	public void setRespostaCaptcha(String respostaCaptcha) {
		this.respostaCaptcha = respostaCaptcha;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getImgCaptcha64() {
		return imgCaptcha64;
	}

	public void setImgCaptcha64(String imgCaptcha64) {
		this.imgCaptcha64 = imgCaptcha64;
	}

	public String getSomCaptcha64() {
		return somCaptcha64;
	}

	public void setSomCaptcha64(String somCaptcha64) {
		this.somCaptcha64 = somCaptcha64;
	}

}
