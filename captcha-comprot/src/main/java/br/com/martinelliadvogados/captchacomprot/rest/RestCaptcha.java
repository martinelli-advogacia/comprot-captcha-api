package br.com.martinelliadvogados.captchacomprot.rest;

import java.io.IOException;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import com.hepta.martinelliadvogados.shared.dto.ListaProcessoWrapper;
import com.hepta.martinelliadvogados.shared.dto.ProcessoDTO;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

import br.com.martinelliadvogados.captchacomprot.captcha.CaptchaService;
import br.com.martinelliadvogados.captchacomprot.dto.CaptchaDTO;
import br.com.martinelliadvogados.captchacomprot.dto.CaptchaProcessoDTO;
import br.com.martinelliadvogados.captchacomprot.dto.RequestListaProcessosDTO;
import br.com.martinelliadvogados.captchacomprot.dto.RequestProcessoDTO;
import br.com.martinelliadvogados.captchacomprot.dto.ResponseListaProcessosDTO;
import br.com.martinelliadvogados.captchacomprot.exceptions.HeptaException;

@Path("/captcha")
public class RestCaptcha {
	private static Logger LOG = Logger.getLogger(RestCaptcha.class);

	@POST
	@Path("/listaProcessos")
	public Response listaProcessos(RequestListaProcessosDTO msg) {
		try {
			CaptchaDTO captcha = CaptchaService.getCaptchaListaProcesso();
			captcha.setRequest(msg);
			return Response.ok().entity(captcha).build();
		} catch (Exception e) {
			e.printStackTrace();
			String msgErro  =StacktraceToString.convert(e);
			LOG.error(msgErro);
			return Response.serverError().entity("Erro ao buscar captcha" +msgErro).build();
		}
	}

	@POST
	@Path("/listaProcessos/captcha")
	public Response listaProcessosCaptcha(CaptchaDTO msg) {
		try {
			String token = msg.getToken();
			String captcha = msg.getRespostaCaptcha();
			RequestListaProcessosDTO req = msg.getRequest();
			String cnpj = req.getCnpj();
			String ultimoProcesso = req.getNumUltimoProcesso();

			long dataFinal = req.getDataFinal();
			long dataInicial = req.getDataInicial();
			ListaProcessoWrapper dados = CaptchaService.getListaProcessosComprot(cnpj, dataInicial, dataFinal,
					ultimoProcesso, token, captcha);
			if (dados != null) {
				CaptchaService.salvaCaptchaCorreto(msg);
				ResponseListaProcessosDTO res = CaptchaService.salvarListaProcessos(cnpj, dados);
				res.setCnpj(cnpj);
				res.setDataFinal(dataFinal);
				res.setDataInicial(dataInicial);
				return Response.ok().entity(res).build();
			} else {
				LOG.error("Deu erro na busca");
				throw new Exception("Deu erro na busca");
			}
		} catch (HeptaException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (Exception e) {
			// se der qualquer erro, mandamos a request de volta com novo captcha
			e.printStackTrace();
			LOG.error(StacktraceToString.convert(e));
			CaptchaDTO captcha = null;
			try {
				captcha = CaptchaService.getCaptchaListaProcesso();
				captcha.setRequest(msg.getRequest());
			} catch (IOException e1) {
				LOG.error(StacktraceToString.convert(e1));
				e1.printStackTrace();
			}

			return Response.serverError().entity(captcha).build();
		}
	}

	@POST
	@Path("/processo")
	public Response processo(RequestProcessoDTO msg) {
		try {
			CaptchaProcessoDTO captcha = CaptchaService.getCaptchaProcesso();
			captcha.setRequest(msg);
			return Response.ok().entity(captcha).build();
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(StacktraceToString.convert(e));
			return Response.serverError().entity("Erro ao buscar captcha").build();
		}
	}

	@POST
	@Path("/processo/captcha")
	public Response processoCaptcha(CaptchaProcessoDTO msg) {
		try {
			String token = msg.getToken();
			String captcha = msg.getRespostaCaptcha();
			RequestProcessoDTO req = msg.getRequest();
			String numProcesso = req.getNumProcesso();
			ProcessoWrapper dados = CaptchaService.getProcesso(numProcesso, token, captcha);
			if (dados != null) {
				CaptchaService.salvaCaptchaCorreto(msg);
				ProcessoDTO processo = CaptchaService.salvarProcesso(dados);
				return Response.ok().entity(processo).build();
			} else {
				LOG.error("Deu erro na busca");
				throw new Exception("Deu erro na busca");
			}
		} catch (HeptaException e) {
			return Response.serverError().entity(e.getMessage()).build();
		} catch (Exception e) {
			// se der qualquer erro, mandamos a request de volta com novo captcha
			e.printStackTrace();
			LOG.error(StacktraceToString.convert(e));
			CaptchaProcessoDTO captcha = null;
			try {
				captcha = CaptchaService.getCaptchaProcesso();
				captcha.setRequest(msg.getRequest());
			} catch (IOException e1) {
				LOG.error(StacktraceToString.convert(e1));
				e1.printStackTrace();
			}
			return Response.serverError().entity(captcha).build();
		}
	}
}
