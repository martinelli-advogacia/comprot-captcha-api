package br.com.martinelliadvogados.captchacomprot.dto;

public class RequestListaProcessosDTO extends RequestComprot {
	String cnpj;
	String numUltimoProcesso;
	long dataInicial;
	long dataFinal;

	public RequestListaProcessosDTO() {
		super();
	}

	public RequestListaProcessosDTO(String cnpj, String numUltimoProcesso, long dataInicial, long dataFinal) {
		super();
		this.cnpj = cnpj;
		this.numUltimoProcesso = numUltimoProcesso;
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNumUltimoProcesso() {
		return numUltimoProcesso;
	}

	public void setNumUltimoProcesso(String numUltimoProcesso) {
		this.numUltimoProcesso = numUltimoProcesso;
	}

	public long getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(long dataInicial) {
		this.dataInicial = dataInicial;
	}

	public long getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(long dataFinal) {
		this.dataFinal = dataFinal;
	}

}
