package br.com.martinelliadvogados.captchacomprot.dto;

public class RequestProcessoDTO extends RequestComprot{
	String numProcesso;

	public RequestProcessoDTO() {
		super();
	}

	public RequestProcessoDTO(String numProcesso) {
		super();
		this.numProcesso = numProcesso;
	}

	public String getNumProcesso() {
		return numProcesso;
	}

	public void setNumProcesso(String numProcesso) {
		this.numProcesso = numProcesso;
	}

}
