package br.com.martinelliadvogados.captchacomprot.exceptions;

import javax.ws.rs.core.Response.Status;

public class SemDadosComprotException extends HeptaException {

	private static final long serialVersionUID = 1L;

	public SemDadosComprotException() {
		super("Nenhum dado encontrado no COMPROT", Status.NO_CONTENT);
	}

}