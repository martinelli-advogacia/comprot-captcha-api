package br.com.martinelliadvogados.captchacomprot.dto;

import java.util.List;

import com.hepta.martinelliadvogados.shared.dto.ProcessoDTO;

public class ResponseListaProcessosDTO {
	String cnpj;
	String numUltimoProcesso;
	long dataInicial;
	long dataFinal;
	int numProcessoTotal;
	List<ProcessoDTO> processos;

	public ResponseListaProcessosDTO() {
		super();
	}

	public ResponseListaProcessosDTO(String cnpj, String numUltimoProcesso, long dataInicial, long dataFinal,
			int numProcessoTotal, List<ProcessoDTO> processos) {
		super();
		this.cnpj = cnpj;
		this.numUltimoProcesso = numUltimoProcesso;
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
		this.numProcessoTotal = numProcessoTotal;
		this.processos = processos;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNumUltimoProcesso() {
		return numUltimoProcesso;
	}

	public void setNumUltimoProcesso(String numUltimoProcesso) {
		this.numUltimoProcesso = numUltimoProcesso;
	}

	public long getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(long dataInicial) {
		this.dataInicial = dataInicial;
	}

	public long getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(long dataFinal) {
		this.dataFinal = dataFinal;
	}

	public int getNumProcessoTotal() {
		return numProcessoTotal;
	}

	public void setNumProcessoTotal(int numProcessoTotal) {
		this.numProcessoTotal = numProcessoTotal;
	}

	public List<ProcessoDTO> getProcessos() {
		return processos;
	}

	public void setProcessos(List<ProcessoDTO> processos) {
		this.processos = processos;
	}

}
