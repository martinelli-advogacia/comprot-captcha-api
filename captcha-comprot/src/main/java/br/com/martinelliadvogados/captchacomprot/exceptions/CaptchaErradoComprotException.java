package br.com.martinelliadvogados.captchacomprot.exceptions;

import javax.ws.rs.core.Response.Status;

public class CaptchaErradoComprotException extends HeptaException {

	private static final long serialVersionUID = 1L;

	public CaptchaErradoComprotException() {
		super("Captcha incorreto, tente novamente.", Status.EXPECTATION_FAILED);
	}

}