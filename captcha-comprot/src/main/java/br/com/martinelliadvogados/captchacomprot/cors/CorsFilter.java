package br.com.martinelliadvogados.captchacomprot.cors;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.hepta.martinelliadvogados.shared.Application;

@Provider
@PreMatching
public class CorsFilter implements ContainerRequestFilter, ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext request) throws IOException {

		if (isPreflightRequest(request)) {
			request.abortWith(Response.ok().build());
		}
	}

	private static boolean isPreflightRequest(ContainerRequestContext request) {
		return request.getHeaderString("Origin") != null && request.getMethod().equalsIgnoreCase("OPTIONS");
	}

	@Override
	public void filter(ContainerRequestContext request, ContainerResponseContext response) throws IOException {

		if (request.getHeaderString("Origin") == null) {
			return;
		}

		if (isPreflightRequest(request)) {
			response.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
			response.getHeaders()
					.add("Access-Control-Allow-Headers",
							"X-Requested-With, Authorization, Accept-Version, Content-MD5, CSRF-Token, Content-Type");
		}

		String origin = generateOrigin(request);

		response.getHeaders().add("Access-Control-Allow-Credentials", "true");
		response.getHeaders().add("Access-Control-Allow-Origin", origin);
		response.getHeaders()
				.add("Access-Control-Expose-Headers", "Content-type, Content-Disposition, X-Suggested-Filename");
	}

	private String generateOrigin(ContainerRequestContext request) {
		String urlLocal = Application.getApplicationDomainFront();
		String urlDomain = Application.getApplicationDomain();
		String requestUrl = request.getHeaderString("Origin");
		if (urlLocal.contains(requestUrl)) {
			return requestUrl;
		} else if (urlDomain.contains(requestUrl)) {
			return requestUrl;
		}
		return urlDomain;
	}

}
