import requests
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import os
import sys


def main():
    options = webdriver.ChromeOptions()
    options.add_argument('ignore-certificate-errors')
    driver = webdriver.Chrome("./chromedriver", options=options)
    try:
        for x in range(100):
            driver.get(
                "https://comprot.fazenda.gov.br/comprotegov/site/index.html#ajax/processo-consulta.html")
            WebDriverWait(driver, 10).until(
                EC.presence_of_element_located(
                    (By.CSS_SELECTOR, "#divContainerCaptcha_captcha_serpro_gov_br img"))
            )
            captchaImageElement = WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "#divContainerCaptcha_captcha_serpro_gov_br img"))
            )

            tokenValue = driver.find_element_by_id(
                "txtToken_captcha_serpro_gov_br").get_attribute('value')
            urlWav = "https://comprot.fazenda.gov.br/captchaserpro/captcha/2.0.0/som/som.wav?"+tokenValue
            r = requests.get(urlWav, allow_redirects=True, verify=False)
            # print(urlWav)
            # print(r.status_code)
            if r.status_code == 200:
                timestamp = int(time.time())
                pathImg = './download/img_'+str(timestamp)+'.png'
                pathWav = './download/wav_'+str(timestamp)+'.wav'
                with open(pathImg, 'wb') as file:
                    file.write(captchaImageElement.screenshot_as_png)
                with open(pathWav, 'wb') as file:
                    open(pathWav, 'wb').write(r.content)
                print(pathImg)
                print(pathWav)
                print(tokenValue)
                break
    except:
        pass
    finally:
        driver.close()


if __name__ == '__main__':  # chamada da funcao principal
    main()
