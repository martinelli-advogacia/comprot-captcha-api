package com.hepta.martinelliadvogados.comprot.job;

import org.jboss.logging.Logger;
import org.quartz.DisallowConcurrentExecution;

import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.enums.JobComprot;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.service.MuitosProcessosService;

@DisallowConcurrentExecution
public class ErrosJobs extends ComprotJob {

	private static final Logger LOG = Logger.getLogger(ErrosJobs.class);
	MuitosProcessosService service = new MuitosProcessosService(LOG, true);

	public ErrosJobs() {
		super(LOG);
	}

	@Override
	protected JobComprot setJobType(TipoCliente tipo) {
		return JobComprot.ERROS;
	}

	@Override
	public void realJob() throws Throwable {
		int count = 0;
		String msg = "Erros : ";

		service.setNumClientes(Application.getComprotJobsQuantidadeErros());
		count = service.pegarMuitosProcessos();

		msg += count;
		log(msg);
	}

}