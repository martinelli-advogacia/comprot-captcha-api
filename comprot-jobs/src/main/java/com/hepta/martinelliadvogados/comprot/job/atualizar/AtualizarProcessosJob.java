package com.hepta.martinelliadvogados.comprot.job.atualizar;

import org.jboss.logging.Logger;
import org.quartz.DisallowConcurrentExecution;

import com.hepta.martinelliadvogados.comprot.job.ComprotJob;
import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.enums.JobComprot;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.service.AtualizarProcessosService;

@DisallowConcurrentExecution
public class AtualizarProcessosJob extends ComprotJob {

	private static final Logger LOG = Logger.getLogger(AtualizarProcessosJob.class);

	AtualizarProcessosService service = new AtualizarProcessosService();

	public AtualizarProcessosJob() {
		super(LOG);
		this.prefixLogMsg = ">>";
	}

	@Override
	protected JobComprot setJobType(TipoCliente tipo) {
		if (tipo.equals(TipoCliente.CASA))
			return JobComprot.BUSCA_DADOS_CASA;
		else
			return JobComprot.BUSCA_DADOS_PROSPECCAO;
	}

	@Override
	public void realJob() throws Throwable {
		String descricao = "ATUALIZAR DADOS PROCESSOS EXISTENTES " + tipo.toString();
		log("INICIO JOB " + descricao);



		int quantidade = 0;
		if (tipo.equals(TipoCliente.CASA))
			quantidade = Application.getComprotJobsQuantidadeAtualizaCasa();
		else
			quantidade = Application.getComprotJobsQuantidadeAtualizaProspeccao();

		service.setNovosProcessos(false);
		service.setTipo(tipo);
		service.setImprimirLogs(mostrarLogs);
		service.setNumeroProcessos(quantidade);
		int atualizados = service.atualizarProcessos();

		String msg = ">>Atualizados : " + atualizados + " " + tipo.toString();
		if (atualizados != 0)// impedir spam
			LOG.warn(msg);
		log("FIM JOB " + descricao);
	}

}