package com.hepta.martinelliadvogados.comprot.job;

import org.jboss.logging.Logger;
import org.quartz.DisallowConcurrentExecution;

import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.enums.JobComprot;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.service.NovosProcessosService;

@DisallowConcurrentExecution
public class AdicionaNovoProcessoJob extends ComprotJob {

	private static final Logger LOG = Logger.getLogger(AdicionaNovoProcessoJob.class);
	NovosProcessosService service = new NovosProcessosService(LOG, true);

	public AdicionaNovoProcessoJob() {
		super(LOG);
		this.mostrarLogs = true;
	}

	@Override
	protected JobComprot setJobType(TipoCliente tipo) {
		if (tipo.equals(TipoCliente.CASA))
			return JobComprot.ACHA_NOVOS_CASA;
		else
			return JobComprot.ACHA_NOVOS_PROSPECCAO;
	}

	@Override
	public void realJob() throws Throwable {
		log("INICIANDO JOB DE BUSCA DE NOVOS PROCESSOS " + tipo.toString() + " " + System.currentTimeMillis() );
		int count = 0;
		String msg = "Novos : ";

		int quantidade = 0;
		if (tipo.equals(TipoCliente.CASA))
			quantidade = Application.getComprotJobsQuantidadeNovosCasa();
		else
			quantidade = Application.getComprotJobsQuantidadeNovosProspeccao();

		service.setTipo(tipo);
		service.setNumClientes(quantidade);
		count = service.pesquisarNovosProcessos();

		msg += count + " " + tipo.toString();
		LOG.warn(msg +" "+ System.currentTimeMillis());
		log("FIM JOB DE BUSCA DE NOVOS PROCESSOS " + tipo.toString() + " " + System.currentTimeMillis());
	}

}
