package com.hepta.martinelliadvogados.comprot.job.atualizar;

import org.jboss.logging.Logger;
import org.quartz.DisallowConcurrentExecution;

import com.hepta.martinelliadvogados.comprot.job.ComprotJob;
import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.enums.JobComprot;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.service.AtualizarProcessosService;

@DisallowConcurrentExecution
public class AtualizarNovosProcessosJob extends ComprotJob {
	private static final Logger LOG = Logger.getLogger(AtualizarNovosProcessosJob.class);
	AtualizarProcessosService service = new AtualizarProcessosService();



	public AtualizarNovosProcessosJob() {
		super(LOG);
		this.mostrarLogs = true;
		this.prefixLogMsg = ">>";
	}

	@Override
	protected JobComprot setJobType(TipoCliente tipo) {
		if (tipo.equals(TipoCliente.CASA))
			return JobComprot.ATUALIZA_CASA;
		else
			return JobComprot.ATUALIZA_PROSPECCAO;
	}

	@Override
	public void realJob() throws Throwable {
		long inicio = System.currentTimeMillis();
		String descricao = "ATUALIZAR DADOS NOVOS PROCESSOS " + tipo.toString();
		log("INICIO JOB " + descricao);

		final String BUSCA = System.getenv("QTD_BUSCA");

		log("Parâmetros: \nQTD_BUSCA: "+BUSCA
				+"\nDATA_INICIAL: " + System.getenv("DATA_INICIAL")
				+"\nDATA_FINAL: " + System.getenv("DATA_FINAL")
		);

		int quantidade = 0;
		if (tipo.equals(TipoCliente.CASA))
			quantidade = Integer.parseInt(BUSCA); //Application.getComprotJobsQuantidadeBuscaCasa();
		else
			quantidade = Application.getComprotJobsQuantidadeBuscaProspeccao();

		service.setNovosProcessos(true);
		service.setTipo(tipo);
		service.setImprimirLogs(mostrarLogs);
		service.setNumeroProcessos(quantidade);

		int atualizados = service.atualizarProcessos();

		String msg = ">>NOVOS Atualizados : " + atualizados + " " + tipo.toString();
		log(msg);

		log("FIM JOB " + descricao);
		long fim = System.currentTimeMillis();
		long segundos = (fim - inicio) / 1000;
		log("Tempo do Job: "+segundos+"seg. para "+BUSCA+" registros");
	}

}