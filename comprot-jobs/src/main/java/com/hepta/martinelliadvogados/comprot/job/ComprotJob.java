package com.hepta.martinelliadvogados.comprot.job;

import java.time.LocalDateTime;

import org.jboss.logging.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.hepta.martinelliadvogados.shared.entity.JobStatus;
import com.hepta.martinelliadvogados.shared.enums.JobComprot;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.JobStatusDAO;
import com.hepta.martinelliadvogados.shared.persistence.LogJobDAO;
import com.hepta.martinelliadvogados.shared.utils.DataHelper;

@DisallowConcurrentExecution
public class ComprotJob implements Job {
	private static Logger LOG = Logger.getLogger(ComprotJob.class);

	protected TipoCliente tipo;
	protected boolean cronometrado = false;
	protected String prefixLogMsg = "";
	protected JobComprot job;
	protected boolean mostrarLogs = false;

	public ComprotJob(Logger LOGAcima) {
		super();
		LOG = LOGAcima;
	}

	protected void log(String msg) {
		if (mostrarLogs)
			LOG.info(prefixLogMsg + " " + tipo.toString() + " " + msg);
	}

	protected void realJob() throws Throwable {
		// override
	}

	protected JobComprot setJobType(TipoCliente tipo) {
		return null;//override
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		tipo = (TipoCliente) context.getJobDetail().getJobDataMap().get("tipo");
		job = setJobType(tipo);
		JobStatusDAO dao = new JobStatusDAO();
		JobStatus status = null;
		try {
			status = dao.achaOuCria(job);
		} catch (Throwable e1) {
			LOG.error(e1.getMessage());
			e1.printStackTrace();
		}
		try {
			if (status != null) {
				status.setDataInicio(LocalDateTime.now());
				dao.update(status);
			}
			if (DataHelper.isHorarioFuncionamentoComprot()) {
				realJob();
			}
			if (status != null) {
				status.setDataFim(LocalDateTime.now());
				dao.update(status);
			}
		} catch (Throwable e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
			if (status != null) {
				LogJobDAO daoLog = new LogJobDAO();
				daoLog.log(status, e);
			}
		}
	}
}
