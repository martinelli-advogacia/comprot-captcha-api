package com.hepta.martinelliadvogados.comprot.job.scheduler;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.jboss.logging.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.hepta.martinelliadvogados.comprot.job.AdicionaNovoProcessoJob;
import com.hepta.martinelliadvogados.comprot.job.ErrosJobs;
import com.hepta.martinelliadvogados.comprot.job.atualizar.AtualizarNovosProcessosJob;
import com.hepta.martinelliadvogados.comprot.job.atualizar.AtualizarProcessosJob;
import com.hepta.martinelliadvogados.shared.Application;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.utils.HibernateUtil;

public final class SchedulerComprot implements ServletContextListener {
	private static final Logger LOG = Logger.getLogger(SchedulerComprot.class);
	Scheduler quartz;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		HibernateUtil.shutdown();
		try {
			if (quartz != null)
				quartz.shutdown();
		} catch (SchedulerException e) {
			LOG.error("SEVERO:NÃO FOI POSSIVEL FINALIZAR O QUARTZ");
			e.printStackTrace();
		}
		LOG.info("Scheduler foi finalizado");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		LOG.info(Application.getApplicationEnv());
		LOG.info("Scheduler foi inicializado");
		SchedulerFactory schedulerFactory = new StdSchedulerFactory();

		try {
			quartz = schedulerFactory.getScheduler();
		} catch (SchedulerException e1) {
			LOG.error("SEVERO:NÃO FOI POSSIVEL INICIALIZAR O QUARTZ");
			e1.printStackTrace();
			return;
		}
		String identity = "COMPROT";
		JobDataMap jobDataMapCasa = new JobDataMap();
		jobDataMapCasa.put("tipo", TipoCliente.CASA);
		JobDataMap jobDataMapProspeccao = new JobDataMap();
		jobDataMapProspeccao.put("tipo", TipoCliente.PROSPECCAO);

		JobDetail jobNovosCasa = JobBuilder	.newJob(AdicionaNovoProcessoJob.class)
											.withIdentity("NovosProcessosCasa", identity)
											.setJobData(jobDataMapCasa)
											.build();
		Trigger triggerNovosCasa = TriggerBuilder	.newTrigger()
													.withIdentity("TriggerNovosProcessosCasa", identity)
													.startNow()
													.withSchedule(SimpleScheduleBuilder	.simpleSchedule()
																						.withIntervalInSeconds(1)
																						.repeatForever())
													.build();

		// INSERIR DADOS COMPLETOS EM PROCESSOS NOVOS ENCONTRADOS DE CASA
		JobDetail jobDadosCasa = JobBuilder	.newJob(AtualizarNovosProcessosJob.class)
											.withIdentity("AtualizarNovosProcessosCasa", identity)
											.setJobData(jobDataMapCasa)
											.build();
		Trigger triggerDadosCasa = TriggerBuilder	.newTrigger()
													.withIdentity("TriggerAtualizarNovosProcessosCasa", identity)
													.startNow()
													.withSchedule(SimpleScheduleBuilder	.simpleSchedule()
																						.withIntervalInSeconds(1)
																						.repeatForever())
													.build();

		// ATUALIZAR DADOS DE PROCESSOS ATIVOS DE CASA
		JobDetail jobAtualizarCasa = JobBuilder	.newJob(AtualizarProcessosJob.class)
												.withIdentity("AtualizarProcessosCasa", identity)
												.usingJobData(jobDataMapCasa)
												.build();
		Trigger triggerAtualizarCasa = TriggerBuilder	.newTrigger()
														.withIdentity("TriggerAtualizarProcessosCasa", identity)
														.startNow()
														.withSchedule(SimpleScheduleBuilder	.simpleSchedule()
																							.withIntervalInSeconds(1)
																							.repeatForever())
														.build();

		// PROSPECCAO
		// ACHAR NOVOS PROCESSOS DE PROSPECCAO
		JobDetail jobNovosProspeccao = JobBuilder	.newJob(AdicionaNovoProcessoJob.class)
													.withIdentity("NovosProcessosProspeccao", identity)
													.usingJobData(jobDataMapProspeccao)
													.build();
		Trigger triggerNovosProspeccao = TriggerBuilder	.newTrigger()
														.withIdentity("TriggerNovosProcessosProspeccao", identity)
														.startNow()
														.withSchedule(SimpleScheduleBuilder	.simpleSchedule()
																							.withIntervalInSeconds(1)
																							.repeatForever())
														.build();

		// INSERIR DADOS COMPLETOS EM PROCESSOS NOVOS ENCONTRADOS DE PROSPECCAO
		JobDetail jobDadosProspeccao = JobBuilder	.newJob(AtualizarNovosProcessosJob.class)
													.withIdentity("AtualizarNovosProcessosProspeccao", identity)
													.usingJobData(jobDataMapProspeccao)
													.build();
		Trigger triggerDadosProspeccao = TriggerBuilder	.newTrigger()
														.withIdentity("TriggerAtualizarNovosProcessosProspeccao",
																identity)
														.startNow()
														.withSchedule(SimpleScheduleBuilder	.simpleSchedule()
																							.withIntervalInSeconds(1)
																							.repeatForever())
														.build();

		// ATUALIZAR DADOS DE PROCESSOS ATIVOS DE PROSPECCAO
		JobDetail jobAtualizarProspeccao = JobBuilder	.newJob(AtualizarProcessosJob.class)
														.withIdentity("AtualizarProcessosProspeccao", identity)
														.usingJobData(jobDataMapProspeccao)
														.build();
		Trigger triggerAtualizarProspeccao = TriggerBuilder	.newTrigger()
															.withIdentity("TriggerAtualizarProcessosProspeccao",
																	identity)
															.startNow()
															.withSchedule(
																	SimpleScheduleBuilder	.simpleSchedule()
																							.withIntervalInSeconds(1)
																							.repeatForever())
															.build();

		// ERROS
		// ATUALIZAR DADOS DE PROCESSOS ATIVOS DE PROSPECCAO
		JobDetail jobErros = JobBuilder	.newJob(ErrosJobs.class)
										.withIdentity("Erros", identity)
										.usingJobData(jobDataMapProspeccao)
										.withDescription("Cuidando de erros")
										.build();
		Trigger triggerErros = TriggerBuilder	.newTrigger()
												.withIdentity("TriggerErros", identity)
												.startNow()
												.withSchedule(SimpleScheduleBuilder	.simpleSchedule()
																					.withIntervalInSeconds(1)
																					.repeatForever())
												.build();

		try {
			// casa
//			quartz.scheduleJob(jobNovosCasa, triggerNovosCasa);
			quartz.scheduleJob(jobDadosCasa, triggerDadosCasa);
			//quartz.scheduleJob(jobAtualizarCasa, triggerAtualizarCasa);
			// prospeccao
//			quartz.scheduleJob(jobNovosProspeccao, triggerNovosProspeccao);
			//quartz.scheduleJob(jobDadosProspeccao, triggerDadosProspeccao);
			//quartz.scheduleJob(jobAtualizarProspeccao, triggerAtualizarProspeccao);
			// erros
			//quartz.scheduleJob(jobErros, triggerErros);
		} catch (SchedulerException e) {
			LOG.error("SEVERO:NÃO FOI POSSIVEL FAZER SCHEDULE DOS JOBS DO QUARTZ");
			e.printStackTrace();
		}

		try {
			quartz.start();
		} catch (SchedulerException e) {
			LOG.error("SEVERO:NÃO FOI POSSIVEL FAZER INICIAR JOBS DO QUARTZ");
			e.printStackTrace();
		}
	}
}
