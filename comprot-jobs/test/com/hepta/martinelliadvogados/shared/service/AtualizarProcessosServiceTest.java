package com.hepta.martinelliadvogados.shared.service;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.utils.CronometroExecucao;

class AtualizarProcessosServiceTest {
	AtualizarProcessosService service = new AtualizarProcessosService(true,true,500,true);

	@Test
	void testAtualizarProcessosService() {
		service.setTipo(TipoCliente.CASA);
		CronometroExecucao<String> cronometro1 = new CronometroExecucao<>();
		CronometroExecucao<String> cronometro2 = new CronometroExecucao<>();
		CronometroExecucao<String> cronometro3 = new CronometroExecucao<>();
		try {
			final Map<String, Processo> processosEntity = new HashMap<>();
			cronometro1.cronometrar(() -> {
				processosEntity.putAll(service.puxarDados());
				return "";
			}, "Puxando do banco");

			 
			final Map<String, ProcessoWrapper> processos = new HashMap<>();
			cronometro2.cronometrar(() -> {
				processos.putAll(service.geraRequests(processosEntity));
				return "";
			}, "Buscando do comprot");
			
			cronometro3.cronometrar(() -> {
				System.out.println(service.updateDados(processos, processosEntity));
				return "";
			}, "Salvando no banco");
			
			assert (true);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	void testAtualizarProcessos() {
		fail("Not yet implemented");
	}

	@Test
	void testPuxarDados() {
		fail("Not yet implemented");
	}

	@Test
	void testGeraRequests() {
		fail("Not yet implemented");
	}

	@Test
	void testPreparaRequests() {
		fail("Not yet implemented");
	}

	@Test
	void testGetDados() {
		fail("Not yet implemented");
	}

	@Test
	void testUpdateDados() {
		fail("Not yet implemented");
	}

}
