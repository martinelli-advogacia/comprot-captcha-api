package com.hepta.martinelliadvogados.shared.service;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;
import org.junit.jupiter.api.Test;

import com.hepta.martinelliadvogados.shared.dto.DadosNovosProcessosDTO;
import com.hepta.martinelliadvogados.shared.dto.ListaProcessoDTO;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.FilialDAO;
import com.hepta.martinelliadvogados.shared.utils.CronometroExecucao;
import com.hepta.martinelliadvogados.shared.utils.DataHelper;

class NovosProcessosServiceTest {
	private static final Logger LOG = Logger.getLogger(NovosProcessosServiceTest.class);
	NovosProcessosService service = new NovosProcessosService(LOG,false);

	@Test
	void testAtualizarProcessosService() {
		service.setTipo(TipoCliente.CASA);
		CronometroExecucao<String> cronometro1 = new CronometroExecucao<>();
		CronometroExecucao<String> cronometro2 = new CronometroExecucao<>();
		CronometroExecucao<String> cronometro3 = new CronometroExecucao<>();
		try {
			final List<Filial> filiais = new ArrayList<>();
//			cronometro1.cronometrar(() -> {
//				filiais.addAll(service.puxarDados());
//				return "";
//			}, "Puxando do banco");
			FilialDAO dao = new FilialDAO();
			Filial filial = dao.find(897393L);
			filiais.add(filial);
			 
			final List<DadosNovosProcessosDTO> processos = new ArrayList<>();
			cronometro2.cronometrar(() -> {
				processos.addAll(service.buscaDadosProcessosPorFilial(DataHelper.getDataInicial(),
						DataHelper.getDataAtual(), filiais));
				return "";
			}, "Buscando do comprot");
			int count = 0;
			for (DadosNovosProcessosDTO d : processos) {
				int num = d.getWrapper().getProcessos().size();
				if (!d.getWrapper().isCorreto())
					System.out.println("Numero de processos desse cliente("
							+ num
							+ ") não é igual ao do comprot("
							+ d.getWrapper().getTotalDeProcessosEncontrados()
							+ ")");
				count += num;
			}
			System.out.println("Achou " + count + " no comprot");
			cronometro3.cronometrar(() -> {
				service.inserirNovosProcessosPesquisados(processos);
				return "";
			}, "Salvando no banco");
			
			assert (true);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	void testAtualizarProcessos() {
		fail("Not yet implemented");
	}

	@Test
	void testPuxarDados() {
		fail("Not yet implemented");
	}

	@Test
	void testGeraRequests() {
		fail("Not yet implemented");
	}

	@Test
	void testPreparaRequests() {
		fail("Not yet implemented");
	}

	@Test
	void testGetDados() {
		fail("Not yet implemented");
	}

	@Test
	void testUpdateDados() {
		fail("Not yet implemented");
	}

}
