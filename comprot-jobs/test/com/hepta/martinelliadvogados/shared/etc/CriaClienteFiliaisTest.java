package com.hepta.martinelliadvogados.shared.etc;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import com.hepta.martinelliadvogados.shared.utils.CheckClientesFiliais;
import com.hepta.martinelliadvogados.shared.utils.CriaClienteFiliais;

class CriaClienteFiliaisTest {

	@Test
	void criaTest() {
		try {
			CriaClienteFiliais cria = new CriaClienteFiliais();
			cria.criar();
			assert(true);
		}catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	void checkTest() {
		try {
			CheckClientesFiliais cria = new CheckClientesFiliais();
			cria.check();
			assert(true);
		}catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
