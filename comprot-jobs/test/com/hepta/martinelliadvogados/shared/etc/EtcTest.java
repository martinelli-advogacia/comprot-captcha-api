package com.hepta.martinelliadvogados.shared.etc;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EtcTest {
	@Test
	void testInstant() {
		try {
			Instant step = Instant.ofEpochMilli(0L).plus(365, ChronoUnit.DAYS);
			Instant fim = Instant.now();
			Instant ini = fim.minus(365, ChronoUnit.DAYS);
			while (true) {
				if(ini.compareTo(step) < 0) {
					System.out.println("ini: " + Instant.ofEpochMilli(0L).toString() + " fim: " + ini.toString());
					break;
				}
				System.out.println("ini: " + ini.toString() + " fim: " + fim.toString());

				fim = fim.minus(365, ChronoUnit.DAYS);
				ini = fim.minus(365, ChronoUnit.DAYS);
			}
			assert (true);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}
}
