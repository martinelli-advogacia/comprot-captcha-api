package com.hepta.martinelliadvogados.shared.etc;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.entity.Processo;
import com.hepta.martinelliadvogados.shared.integration.ComprotRequest;
import com.hepta.martinelliadvogados.shared.persistence.ProcessoDAO;
import com.hepta.martinelliadvogados.shared.utils.StacktraceToString;

class AtualizaProcessosTest {
	static final ExecutorService pool = Executors.newFixedThreadPool(10);
	ProcessoDAO daoProcesso = new ProcessoDAO();

	@Test
	void test() {
		try {
			List<String> numeroProcessos = new ArrayList<>(buscaArquivo("processos_autos_infracao"));
			List<Set<String>> processoBatch = new ArrayList<>();
			Set<String> batch = new HashSet<>();
			for (int i = 0; i < numeroProcessos.size(); i++) {
				batch.add(numeroProcessos.get(i));
				if ((i > 0 && i % 250 == 0) || numeroProcessos.size() - i == 1) {
					processoBatch.add(batch);
					batch = new HashSet<>();
				}
			}
			int atualizados = 0;
			int count = 0;
			for (Set<String> b : processoBatch) {
				if (count > 43) {
					System.out.println("batch");
					List<Processo> processosEntity = daoProcesso.find(b);
					Map<String, Processo> processosEntityMap = new HashMap<>();
					for (Processo processo : processosEntity) {
						String key = processo.getNumeroProcessoPrincipal();
						if (!processosEntityMap.containsKey(key)) {
							processosEntityMap.put(processo.getNumeroProcessoPrincipal(), processo);
						}
					}
					Map<String, ProcessoWrapper> processos = geraRequests(processosEntityMap);

					atualizados += daoProcesso.update(processos, processosEntityMap);
					System.out.println("fim batch");
				} else {
					count++;
				}
			}

			assert (atualizados > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	private Set<String> buscaArquivo(String nomeArquivo) throws FileNotFoundException {
		System.out.println("Working Directory = " + System.getProperty("user.dir"));
		Scanner sc = new Scanner(new File(System.getProperty("user.dir") + "/" + nomeArquivo));
		Set<String> lines = new HashSet<>();
		while (sc.hasNextLine()) {
			lines.add(sc.nextLine());
		}
		sc.close();
		return lines;
	}

	public Map<String, ProcessoWrapper> geraRequests(Map<String, Processo> processosEntityMap) {
		List<ProcessoWrapper> wrappers = getDados(processosEntityMap.values());

		Map<String, ProcessoWrapper> map = new HashMap<>();
		for (ProcessoWrapper p : wrappers) {
			if (p != null) {
				String key = p.getProcesso().getNumeroProcesso();
				if (!key.isEmpty() && !key.isBlank())
					map.put(key, p);
			}
		}
		return map;
	}

	public CompletableFuture<ProcessoWrapper> preparaRequests(Processo p) {
		return CompletableFuture.supplyAsync(() -> {
			try {
				HttpClient client = HttpClient.newHttpClient();
				HttpRequest req = ComprotRequest.getProcessoRequest(p.getNumeroProcessoPrincipal());
				HttpResponse<String> response = client.sendAsync(req, BodyHandlers.ofString()).get();
				return ComprotRequest.responseToProcessoWrapper(response.body());
			} catch (Throwable e) {
				e.printStackTrace();
				System.out.println("Erro ao preparar requests" + StacktraceToString.convert(e));
				return null;
			}
		}, pool);
	}

	public List<ProcessoWrapper> getDados(Collection<Processo> collection) {
		List<ProcessoWrapper> dados = new ArrayList<>();
		try {
			List<CompletableFuture<ProcessoWrapper>> futuros = new ArrayList<>();
			for (Processo p : collection) {
				futuros.add(preparaRequests(p));
			}

			CompletableFuture<Void> allFutures = CompletableFuture.allOf(
					futuros.toArray(new CompletableFuture[futuros.size()]));
			CompletableFuture<List<ProcessoWrapper>> resultado = allFutures.thenApply(v -> {
				return futuros.stream().map(f -> f.join()).collect(Collectors.toList());
			});
			dados = resultado.get();
		} catch (Throwable t) {
			t.printStackTrace();
			System.out.println("Erro ao pegar dados: " + StacktraceToString.convert(t));
		}
		return dados;
	}

}
