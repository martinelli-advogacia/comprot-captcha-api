package com.hepta.martinelliadvogados.shared.etc;

import static org.junit.jupiter.api.Assertions.fail;

import java.time.Instant;

import org.junit.jupiter.api.Test;

import com.hepta.martinelliadvogados.shared.utils.DataHelper;

class DataHelperTest {

	@Test
	void testGetDataInicial() {
		try {
			assert(DataHelper.getDataInicial() == 0L);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testGetDataAtual() {
		try {
			assert(DataHelper.getDataAtual() == Instant.now().toEpochMilli());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
