package com.hepta.martinelliadvogados.shared.persistence;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.hepta.martinelliadvogados.shared.entity.Filial;
import com.hepta.martinelliadvogados.shared.enums.Erro;
import com.hepta.martinelliadvogados.shared.enums.TipoCliente;
import com.hepta.martinelliadvogados.shared.persistence.FilialDAO;

class FilialDAOTest {
	FilialDAO dao = new FilialDAO();
	@Test
	void testPorTipoCasa() {
		try {
			List<Filial> list = dao.porTipo(20, TipoCliente.CASA, LocalDate.now().minusDays(7));
			System.out.println(list.size());
			assert (!list.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	void testPorTipoProspeccao() {
		try {
			List<Filial> list = dao.porTipo(20, TipoCliente.PROSPECCAO, LocalDate.now().minusMonths(2));
			System.out.println(list.size());
			assert (!list.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	void testPorErro() {
		try {
			List<Filial> list = dao.porErro(20, Erro.PROCESSOS_DEMAIS);
			assert (!list.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
