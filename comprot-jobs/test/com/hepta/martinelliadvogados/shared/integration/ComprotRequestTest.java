package com.hepta.martinelliadvogados.shared.integration;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.hepta.martinelliadvogados.shared.dto.ListaProcessoDTO;
import com.hepta.martinelliadvogados.shared.dto.ListaProcessoWrapper;
import com.hepta.martinelliadvogados.shared.dto.ProcessoWrapper;
import com.hepta.martinelliadvogados.shared.integration.ComprotRequest;

class ComprotRequestTest {

	@Test
	public void buscaProcessosPorCnpjTest() throws IOException, InterruptedException {
		ComprotRequest comprot = new ComprotRequest();
		try {
			String cpfCnpjComMascara = "43999424000114";
			long dataInicial = 0L;
			long dataFinal = 1600225200000L;
			String numeroUltimoProcesso = "";
			ListaProcessoWrapper wrapper = comprot.buscaProcessosPorCnpj(cpfCnpjComMascara, dataInicial,
					dataFinal, numeroUltimoProcesso);
			System.out.println(wrapper.getTotalDeProcessosEncontrados());
			System.out.println(wrapper.getProcessos().size());
			assert (wrapper.getProcessos().size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void buscaProcessosTest() throws IOException, InterruptedException {
		try {
			ComprotRequest comprot = new ComprotRequest();
			String numProcesso = "001110003159580";
			ProcessoWrapper wrapper = comprot.buscaProcesso(numProcesso);
			System.out.println(wrapper.getMovimentos().size());
			System.out.println(wrapper.getProcesso().getNomeAssunto());
			System.out.println(wrapper.getProcesso().getNumeroCpfCnpj());
			assert (wrapper.getMovimentos().size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

	}

	@Test
	public void apiRetonaDuplicadosTest() throws IOException, InterruptedException {
		try {
			ComprotRequest comprot = new ComprotRequest();
			String cnpj = "63460299000187";
			long dataInicial = 0L;
			long dataFinal = System.currentTimeMillis();
			String numeroUltimoProcesso = "";
			ListaProcessoWrapper wrapper = comprot.buscaProcessosPorCnpj(cnpj, dataInicial, dataFinal,
					numeroUltimoProcesso);
			if (wrapper.getProcessos().isEmpty())
				fail("Sem processos nesse cnpj");

			Map<String, ListaProcessoDTO> mapProcessos = new HashMap<>();
			for (ListaProcessoDTO p : wrapper.getProcessos()) {
				numeroUltimoProcesso = p.getNumeroProcessoPrincipal();
				mapProcessos.put(numeroUltimoProcesso, p);
			}

			ListaProcessoWrapper wrapper2 = comprot.buscaProcessosPorCnpj(cnpj, dataInicial, dataFinal,
					numeroUltimoProcesso);
			if (wrapper2.getProcessos().isEmpty())
				fail("Sem processos na segunda busca desse cnpj");

			for (ListaProcessoDTO p : wrapper2.getProcessos()) {
				if (mapProcessos.containsKey(p.getNumeroProcessoPrincipal()))
					fail("Chave duplicada.");
			}

			assert (true);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
