Esse projeto é separado em duas partes:
	1 - Um back-end dos jobs que são rodados periodicamente.
	2 - Um back-end + front-end para pesquisar sob demanda.


Para configurar o back-end dos jobs:
	1 - você precisa ter um MicrosoftSQL server na sua máquina (reveja o arquivo de properties  para mais informações)
	2 - Depois de configurar o MSSQL, altere o arquivo de properties e mude a opção de criar tabelas no db para CREATE.
	3 - procure o teste CriaClienteFiliaisTest, rode ele e espere. Esse teste insere em torno de 130k Clientes e 135k Filiais no banco, portanto demora bastante. Se esse teste não inserir nada, veja se existem os arquivos cnpjs_carteira e cnpjs_todos no root do projeto martinelli-advogados-apis
	4 - Adicione o certificado para o COMPROT que está na pasta principal dos dois projetos (martinelli-advogados-comprot-apis) no seu cacerts (que está localizado na JVM que você está usando)

Após isso, é só rodar o projeto no tomcat.

DETALHES IMPORTANTES:
	Esse projeto pega milhões de dados do site do comprot, para que a inserção no banco seja mais rápida, ignoramos inserção de chaves duplicadas, ou seja, o SQL server ignora qualquer insert que tenha chave duplicada na tabela e continua normalmente. Para que isso seja possível, tivemos de rodar esse comando na mão:
		ALTER TABLE Processo DROP CONSTRAINT UK_m5oxurrad5sduw1vbipcqk46c
		ALTER TABLE Processo ADD CONSTRAINT UK_DS_NUMERO_PROCESSO_PRINCIPAL UNIQUE ( DS_NUMERO_PROCESSO_PRINCIPAL ) WITH (IGNORE_DUP_KEY = ON) ON [PRIMARY]
	Damos drop na unique key da coluna DS_NUMERO_PROCESSO_PRINCIPAL e criamos ela novamente com a opção de ignorar.

Para configurar o back-end + front-end de pesquisa sob demanda:
	Apague a paste WebContent
	Rode o comando "npm run build" dentro da pasta do front-end
	Supostamente isso gera a pasta WebContent no lugar correto e a partir daí é só gerar o war normalmente.