FROM tomcat:9.0.39-jdk11-openjdk

ARG DATA_INICIAL
ARG DATA_FINAL
ARG QTD_BUSCA
ENV DATA_INICIAL=${DATA_INICIAL}
ENV DATA_FINAL=${DATA_FINAL}
ENV QTD_BUSCA=${QTD_BUSCA}

COPY comprot.der comprot.der

RUN echo yes | keytool -importcert -alias startssl -keystore \
    /$JAVA_HOME/lib/security/cacerts -storepass changeit -file comprot.der

COPY comprot-jobs/target/comprot-jobs.war /usr/local/tomcat/webapps/comprot-jobs.war

WORKDIR /usr/local/tomcat/webapps